<?php
/**
*
* @author MarkDHamill (Mark D Hamill) mark@phpbbservices.com
* @package umil
* @version $Id digests_install.php 2.2.6 2009-11-08 02:51:30GMT MarkDHamill $
* @copyright (c) 2009 Mark D Hamill
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/ucp_digests');

if (!file_exists($phpbb_root_path . 'umil/umil.' . $phpEx))
{
	trigger_error('Please download the latest UMIL (Unified MOD Install Library) from: <a href="http://www.phpbb.com/mods/umil/">phpBB.com/mods/umil</a>', E_USER_ERROR);
}

// We only allow a founder to install this MOD
if ($user->data['user_type'] != USER_FOUNDER)
{
	if ($user->data['user_id'] == ANONYMOUS)
	{
		login_box('', 'LOGIN');
	}
	trigger_error('NOT_AUTHORISED');
}

if (!class_exists('umil'))
{
	include($phpbb_root_path . 'umil/umil.' . $phpEx);
}

$umil = new umil(true);

$mod = array(
	'name'		=> 'phpBB Digests',
	'version'	=> '2.2.6',
	'config'	=> 'digests_version',
);

if (confirm_box(true))
{
	// Install the base 2.2.6 version
	if (!$umil->config_exists($mod['config']))
	{
		
		// We must handle the version number ourselves.
		$umil->config_add($mod['config'], $mod['version']);

		// Set other configuration variables
		$umil->config_add('digests_custom_stylesheet_path', 'prosilver/theme/digest_stylesheet.css');
		$umil->config_add('digests_digests_title', 'phpBB Digests');
		$umil->config_add('digests_enable_auto_subscriptions', '0');
		$umil->config_add('digests_enable_custom_stylesheets', '0');
		$umil->config_add('digests_enable_log', '0');
		$umil->config_add('digests_host', 'phpbbservices.com');
		$umil->config_add('digests_key_value', '');
		$umil->config_add('digests_max_items', '0');
		$umil->config_add('digests_page_url', 'http://phpbbservices.com/digests/');
		$umil->config_add('digests_require_key', '0');
		$umil->config_add('digests_show_output', '1');
		$umil->config_add('digests_user_check_all_forums', '1');
		$umil->config_add('digests_user_digest_filter_type', 'ALL');
		$umil->config_add('digests_user_digest_format', 'HTML');
		$umil->config_add('digests_user_digest_max_display_words', '0');
		$umil->config_add('digests_user_digest_max_posts', '0');
		$umil->config_add('digests_user_digest_min_words', '0');
		$umil->config_add('digests_user_digest_new_posts_only', '0');
		$umil->config_add('digests_user_digest_pm_mark_read', '0');
		$umil->config_add('digests_user_digest_remove_foes', '0');
		$umil->config_add('digests_user_digest_reset_lastvisit', '0');
		$umil->config_add('digests_user_digest_send_hour_gmt', '-1');
		$umil->config_add('digests_user_digest_send_on_no_posts', '0');
		$umil->config_add('digests_user_digest_show_mine', '1');
		$umil->config_add('digests_user_digest_show_pms', '1');
		$umil->config_add('digests_user_digest_sortby', 'board');
		$umil->config_add('digests_user_digest_type', 'DAY');
		$umil->config_add('digests_weekly_digest_day', '0');

		// Create the one new table needed
		$umil->table_add(array(
			array(DIGESTS_SUBSCRIBED_FORUMS_TABLE, array(
				'COLUMNS' => array(
					'user_id' => array('UINT', '0'),
					'forum_id' => array('UINT', '0'),
				),

				'PRIMARY_KEY'	=> array('user_id', 'forum_id'),
			)),
		));

		// Extend the phpbb_users_table with additional columns
		$umil->table_column_add(USERS_TABLE, 'user_digest_type', array('VCHAR:4', 'NONE'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_format', array('VCHAR:4', 'HTML'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_show_mine', array('TINT:4', '1'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_send_on_no_posts', array('TINT:4', '0'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_send_hour_gmt', array('DECIMAL', '0.00'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_show_pms', array('TINT:4', '1'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_max_posts', array('UINT', '0'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_min_words', array('UINT', '0'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_remove_foes', array('TINT:4', '0'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_sortby', array('VCHAR:13', 'board'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_max_display_words', array('UINT', '0'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_reset_lastvisit', array('TINT:4', '1'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_filter_type', array('VCHAR:3', 'ALL'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_pm_mark_read', array('TINT:4', '0'));
		$umil->table_column_add(USERS_TABLE, 'user_digest_new_posts_only', array('TINT:4', '0'));
	
		// Add the ACP Digest Settings Category, placed under the General Tab
		$umil->module_add(array(
			array('acp', 'ACP_CAT_GENERAL', 'ACP_DIGEST_SETTINGS')
		));

		// Add the General Settings and User Default Settings to the Digest Settings category in the ACP.
		$umil->module_add(array(
			array('acp', 'ACP_DIGEST_SETTINGS', array(
					'module_basename'   => 'board',
					'module_langname'   => 'ACP_DIGEST_GENERAL_SETTINGS',
					'module_mode'       => 'digest_general',
					'module_auth'       => 'acl_a_board',
				),
			),
			array('acp', 'ACP_DIGEST_SETTINGS', array(
					'module_basename'   => 'board',
					'module_langname'   => 'ACP_DIGEST_USER_DEFAULT_SETTINGS',
					'module_mode'       => 'digest_user_defaults',
					'module_auth'       => 'acl_a_board',
				),
			),
		));

		// Add the new top level UCP Digests Category
		$umil->module_add(array(
			array('ucp', 0, 'UCP_DIGESTS')
		));
	
		// Add the General Settings and User Default Settings to the Digest Settings category in the ACP.
		$umil->module_add(array(
			array('ucp', 'UCP_DIGESTS', array(
					'module_basename'   => 'digests',
					'module_langname'   => 'UCP_DIGESTS_BASICS',
					'module_mode'       => 'basics',
					'module_auth'       => '',
				),
			),
			array('ucp', 'UCP_DIGESTS', array(
					'module_basename'   => 'digests',
					'module_langname'   => 'UCP_DIGESTS_POSTS_SELECTION',
					'module_mode'       => 'posts_selection',
					'module_auth'       => '',
				),
			),
			array('ucp', 'UCP_DIGESTS', array(
					'module_basename'   => 'digests',
					'module_langname'   => 'UCP_DIGESTS_POST_FILTERS',
					'module_mode'       => 'post_filters',
					'module_auth'       => '',
				),
			),
			array('ucp', 'UCP_DIGESTS', array(
					'module_basename'   => 'digests',
					'module_langname'   => 'UCP_DIGESTS_ADDITIONAL_CRITERIA',
					'module_mode'       => 'additional_criteria',
					'module_auth'       => '',
				),
			),
		));
		
		// Our final action, we purge the board cache
		$umil->cache_purge();
	}

	// We are done
	trigger_error('Done!');
}
else
{
	confirm_box(false, sprintf($user->lang['DIGEST_INSTALL_MOD_CONFIRM'], $mod['name']));
}

// Shouldn't get here.
redirect($phpbb_root_path . $user->page['page_name']);

?>