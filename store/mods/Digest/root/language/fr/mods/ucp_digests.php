<?php
/** 
*
* ucp_digests.php [French]
*
* @package language
* @version $Id: v3_modules.xml 52 2007-12-09 19:45:45Z jelly_doughnut $
* @copyright (c) 2005 phpBB Group 
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/
					
/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
						
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
		
global $config;
				
$lang = array_merge($lang, array(
	'DIGEST_ALL_FORUMS'					=> 'Tout',
	'DIGEST_AUTHOR'						=> 'Auteur',
	'DIGEST_BAD_EOL'					=> 'La fin de rev�t la valeur de %s est nul.', 
	'DIGEST_BOARD_LIMIT'				=> '%s (Monter la limite)',
	'DIGEST_BY'							=> 'Par',
	'DIGEST_CONNECT_SOCKET_ERROR'		=> 'Incapable d`ouvrir la connexion au site de Smartfeed de phpBB, l`erreur rapport�e est:<br />%s',
	'DIGEST_COUNT_LIMIT'				=> 'Le nombre maximum de postes dans le condens�',
	'DIGEST_COUNT_LIMIT_EXPLAIN'		=> 'Entrer un nombre plus grand que z�ro si vous voulez limiter le nombre de postes dans le condens�.',
	'DIGEST_CURRENT_VERSION_INFO'		=> 'Vous courez la version <strong>%s</strong>.',
	'DIGEST_DAILY'						=> 'Quotidiennement',
	'DIGEST_DATE'						=> 'Date',
	'DIGEST_DISABLED_MESSAGE'			=> 'Pour rendre capable des champs, choisir des Principes fondamentaux et choisir un type de condens�',
	'DIGEST_DISCLAIMER'					=> 'Ce condens� est envoy� aux membres inscrits de <a href="%s">%s</a> Forums. Vous pouvez changer ou pouvez effacer votre abonnement du <a href="%sucp.%s">Tableau de commande avant d`utilisateur</a>. Si vous avez des questions ou des r�actions sur le format de ce condens� l`envoient s`il vous pla�t au <a href="mailto:%s">%s Webmestre</a>.',
	'DIGEST_EXPLANATION'				=> 'Les condens�s sont des r�sum�s d`e-mails de messages post�s que vous sont ici envoy� p�riodiquement. Les condens�s peuvent �tre envoy�s quotidien, l`hebdomadaire ou mensuel � une heure du jour que vous choisissez. Vous pouvez sp�cifier ces forums particuliers pour lesquels vous voulez les r�sum�s de message (choisit de la S�lection de Postes), ou par d�faut vous pouvez �lire de recevoir tous messages pour tous forums pour lesquels vous �tes permis l`acc�s. Vous pouvez, bien s�r, annulez votre abonnement de condens� � tout moment par revenant simplement � cette page. La plupart des utilisateurs trouvent des condens�s pour �tre tr�s utiles. Nous vous encourageons � le donner un essaie!',
	'DIGEST_FILTER_ERROR'				=> "mail_digests.$phpEx a �t� appel� avec un invalide user_digest_filter_type = %s",
	'DIGEST_FILTER_FOES'				=> 'Enlever des postes de mes ennemis',
	'DIGEST_FILTER_TYPE'				=> 'Les types de postes dans le condens�',
	'DIGEST_FORMAT_ERROR'				=> "mail_digests.$phpEx a �t� appel� avec un invalide user_digest_format of %s",
	'DIGEST_FORMAT_FOOTER' 				=> 'Dig�rer le Format:',
	'DIGEST_FORMAT_HTML'				=> 'HTML',
	'DIGEST_FORMAT_HTML_EXPLAIN'		=> 'LE HTML fournira la mise en forme, BBCode et les signatures (si permis). Stylesheets est appliqu� si votre programme d`e-mail permet.',
	'DIGEST_FORMAT_HTML_CLASSIC'		=> 'Classique de HTML',
	'DIGEST_FORMAT_HTML_CLASSIC_EXPLAIN'	=> 'Similaire au HTML sauf les postes de sujet sont �num�r� l`int�rieur de tables',
	'DIGEST_FORMAT_PLAIN'				=> 'HTML simple',
	'DIGEST_FORMAT_PLAIN_EXPLAIN'		=> 'LE HTML simple n`applique pas de style ou des couleurs',
	'DIGEST_FORMAT_PLAIN_CLASSIC'		=> 'Le Classique simple de HTML',
	'DIGEST_FORMAT_PLAIN_CLASSIC_EXPLAIN'	=> 'Similaire au HTML Simple sauf les postes de sujet sont �num�r� l`int�rieur de tables',
	'DIGEST_FORMAT_STYLING'				=> 'Le condens� concevant',
	'DIGEST_FORMAT_STYLING_EXPLAIN'		=> 'S`il vous pla�t noter que le style en fait rendu d�pend des capacit�s de votre programme d`e-mail. D�placer votre curseur sur le type de style pour apprendre plus de chaque style.',
	'DIGEST_FORMAT_TEXT'				=> 'Texte',
	'DIGEST_FORMAT_TEXT_EXPLAIN'		=> 'Aucun HTML appara�tra dans le condens�. Seulement le texte sera montr�.',
	'DIGEST_FREQUENCY'					=> 'Le type de condens� a voulu',
	'DIGEST_FREQUENCY_EXPLAIN'			=> "Les condens�s hebdomadaires sont envoy�s sur %s. Les condens�s mensuels sont envoy�s sur le premier du mois. Le Temps universel est utilis� pour d�terminer le jour de la semaine.",
	'DIGEST_INTRODUCTION' 				=> 'Voici le dernier condens� de messages post�s sur %s Forums. S`il vous pla�t venir et joindre la discussion!',
	'DIGEST_LASTVISIT_RESET'			=> 'Remettre � l`�tat initial ma derni�re date de visite quand je suis envoy� un condens�',
	'DIGEST_LATEST_VERSION_INFO'		=> 'La derni�re version disponible est <strong>%s</strong>.',
	'DIGEST_LINK'						=> 'Lien',
	'DIGEST_LOG_WRITE_ERROR'			=> 'Incapable d`�crire pour noter avec le chemin, le chemin = %s. Ceci est fr�quemment caus� par le manque de public �crit des permissions sur ce dossier.',
	'DIGEST_MAIL_FREQUENCY' 			=> 'Dig�rer la Fr�quence',
	'DIGEST_MARK_READ'					=> 'La marque comme a lu quand ils apparaissent dans le condens�',
	'DIGEST_MAX_SIZE'					=> 'Les mots maximums pour afficher dans une poste',
	'DIGEST_MAX_SIZE_EXPLAIN'			=> 'Notification : Pour garantir l`interpr�tation coh�rente, si une poste doit �tre tronqu�e, le HTML sera enlev� de la poste.',
	'DIGEST_MAX_WORDS_NOTIFIER'			=> '... ',
	'DIGEST_MIN_SIZE'					=> 'Les mots minimums ont exig� dans la poste pour la poste pour appara�tre dans un condens�',
	'DIGEST_MIN_SIZE_EXPLAIN'			=> 'Si vous partez ce vide, ces postes avec le texte de n`importe quel nombre de mots sont incluses',
	'DIGEST_MONTHLY'					=> 'Mensuellement',
	'DIGEST_NEW'						=> 'Nouveau',
	'DIGEST_NEW_POSTS_ONLY'				=> 'Montrer de nouvelles postes seulement',
	'DIGEST_NEW_POSTS_ONLY_EXPLAIN'		=> 'Ceci �liminera n`importe quelles postes post�es avant la date et chronom�tre dure vous a visit� ce conseil. Si vous visitez le conseil et lisez fr�quemment la plupart des postes, ceci gardera des postes redondantes d`appara�tre dans votre condens�. Il peut signifier aussi que vous manquerez quelques postes dans les forums que vous n`avez pas lu.',
	'DIGEST_NO_CONSTRAINT'				=> 'Aucune contrainte',
	'DIGEST_NO_FORUMS_CHECKED' 			=> 'Au moins un forum doit �tre v�rifi�',
	'DIGEST_NO_LIMIT'					=> 'Aucune limite',
	'DIGEST_NO_POSTS'					=> 'Il n`y a pas de nouvelles postes.',
	'DIGEST_NO_PRIVATE_MESSAGES'		=> 'Vous avez de message non nouveaux ou non lus priv�s.',
	'DIGEST_NONE'						=> 'Aucun (d�sabonne)',
	'DIGEST_ON'							=> 'sur',
	'DIGEST_POST_TEXT'					=> 'Poster le Texte', 
	'DIGEST_POST_TIME'					=> 'Poster le Temps', 
	'DIGEST_POST_SIGNATURE_DELIMITER'	=> '<br />____________________<br />', // Place here whatever code (make sure it is valid XHTML) you want to use to distinguish the end of a post from the beginning of the signature line
	'DIGEST_POSTS_TYPE_ANY'				=> 'Toutes postes',
	'DIGEST_POSTS_TYPE_FIRST'			=> 'Les premi�res postes de sujets seulement',
	'DIGEST_POWERED_BY'					=> 'A aliment� par',
	'DIGEST_PRIVATE_MESSAGES_IN_DIGEST'	=> 'Ajouter mes messages priv�s non lus',
	'DIGEST_PUBLISH_DATE'				=> 'Le condens� a �t� publi� en particulier pour vous sur %s',
	'DIGEST_REMOVE_YOURS'				=> 'Enlever mes postes',
	'DIGEST_ROBOT'						=> 'Robot',
	'DIGEST_SALUTATION' 				=> 'Dear',
	'DIGEST_SELECT_FORUMS'				=> 'Inclure des postes pour ces forums',
	'DIGEST_SELECT_FORUMS_EXPLAIN'		=> 'S`il vous pla�t noter les cat�gories et les forums montr�s sont pour ces vous �tes permis de lire seulement. La s�lection de forum est rendue infirme quand vous choisissez les sujets de bookmarked seulement.',
	'DIGEST_SEND_HOUR' 					=> 'L`heure a envoy�',
	'DIGEST_SEND_HOUR_EXPLAIN'			=> 'L`heure d`arriv�e de condens� est le temps fond� sur le fuseau horaire et le temps d`�conomies/�t� de lumi�re vous r�glez dans vos pr�f�rences de conseil.',
	'DIGEST_SEND_IF_NO_NEW_MESSAGES'	=> 'Envoyer le condens� si aucuns nouveaux messages:',
	'DIGEST_SEND_ON_NO_POSTS'	 		=> 'Envoyer un condens� s`il n`y a pas de nouvelles postes',
	'DIGEST_SHOW_MY_MESSAGES' 			=> 'Montrer mes postes dans le condens�:',
	'DIGEST_SHOW_NEW_POSTS_ONLY' 		=> 'Montrer de nouvelles postes seulement',
	'DIGEST_SHOW_PMS' 					=> 'Montrer mes messages priv�s',
	'DIGEST_SIZE_ERROR'					=> "Ce champ est un champ exig�. Vous devez entrer un nombre entier positif, moins qu'ou �gal au maximum permis par l'Administrateur de Forum. Le maximum permis est %s. Si cette valeur est z�ro, il n'y a pas de limite.",
	'DIGEST_SIZE_ERROR_MIN'				=> 'Vous devez entrer une valeur positive ou partez le vide de champ',
	'DIGEST_SOCKET_FUNCTIONS_DISABLED'	=> 'Les fonctions de douille actuellement sont rendues infirme.',
	'DIGEST_SORT_BY'					=> 'Poster l`ordre de genre',
	'DIGEST_SORT_BY_ERROR'				=> "mail_digests.$phpEx a �t� appel� avec un invalide user_digest_sortby = %s",
	'DIGEST_SORT_BY_EXPLAIN'			=> 'Tous condens�s sont tri�s par la cat�gorie et alors par le forum, comme ils sont montr�s sur l`indice principal. Les options de genre s`appliquent � comment les postes sont arrang�es dans les sujets. L`Ordre traditionnel est l`ordre implicite utilis� par phpBB 2, qui est le dernier temps de poste de sujet (descendant) alors par le temps de poste dans le sujet.',
	'DIGEST_SORT_FORUM_TOPIC'			=> 'Ordre traditionnel',
	'DIGEST_SORT_FORUM_TOPIC_DESC'		=> 'L`Ordre traditionnel, Derni�res Postes Premi�rement',
	'DIGEST_SORT_POST_DATE'				=> 'De plus vieil � plus nouveau',
	'DIGEST_SORT_POST_DATE_DESC'		=> 'De plus nouvel � plus vieux',
	'DIGEST_SORT_USER_ORDER'			=> 'Utiliser mes pr�f�rences d`exposition de conseil',
	'DIGEST_SQL_PMS'					=> 'SQL a utilis� pour les messages priv�s pour %s: %s',
	'DIGEST_SQL_PMS_NONE'				=> 'Aucun SQL a distribu� pour private_messages pour %s parce que l`utilisateur a opt� pour ne pas montrer de messages priv�s dans le condens�.',
	'DIGEST_SQL_POSTS_USERS'			=> 'SQL a utilis� pour les postes pour %s: %s',
	'DIGEST_SQL_USERS'					=> 'SQL a rapport� des utilisateurs obtenant des condens�s: %s',
	'DIGEST_SUBJECT_LABEL'				=> 'Sujet',
	'DIGEST_SUBJECT_TITLE'				=> '%s %s Condens�',
	'DIGEST_TIME_ERROR'					=> "mail_digests.$phpEx pr�m�dit� un mauvais condens� envoie l'heure de %s",
	'DIGEST_TOTAL_POSTS'				=> 'Les postes totales dans ce condens�:',
	'DIGEST_TOTAL_UNREAD_PRIVATE_MESSAGES'	=> 'Les messages priv�s, non lus et totaux:',
	'DIGEST_UNREAD'						=> 'Non lu',
	'DIGEST_UPDATED'					=> 'Vos cadres de condens� ont �t� �pargn�s',
	'DIGEST_USE_BOOKMARKS'				=> 'Les sujets de Bookmarked seulement',
	'DIGEST_VERSION_NOT_UP_TO_DATE'		=> 'La Notification d`administrateur : cette version de Condens�s de phpBB n`est pas actuelle. Les mises � jour sont disponibles sur le <a href="%s">Dig�re le site internet</a>.',
	'DIGEST_VERSION_UP_TO_DATE'			=> 'Cette version de Condens�s de phpBB est � jour, aucune mise � jour disponible.',
	'DIGEST_WEEKDAY' => array(
		0	=> 'Dimanche',
		1 	=> 'Le lundi',
		2	=> 'Mardi',
		3	=> 'Mercredi',
		4	=> 'Jeudi',
		5	=> 'Vendredi',
		6	=> 'Samedi'),
	'DIGEST_WEEKLY'						=> 'Hebdomadaire',
	'DIGEST_YOU_HAVE_PRIVATE_MESSAGES' 	=>	'Vous avez des messages priv�s',
	'DIGEST_YOUR_DIGEST_OPTIONS' 		=> 'Vos options de condens�:',
	'S_DIGEST_TITLE'					=> $config['digests_digests_title'],
	'UCP_DIGESTS_MODE_ERROR'			=> 'Les condens�s ont �t� appel�s avec un mode nul de %s',
				
));
			
?>