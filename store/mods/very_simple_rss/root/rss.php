<?php
/**
*
* @package Very Simple RSS php code
* @version 1.0.0 of 06.04.2009
* @copyright (c) By Shapoval Andrey Vladimirovich (AllCity) ~ http://allcity.net.ru/
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

// Custom Page code from http://www.phpbb.com/kb/article/add-a-new-custom-page-to-phpbb/
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path.'common.'.$phpEx);

// Start session management code from http://www.phpbb.com/kb/article/add-a-new-custom-page-to-phpbb/
$user->session_begin();
$auth->acl($user->data);
$user->setup();

// Cache function
function Very_Simple_RSS_SQL()
{
  global $db, $cache, $user;

  // Here you can set the default number of topics to load (from 1 to 100 recommend)
  $topics_page_limit = 20;

  // Here you can set the default time to reload cache in minuts (from 1 to 120 recommend)
  $cache_time_limit = 15;

    // Try finding the data in the cacge
    if(($rss_sql_data = $cache->get('_very_simple_rss_'.str_replace('-', '_', $user->lang['USER_LANG']).'_sql')) === false)
    {

      $rss_sql = array(
      'SELECT' => 't.topic_id, t.forum_id, t.topic_poster, t.topic_last_post_subject, t.topic_title, t.topic_last_post_id, t.topic_last_poster_id, t.topic_last_post_time, t.topic_replies, t.topic_views, t.topic_first_poster_name, t.topic_last_poster_name',
      'FROM' => array(TOPICS_TABLE => 't'),
      'ORDER_BY' => 'topic_last_post_time DESC'
      );
      $result = $db->sql_query_limit($db->sql_build_query('SELECT', $rss_sql), $topics_page_limit);
      while($row = $db->sql_fetchrow($result))
      {
        $rss_sql_data[] = array(
        'topic_id' => $row['topic_id'],
        'forum_id' => $row['forum_id'],
        'topic_poster' => $row['topic_poster'],
        'topic_last_post_subject' => $row['topic_last_post_subject'],
        'topic_title' => $row['topic_title'],
        'topic_last_post_id' => $row['topic_last_post_id'],
        'topic_last_poster_id' => $row['topic_last_poster_id'],
        'topic_last_post_time' => $row['topic_last_post_time'],
        'topic_replies' => $row['topic_replies'],
        'topic_views' => $row['topic_views'],
        'topic_first_poster_name' => $row['topic_first_poster_name'],
        'topic_last_poster_name' => $row['topic_last_poster_name']
        );
      }
      $db->sql_freeresult($result);

    // cache this data, this improves performance!
    $cache->put('_very_simple_rss_'.str_replace('-', '_', $user->lang['USER_LANG']).'_sql', $rss_sql_data, 60*$cache_time_limit);
    }

  // Return data
  return $rss_sql_data;
}

// Data function
function Very_Simple_RSS_DATA()
{
  global $phpEx, $config, $user;

  // Language file
  $user->add_lang('mods/very_simple_rss');

    $sql_cache = Very_Simple_RSS_SQL();

      foreach($sql_cache as $row)
      {
        echo "<item>\r\n";
        echo "<title>".$row['topic_last_post_subject']."</title>\r\n";
        echo "<link>".generate_board_url()."/viewtopic.".$phpEx."?p=".$row['topic_last_post_id']."#p".$row['topic_last_post_id']."</link>\r\n";
        echo "<description><![CDATA[".$user->lang['RSS_TOPIC_TITLE'].": <a href=\"".generate_board_url()."/viewtopic.".$phpEx."?f=".$row['forum_id']."&amp;t=".$row['topic_id']."\">".$row['topic_title']."</a><br />".$user->lang['RSS_AUTHOR_TOPIC'].": <a href=\"".generate_board_url()."/memberlist.".$phpEx."?mode=viewprofile&amp;u=".$row['topic_poster']."\">".$row['topic_first_poster_name']."</a><br />".$user->lang['RSS_AUTHOR_POST'].": <a href=\"".generate_board_url()."/memberlist.".$phpEx."?mode=viewprofile&amp;u=".$row['topic_last_poster_id']."\">".$row['topic_last_poster_name']."</a><br />".$user->lang['RSS_ANSWERS'].": ".$row['topic_replies']."<br />".$user->lang['RSS_VIEWS'].": ".$row['topic_views']."]]></description>\r\n";
        // pubDate in RFC 2822 format
        echo "<pubDate>".date('D, d M Y H:i:s O',$row['topic_last_post_time'])."</pubDate>\r\n";
        echo "<guid>".generate_board_url()."/viewtopic.".$phpEx."?p=".$row['topic_last_post_id']."#p".$row['topic_last_post_id']."</guid>\r\n";
        echo "</item>\r\n";
      }

  // Return function
  return true;
}

// This xhtml and xml file content!
header('Content-Type: application/xhtml+xml; charset=UTF-8');

// RSS header...
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"\x3F>\r\n";
echo "<rss version=\"2.0\">\r\n";
echo "<channel>\r\n";
echo "<title>".$config['sitename']." - RSS</title>\r\n";
echo "<link>".generate_board_url()."/</link>\r\n";
echo "<description>".$config['site_desc']."</description>\r\n";
echo "<language>".$user->lang['USER_LANG']."</language>\r\n";
echo "<generator>phpBB3 Very Simple RSS by AllCity</generator>\r\n";

// RSS body...
Very_Simple_RSS_DATA();

// RSS footer...
echo "</channel>\r\n";
echo "</rss>";

?>