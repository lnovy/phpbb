<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $id: install.php - 9151 11-26-2008 16:07:48 - 2.0.RC2 dcz $
* @copyright (c) 2006 - 2008 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* install [Czech]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	// Install
	'SEO_INSTALL_PANEL'	=> 'GYM Sitemaps &amp; RSS Instalační Panel',
  'CAT_INSTALL_GYM_SITEMAPS'   => 'Instalovat GYM Sitemaps',
  'CAT_UNINSTALL_GYM_SITEMAPS' => 'Odinstalovat GYM Sitemaps',
  'CAT_UPDATE_GYM_SITEMAPS'    => 'Aktualizovat GYM Sitemaps',
	'SEO_ERROR_INSTALL'	=> 'Nastala chyba během instalace. Pokud chcete opakovat instalaci, proveďte nejdříve odinstalaci.',
	'SEO_ERROR_INSTALLED'	=> '%s mód je již nainstalován.',
	'SEO_ERROR_ID'	=> '%1$ mód nemá žádné ID.',
	'SEO_ERROR_UNINSTALLED'	=> '%s mód je již odinstalován.',
	'SEO_ERROR_INFO'	=> 'Informace :',
	'SEO_FINAL_INSTALL_GYM_SITEMAPS'	=> 'Příhlásit se do ACP',
	'SEO_FINAL_UPDATE_GYM_SITEMAPS'	=> 'Příhlásit se do ACP',
	'SEO_FINAL_UNINSTALL_GYM_SITEMAPS'	=> 'Návrat na obsah fóra',
	'SEO_OVERVIEW_TITLE'	=> 'Přehled GYM Sitemaps &amp; RSS',
	'SEO_OVERVIEW_BODY'	=> '<p>Vítejte v phpBB3 SEO GYM sitemaps &amp; RSS %1$s instalátoru.</p><p>Prosím, čtěte <a href="%3$s" title="Zkontrolovat vlákno o vydání" target="_phpBBSEO"><b>vlákno o vydání</b></a> pro více informací</p><p><strong style="text-transform: uppercase;">Poznámka:</strong> Musíte mít již provedené dané změny v kódu a všechny nové soubory nahrané na FTP, předtím než budete pokračovat v průvodci instalací.</p><p>Tento instalátor vás provede procesem instalace GYM sitemaps &amp; RSS administračním kontrolním panelem (ACP). To vám dovoli generovat SEO optimalizované Google Sitemaps a RSS zdroje. Sestavení tohoto módu vám dovolí generovat Google Sitemaps a RSS zdroje pro jakoukoliv php/SQL aplikaci nainstalovanou na vašich stránkách, použitím specializovaných pluginů. Pro návrhy, týkající se GYM Sitemaps &amp; RSS mód, navštivte <a href="%3$s" title="Fórum podpory" target="_phpBBSEO"><b>fórum podpory</b></a>.</p> ',
	'CAT_SEO_PREMOD'	=> 'GYM Sitemaps &amp; RSS',
	'SEO_INSTALL_INTRO'		=> 'Vítejte v phpBB3 SEO GYM Sitemaps &amp; RSS instalátoru.',
	'SEO_INSTALL_INTRO_BODY'	=> '<p>Chystáte se nainstalovat mód %1$s %2$s. Tento instalační nástroj aktivuje administrační ovládací panel GYM Sitemaps &amp; RSS v phpBB3 ACP.</p><p>Po instalaci musíte v ACP zvolit vhodné nastavení.</p>
	<p><strong>Poznámka:</strong> Pokud tento mód instalujete poprvé, velmi doporučujeme, abyste otestovali veškeré nastavení na lokálním nebo privátním serveru, předtím než ho zpřístupníte online.</p><br/>
	<p>Požadavky :</p>
	<ul>
		<li>Apache server (Linux OS) s mod_rewrite pro přepisování URL odkazů.</li>
		<li>IIS server (Windows OS) s isapi_rewrite pro přepisování URL odkazů, ale musíte přizpůsobit přepisovací pravidla v httpd.ini</li>
	</ul>',
	'SEO_INSTALL'		=> 'Instalovat',
	'UN_SEO_INSTALL_INTRO'		=> 'Vítejte v odinstalátoru GYM Sitemaps &amp; RSS',
	'UN_SEO_INSTALL_INTRO_BODY'	=> '<p>Chystáte se odinstalovat mód %1$s %2$s.</p>
	<p><strong>Poznámka:</strong> GYM Sitemaps a RSS nebudou dostupné, jakmile tento mód odinstalujete.</p>',
	'UN_SEO_INSTALL'		=> 'Odinstalovat',
	'SEO_INSTALL_CONGRATS'		=> 'Blahopřejeme!',
	'SEO_INSTALL_CONGRATS_EXPLAIN'	=> '<p>Úspěšně jste nainstaloval mód %1$s %2$s. Jděte do phpBB3 ACP a pokračujte s nastavením módu.<p>
	<p>Zobrazí se v phpBB3 SEO kategorii. Kromě mnoha jiných věcí budete moci :</p>
	<h2>Přesně nastavit vaše Google Sitemaps a RSS zdroje/h2>
	<p>Google Sitemaps a RSS zdroje podporují pokročilé XSLT stylizování, phpBB3 CSS bude použito na tyto styly bez nutnosti editovat jediný řádek kódu.</p>
	<p>Google Sitemaps a RSS zdroje budou automaticky detekovat phpBB3 SEO mod rewrite a jeho nastavení; používání jiného typu URL mod rewrite je tak mnohem jednodušší.</p>
	<!--<h2>Generovat individuální .htaccess</h2>
	<p>Jakmile budete mít volby výše nastavené, můžete generovat svůj .htaccess a uložit ho přímo na server.</p> -->',
	'UN_SEO_INSTALL_CONGRATS'	=> 'Módy ACP GYM Sitemaps &amp; RSS byly odstraněny.',
	'UN_SEO_INSTALL_CONGRATS_EXPLAIN'	=> '<p>Nyní jste úspěšně odstranili mód %1$s %2$s.<p>
	<p> Vaše Google Sitemaps a RSS zdroje nejsou přístupné.</p>',
	'SEO_VALIDATE_INFO'	=> 'Validační Info :',
	'SEO_LICENCE_TITLE'	=> 'GNU LESSER GENERAL PUBLIC LICENSE',
	'SEO_LICENCE_BODY'	=> 'phpBB3 SEO GYM Sitemaps &amp; RSS je vydáváno pod GNU LESSER GENERAL PUBLIC LICENSE.',
	'SEO_SUPPORT_TITLE'	=> 'Podpora',
	'SEO_SUPPORT_BODY'	=> 'Plná podpora bude podávána v <a href="%1$s" title="Navštivte %2$s fórum" target="_phpBBSEO"><b>%2$s fóru</b></a>. Poskytneme odpovědi na větsinu dotazů, týkajících se konfiguračních problémů a podpory pro odhalování běžných chyb.</p><p>Určitě navštivte naše <a href="http://www.phpbb-seo.com/boards/" title="SEO Fórum" target="_phpBBSEO"><b>Search Engine Optimization (optimalizace pro vyhledávače) fóra</b></a>.</p><p>Měli byste se <a href="http://www.phpbb-seo.com/boards/profile.php?mode=register" title="Registrovat na phpBB SEO" target="_phpBBSEO"><b>zaregistrovat</b></a> a přihlásit jak na fórum, tak <a href="%3$s" title="Upozornit na aktualizace" target="_phpBBSEO"><b>k vláknu o vydáních</b></a> a být tak upozorňován emailem na každou aktualizaci.',
	// Security
	'SEO_LOGIN'		=> 'Fórum vyžaduje, abyste se zaregistrovali a přihlásili pro zobrazení této stránky.',
	'SEO_LOGIN_ADMIN'	=> 'Fórum vyžaduje, abyste se přihlásili jako administrátor pro zobrazení této stránky.<br/>Vaše session byla smazána z bezpečnostních důvodů.',
	'SEO_LOGIN_FOUNDER'	=> 'Fórum vyžaduje, abyste se přihlásili jako zakladatel pro zobrazení této stránky.',
	'SEO_LOGIN_SESSION'	=> 'Kontrola Session neuspěla.<br/>Nastavení nebude nahrazeno.<br/>Vaše session byla smazána z bezpečnostních důvodů.',
	// Cache status
	'SEO_CACHE_FILE_TITLE'	=> 'Stav cache',
	'SEO_CACHE_STATUS'	=> 'Adresář cache je nastaven : <b>%s</b>',
	'SEO_CACHE_FOUND'	=> 'Adresář cache byl úspěšně nalezen.',
	'SEO_CACHE_NOT_FOUND'	=> 'Adresář cache nebyl nalezen.',
	'SEO_CACHE_WRITABLE'	=> 'Adresář cache je zapisovatelný.',
	'SEO_CACHE_UNWRITABLE'	=> 'V adresáři cache není povolen zápis. Musíte natavit CHMOD adresáře na 0777.',
	'SEO_CACHE_FORUM_NAME'	=> 'Název fóra',
	'SEO_CACHE_URL_OK'	=> 'URL odkaz byl uložen do cache',
	'SEO_CACHE_URL_NOT_OK'	=> 'Tento URL odkaz nebyl uložen do cache',
	'SEO_CACHE_URL'		=> 'Konečný URL odkaz',
	'SEO_CACHE_MSG_OK'	=> 'Cache byla úspěšně aktualizována.',
	'SEO_CACHE_MSG_FAIL'	=> 'Nastala chyba při aktualizaci souboru v cache.',
	'SEO_CACHE_UPDATE_FAIL'	=> 'URL odkaz, který požadujete, nemůže být použit. Cache nebude aktualizována.',
	// Update
	'UPDATE_SEO_INSTALL_INTRO'		=> 'Welcome to the phpBB SEO GYM sitemaps &amp; RSS updater.',
	'UPDATE_SEO_INSTALL_INTRO_BODY'	=> '<p>You are about to update the %1$s module to %2$s. This script will update the phpBB data base.<br/>Your current settings won’t be affected.</p>
	<p><strong>Note:</strong> This script will not update GYM Sitemaps &amp; RSS physical files.<br/><br/>To update from all 2.0.x (phpBB3) versions you <b>must</b> upload all files in the <b>root/</b> directory of the archive to your phpBB/ ftp directory, after you will have taken care manually of the eventual code change you would have implemented in the template files (directory phpBB/styles/, .html, .js and .xsl) added by the module.<br/><br/>You <b>can</b> restart this update script when you want, for example if you did not upload the required files or simply to display the update code changes for phpBB3 files again.</p>',
	'UPDATE_SEO_INSTALL'		=> 'Update',
	'SEO_ERROR_NOTINSTALLED'	=> 'GYM Sitemaps &amp; RSS is not installed!',
	'SEO_UPDATE_CONGRATS_EXPLAIN'	=> '<p>You have now successfully updated %1$s to %2$s.<p>
	<p><strong>Note:</strong> This script does not update GYM Sitemaps &amp; RSS physical files.</p><br/><b>Please</b> implement the code changes listed bellow.<br/><h3>Update report :</h3>',
));
?>