<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $id: gym_common.php - 5146 12-02-2008 10:58:27 - 2.0.RC2 hroudel $
* @copyright (c) 2006 - 2008 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
* Original translation: 11-20-2008 14:38:27 - 2.0.RC1 dcz $
*
*/
/**
*
* gym_common [Česky]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'RSS_AUTH_SOME_USER' => '<b><u>Varování :</u></b>Tento seznam položek je individualizovaný dle <b>%s</b>/vo oprávněním.<br/>Některé položky se nemusí zobrazit, dokud se nepřihlásíte.',
	'RSS_AUTH_THIS_USER' => '<b><u>Varování :</u></b>Tato položka je individualizovaná dle <b>%s</b>/vo oprávněním.<br/>Nebude zobrazena, dokud se nepřihlásíte.',
	'RSS_AUTH_SOME' => '<b><u>Varování :</u></b>Tento seznam položek není veřejný.<br/>Některé položky se nemusí zobrazit, dokud se nepřihlásíte.',
	'RSS_AUTH_THIS' => '<b><u>Varování :</u></b>Tato položka není veřejná.<br/>Nebude zobrazena, dokud se nepřihlásíte.',
	'RSS_CHAN_LIST_TITLE' => 'Přehled RSS',
	'RSS_CHAN_LIST_DESC' => 'Tento přehled kanálů je souhrnem dostupných RSS zdrojů.',
	'RSS_CHAN_LIST_DESC_MODULE' => 'Tento přehled kanálů je souhrnem RSS zdrojů dostupných pro : %s.',
	'RSS_ANNOUCES_DESC' => 'Tyto zdroje jsou souhrnem všech globálních oznámení : %s',
	'RSS_ANNOUNCES_TITLE' => 'Oznámit z  : %s',
	'GYM_LAST_POST_BY' => 'Poslední příspěvek od ',
	'GYM_FIRST_POST_BY' => 'Příspěvek od ',
	'GYM_LINK' => 'Odkaz',
	'GYM_SOURCE' => 'Zdroj',
	'GYM_RSS_SOURCE' => 'Zdroj',
	'RSS_MORE' => 'více',
	'RSS_CHANNELS' => 'Kanály',
	'RSS_CONTENT' => 'Obsah',
	'RSS_SHORT' => 'Stručný seznam',
	'RSS_LONG' => 'Podrobný seznam',
	'RSS_NEWS' => 'Novinky',
	'RSS_NEWS_DESC' => 'Poslední novinky z',
	'RSS_REPORTED_UNAPPROVED' => 'Tato položka momentálně čeká na schválení.',

	'GYM_HOME' => 'Obsah fóra',
	'GYM_FORUM_INDEX' => 'Obsah fóra',
	'GYM_LASTMOD_DATE' => 'Datum poslední změny',
	'GYM_SEO' => 'Search Engine Optimization (optimalizace pro vyhledávače)',
	'GYM_MINUTES' => 'minut(a/y)',
	'GYM_SQLEXPLAIN' => 'detailní SQL zprávu',
	'GYM_SQLEXPLAIN_MSG' => 'Přihlášený jako admin, můžete zkontrolovat %s pro tuto stránku.',
	'GYM_BOOKMARK_THIS' => 'Vložit záložku',
	// Errors
	'GYM_ERROR_404' => 'Tato stránka neexistuje anebo není aktivována',
	'GYM_ERROR_404_EXPLAIN' => 'Server nenašel žádnou stránku odpovídající URL odkazu, který jste použil.',
	'GYM_ERROR_401' => 'Nemáte oprávnění pro zobrazení této stránky.',
	'GYM_ERROR_401_EXPLAIN' => 'Tato stránka je přístupná jen přihlášeným uživatelům s požadovanými oprávněními.',
	'GYM_LOGIN' => 'Musíte být registrovaný a přihlášený pro zobrazení této stránky.',
	'GYM_LOGIN_EXPLAIN' => 'You must be registered and logged in to view this page.',
	'GYM_TOO_FEW_ITEMS' => 'Page Unavailable',
	'GYM_TOO_FEW_ITEMS_EXPLAIN' => 'This page does not contain enough item to be displayed.',
	'GYM_TOO_FEW_ITEMS_EXPLAIN_ADMIN' => 'This page source is either empty or does not contain enough items (less than the configured threshold in ACP) to be displayed.<br/>A 404 Not Found header was sent to properly inform Search Engines to discard this link.',

	'GOOGLE_SITEMAP' => 'Google Sitemap',
	'GOOGLE_SITEMAP_OF' => 'Google Sitemap',
	'GOOGLE_MAP_OF' => 'Google Sitemap %1$s',
	'GOOGLE_SITEMAPINDEX' => 'Obsah Google Sitemaps',
	'GOOGLE_NUMBER_OF_SITEMAP' => 'Počet sitemaps v obsahu Google Sitemaps',
	'GOOGLE_NUMBER_OF_URL' => 'Počet URL odkazů v této Google Sitemap',
	'GOOGLE_SITEMAP_URL' => 'URL odkaz Google Sitemap',
	'GOOGLE_CHANGEFREQ' => 'Frek. změny',
	'GOOGLE_PRIORITY' => 'priorita',

	'RSS_FEED' => 'RSS',
	'RSS_FEED_OF' => 'RSS zdroj %1$s',
	'RSS_2_LINK' => 'RSS 2.0 odkaz',
	'RSS_UPDATE' => 'Aktualizace',
	'RSS_LAST_UPDATE' => 'Poslední aktualizace',
	'RSS_SUBSCRIBE_POD' => '<h2>Vložit nyní záložku tohoto zdroje!</h2>S vaší preferovanou službou.',
	'RSS_SUBSCRIBE' => 'Pro přihlášení k ručnímu odebírání RSS zdroje, prosím použijte následující URL odkaz :',
	'RSS_ITEM_LISTED' => 'Jedna položka k zobrazení.',
	'RSS_ITEMS_LISTED' => 'položek k zobrazení.',
	'RSS_VALID' => 'RSS 2.0 validní zdroj',

	// Old URL handling
	'RSS_1XREDIR' => 'RSS bylo přesunuto',
	'RSS_1XREDIR_MSG' => 'RSS bylo přesunuto, nyní se nachází na této adrese',
	// HTML sitemaps
	'HTML_MAP' => 'Mapa stránek',
	'HTML_MAP_OF' => 'Mapa %1$s',
	'HTML_MAP_NONE' => 'Žádná mapa stránek',
	'HTML_NO_ITEMS' => 'Žádná položka',
	'HTML_NEWS' => 'Novinky',
	'HTML_NEWS_OF' => 'Novinky %1$s',
	'HTML_NEWS_NONE' => 'Žádné novinky',
	'HTML_PAGE' => 'Stránka',
	'HTML_MORE' => 'Celý příspěvek',
	// Forum
	'HTML_FORUM_MAP' => 'Mapa fóra',
	'HTML_FORUM_NEWS' => 'Novinky fóra',
	'HTML_FORUM_GLOBAL_MAP' => 'Seznam globálních oznámení',
	'HTML_FORUM_GLOBAL_NEWS' => 'Globální oznámení',
	'HTML_FORUM_ANNOUNCE_MAP' => 'Seznam oznámení',
	'HTML_FORUM_ANNOUNCE_NEWS' => 'Oznámení',
	'HTML_FORUM_STICKY_MAP' => 'Seznam "Důležité"',
	'HTML_FORUM_STICKY_NEWS' => '"Důležité"',
	'HTML_LASTX_TOPICS_TITLE' => 'Poslední(ch) %1$s aktivní(ch) témat(a)',
));
?>