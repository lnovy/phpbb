<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $id: gym_html.php - 13369 11-20-2008 14:38:27 - 2.0.RC1 dcz $
* @copyright (c) 2006 - 2008 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* gym_html [English]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'HTML_MAIN' => 'HTML',
	'HTML_MAIN_EXPLAIN' => 'Toto je hlavní nastavení pro HTML modul.<br/>Nastavení může být použito pro všechny HTML moduly v závoslosti na vynuceném nastavení.',
	// Reset settings
	'HTML_ALL_RESET' => 'ALL HTML modules',
	// Limits
	'HTML_RSS_NEWS_LIMIT' => 'Mains news page limit',
	'HTML_RSS_NEWS_LIMIT_EXPLAIN' => 'Number of items displayed on the main news page, gathered from the configured RSS source for the main new page.',
	'HTML_MAP_TIME_LIMIT' => 'Time limit for module main maps',
	'HTML_MAP_TIME_LIMIT_EXPLAIN' => 'Limit in days. The maximum age of the items taken into account when building the module main map page. Can be very useful to lower the server load on large data bases. Enter 0 for no limit',
	'HTML_CAT_MAP_TIME_LIMIT' => 'Time limit for category maps',
	'HTML_CAT_MAP_TIME_LIMIT_EXPLAIN' => 'Limit in days. The maximum age of the items taken into account when building the module category map pages. Can be very useful to lower the server load on large data bases. Enter 0 for no limit',
	'HTML_NEWS_TIME_LIMIT' => 'Time limits for News',
	'HTML_NEWS_TIME_LIMIT_EXPLAIN' => 'Limit in days. The maximum age of the items taken into account when building the module news page. Can be very useful to lower the server load on large data bases. Enter 0 for no limit',
	'HTML_CAT_NEWS_TIME_LIMIT' => 'Time limit for category news',
	'HTML_CAT_NEWS_TIME_LIMIT_EXPLAIN' => 'Limit in days. The maximum age of the items taken into account when building the module category news pages. Can be very useful to lower the server load on large data bases. Enter 0 for no limit',
	// sort
	'HTML_MAP_SORT_TITLE' => 'Map sorting',
	'HTML_NEWS_SORT_TITLE' => 'News sorting',
	'HTML_CAT_SORT_TYPE' => 'Sorting for category maps',
	'HTML_CAT_SORT_TYPE_EXPLAIN' => 'Following the same principle as above, this one applies to the module category maps pages, eg a forum map for the HTML forum module.',
	'HTML_NEWS_SORT_TYPE' => 'Sorting for news page',
	'HTML_NEWS_SORT_TYPE_EXPLAIN' => 'Following the same principle as above, this one applies to the module news page, eg the forum news page for the HTML forum module.',
	'HTML_CAT_NEWS_SORT_TYPE' => 'Sorting for category news pages',
	'HTML_CAT_NEWS_SORT_TYPE_EXPLAIN' => 'Following the same principle as above, this one applies to the module category news pages, eg a forum news page for the HTML forum module.',
	'HTML_PAGINATION_GEN' => 'Main Pagination',
	'HTML_PAGINATION_SPEC' => 'Module Pagination',
	'HTML_PAGINATION' => 'Site map pagination',
	'HTML_PAGINATION_EXPLAIN' => 'Activate pagination on the site map pages. You can decide to use only one, or several pages for your site maps.',
	'HTML_PAGINATION_LIMIT' => 'Item per page',
	'HTML_PAGINATION_LIMIT_EXPLAIN' => 'When site map pagination is activated, you can choose how many item to display per page.',
	'HTML_NEWS_PAGINATION' => 'News Pagination',
	'HTML_NEWS_PAGINATION_EXPLAIN' => 'Activate pagination on the news pages. You can decide to use only one, or several pages for your news pages.',
	'HTML_NEWS_PAGINATION_LIMIT' => 'News per page',
	'HTML_NEWS_PAGINATION_LIMIT_EXPLAIN' => 'When news pagination is activated, you can choose how many news to display per page.',
	'HTML_ITEM_PAGINATION' => 'Item pagination',
	'HTML_ITEM_PAGINATION_EXPLAIN' => 'You can here decide to output paginated links (when available) for the listed items. For example, the module can additionally output links of the forum’s topic pages.',
	// Basic settings
	'HTML_SETTINGS' => 'Základní nastavení',
	'HTML_C_INFO' => 'Copyright informace',
	'HTML_C_INFO_EXPLAIN' => 'Zobrazit informace o autorovi na stránkách sitemap a novinek. Základní je phpBB. Tato informace bude použita pouze pokud máte nainstalovaný phpBB SEO MOD.',
	'HTML_SITENAME' => 'Název fóra',
	'HTML_SITENAME_EXPLAIN' => 'Název fóra je zobrazen na stránkách sitemap a novinek. phpBB název fóra je nastaven jako základní.',
	'HTML_SITE_DESC' => 'Popis fóra',
	'HTML_SITE_DESC_EXPLAIN' => 'Popis fóra je zobrazen na stránkách sitemap a novinek. phpBB popis fóra je nastaven jako základní.',
	'HTML_LOGO_URL' => 'Logo webu',
	'HTML_LOGO_URL_EXPLAIN' => 'Obrázek bude použit pro logo v RSS, uloženo v adresáři gym_sitemaps/images/.',
	'HTML_URL' => 'HTML URL',
	'HTML_URL_EXPLAIN' => 'Vložte celou URL k souboru map.php, př. http://www.example.com/adresar/ pokud je map.php nainstalován v http://www.example.com/adresar/.<br/>
	Tato možnost je tu pro případ, kdyby soubor map.php byl uložen jinde než je kořenová složka fóra.',
	'HTML_RSS_NEWS_URL' => 'Mains news page RSS source',
	'HTML_RSS_NEWS_URL_EXPLAIN' => 'Enter here the full url to the RSS feed you want to display on the main news page, exemple http://www.example.com/gymrss.php?news&amp;digest to display all news from all RSS modules installed on the main HTML news page.<br />You can use an RSS 2.0 feed as a source for this page.',
	'HTML_STATS_ON_NEWS' => 'Zobrazit statistiky fóra v novinkách',
	'HTML_STATS_ON_NEWS_EXPLAIN' => 'Volba pro zobrazení statistik fóra na stránkách novinek.',
	'HTML_STATS_ON_MAP' => 'Zobrazit mapy statistik fóra',
	'HTML_STATS_ON_MAP_EXPLAIN' => 'Volba pro zobrazení stránek statistik fóra.',
	'HTML_BIRTHDAYS_ON_NEWS' => 'Display birthdays on news pages',
	'HTML_BIRTHDAYS_ON_NEWS_EXPLAIN' => 'Display, or not, birthdays on news pages.',
	'HTML_BIRTHDAYS_ON_MAP' => 'Display birthdays on news pages',
	'HTML_BIRTHDAYS_ON_MAP_EXPLAIN' => 'Display, or not, birthdays on news pages.',
	'HTML_DISP_ONLINE' => 'Display user online',
	'HTML_DISP_ONLINE_EXPLAIN' => 'Display, or not, the user online list on the site map and news pages.',
	'HTML_DISP_TRACKING' => 'Activate tracking',
	'HTML_DISP_TRACKING_EXPLAIN' => 'Activate, or not, item tracking (read / unread).',
	'HTML_DISP_STATUS' => 'Activate status',
	'HTML_DISP_STATUS_EXPLAIN' => 'Activate, or not, the item status system (Annoncement, Stickies, locked etc ... ).',
	// Cache
	'HTML_CACHE' => 'Cache',
	'HTML_CACHE_EXPLAIN' => 'You can here define various caching opions for the HTML mode. HTML caching is separated from the other modes (Google and RSS). This module uses the standard phpBB’s cache.<br/>This options thus cannot be inherited from the main  level, and only pblicly visible content will be cached. This settings though, may be trasnmitted to the HTML momdules depending on your HTML override settings.<br/><br/>Cache is separated into two types, one for each column in the output : The main column, containing the maps and news, and the optional one, which for example can be used to add a last active topic listing in the HTML forum module.',
	'HTML_MAIN_CACHE_ON' => 'Activate main column caching',
	'HTML_MAIN_CACHE_ON_EXPLAIN' => 'You can here activate / deactivate the site maps and news column caching.',
	'HTML_OPT_CACHE_ON' => 'Activate optional column caching',
	'HTML_OPT_CACHE_ON_EXPLAIN' => 'You can here activate / deactivate the optional column caching.',
	'HTML_MAIN_CACHE_TTL' => 'Durée de vie du cache du contenu principal',
	'HTML_MAIN_CACHE_TTL_EXPLAIN' => 'Nombre maximal d’heures pendant lesquelles un fichier en cache sera utilisé avant d’être mis à jour. Si cette durée est atteinte le fichier en cache sera rafraîchi lorsque quelqu’un y fera appel.',
	'HTML_OPT_CACHE_TTL' => 'Cache duration',
	'HTML_OPT_CACHE_TTL_EXPLAIN' => 'Maximum amount of hours a cached file will be used before it will be updated. Each cached file will be updated everytime someone will browse it after this duration was exeeded when auto regen is on. If not, the cache will only be updated upon demand in ACP.',
	// Auth settings
	'HTML_AUTH_SETTINGS' => 'Oprávnění',
	'HTML_ALLOW_AUTH' => 'Oprávnění',
	'HTML_ALLOW_AUTH_EXPLAIN' => 'Activate the authorization for site map and news pages. If activated, logged in users will be able to browse private content and to view items from private forums if they have the proper authorization.',
	'HTML_ALLOW_NEWS' => 'Povolit novinky',
	'HTML_ALLOW_NEWS_EXPLAIN' => 'Each module can have a news page lisitng the last X active items with their content, which can be filtered. For the forum, the forum news page is generally a page displaying the 10 last topic first posts digest coming from a selection of public and / or private forums.',
	'HTML_ALLOW_CAT_NEWS' => 'Povolit novinky kategorií',
	'HTML_ALLOW_CAT_NEWS_EXPLAIN' => 'Following the same principles as the module news pages, each module categroy can have a news page.',
	// Content
	'HTML_NEWS' => 'Obsah',
	'HTML_NEWS_EXPLAIN' => 'Zde můžete nastavit různé možnosti formátování a filtrování obsahu.<br />Mohou být použity na všechny RSS moduly v závislosti na vašem vynuceném nastavení RSS.',
	'HTML_NEWS_CONTENT' => 'Základní nastavení',
	'HTML_SUMARIZE' => 'Výběr položek',
	'HTML_SUMARIZE_EXPLAIN' => 'Sumarizace obsahu zprávy pro feeds.<br/>Maximální počet vět, slov nebo znaků odpovídající nastavení níže. 0 pro zobrazení všeho.',
	'HTML_SUMARIZE_METHOD' => 'Metoda výběru',
	'HTML_SUMARIZE_METHOD_EXPLAIN' => 'Výběr ze tří možností zobrazení výběru.<br/> Počet řádek, slov nebo znaků. Značky BBcode nebudou poškozeny.',
	'HTML_ALLOW_PROFILE' => 'Zobrazit profily',
	'HTML_ALLOW_PROFILE_EXPLAIN' => 'Autor tématu bude zobrazen v RSS feed.',
	'HTML_ALLOW_PROFILE_LINKS' => 'Link profilu',
	'HTML_ALLOW_PROFILE_LINKS_EXPLAIN' => 'Pokud je autor zobrazen v příspěvku, můžete se rozhodnout bude li uveden odkaz na profil autora.',
	'HTML_ALLOW_BBCODE' => 'Povolit BBcodes',
	'HTML_ALLOW_BBCODE_EXPLAIN' => 'Ve výstupu bude použito BBCode.',
	'HTML_STRIP_BBCODE' => 'Vyjmout BBcodes',
	'HTML_STRIP_BBCODE_EXPLAIN' => 'Můžete nastavit, které BBCode nebudou použity.<br/>Formát:<br/><ul><li><u>Čárkou oddělený seznam bbcodů:</u> Odstranění bbcode, zachová obsah.<br/><u>Příklad:</u> <b>img,b,quote</b><br/>V tomto příkladu nebudou bbcode img, bold a quote bbcode parsovány, značky bbcode budou odstraněny při zachování obsahu.</li><li><u>Čárkou oddělený seznam s možností výmazu:</u> Rozhodnout o použití bbcode.<br /><u>Příklad:</u> <b>img:1,b:0,quote,code:1</b> <br/> BBcode img bude celý smazán, b - místo tučného písma bude pouze normální, quote - bude zachován pouze obsah, code - bude odstraněno včetně obsahu.</li></ul>',
	'HTML_ALLOW_LINKS' => 'Povolit aktivní odkazy',
	'HTML_ALLOW_LINKS_EXPLAIN' => 'Vybrat pokud si přejete aktivní odkazy v obsahu.<br/>Při neaktivní volbě, budou v obsahu zobrazeny linky a emaily bez možnosti odkazu.',
	'HTML_ALLOW_EMAILS' => 'Povolit Emaily',
	'HTML_ALLOW_EMAILS_EXPLAIN' => 'Vybrat pokud chcete email ve tvaru "email AT domain DOT com" místo "email@domain.com" v obsahu položek.',
	'HTML_ALLOW_SMILIES' => 'Povolit smajlíky',
	'HTML_ALLOW_SMILIES_EXPLAIN' => 'Vyberte pokud chcete zobrazit smajlíky v obsahu.',
	'HTML_ALLOW_SIG' => 'Allow signatures',
	'HTML_ALLOW_SIG_EXPLAIN' => 'You may choose here to either display or not the users signatures in content.',
	'HTML_ALLOW_MAP' => 'Aktivovat tento modul',
	'HTML_ALLOW_MAP_EXPLAIN' => 'Povolit tento modul.',
	'HTML_ALLOW_CAT_MAP' => 'Activate module category maps',
	'HTML_ALLOW_CAT_MAP_EXPLAIN' => 'You can here activate / deactivate the module category maps.',
));
?>