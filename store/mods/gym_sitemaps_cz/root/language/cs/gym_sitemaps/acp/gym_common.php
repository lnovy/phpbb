<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $id: gym_common.php - 18560 11-20-2008 14:38:27 - 2.0.RC1 dcz $
* @copyright (c) 2006 - 2008 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* gym_common [English]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	// Main
	'ALL' => 'Vše',
	'MAIN' => 'GYM Sitemap',
	'MAIN_MAIN_RESET' => 'Hlavní možnosti GYM sitemap',
	'MAIN_MAIN_RESET_EXPLAIN' => 'Obnovit všechna hlavní nastavení GYM na původní hodnoty.',
	// Linking setup
	'GYM_LINKS_ACTIVATION' => 'Forum Linking',
	'GYM_LINKS_MAIN' => 'Main links',
	'GYM_LINKS_MAIN_EXPLAIN' => 'Display or not links to main GYM page in footer : SitemapIndex, main RSS feed and feed list page, main map and new page.',
	'GYM_LINKS_INDEX' => 'Links on index',
	'GYM_LINKS_INDEX_EXPLAIN' => 'Display or not links to the available GYM pages for each forum on the forum index. These links are added below the forum descritpions.',
	'GYM_LINKS_CAT' => 'Links on forum page',
	'GYM_LINKS_CAT_EXPLAIN' => 'Display or not links to the available GYM pages on a forum page. These links are added below the forum title.',
	// Google sitemaps
	'GOOGLE' => 'Google',
	// Reset settings
	'GOOGLE_MAIN_RESET' => 'Hlavní možnosti Google Sitemap',
	'GOOGLE_MAIN_RESET_EXPLAIN' => 'Obnovit všechna hlavní nastavení Google Sitemap na původní hodnoty.',
	// RSS feeds
	'RSS' => 'RSS',
	// Reset settings
	'RSS_MAIN_RESET' => 'Hlavní možnosti RSS',
	'RSS_MAIN_RESET_EXPLAIN' => 'Obnovit všechna hlavní nastavení RSS na původní hodnoty.',
	'YAHOO' => 'Yahoo',
	// HTML
	'HTML_MAIN_RESET' => 'Nastavení globálního HTML',
	'HTML_MAIN_RESET_EXPLAIN' => 'Obnovit všechan hlavní nastavení HTML map a novinek main na původní hodnoty.',
	'HTML' => 'Html',

	// GYM authorisation array
	'GYM_AUTH_ADMIN' => 'Administrátor',
	'GYM_AUTH_GLOBALMOD' => 'globální moderátoři',
	'GYM_AUTH_REG' => 'Přihlášení',
	'GYM_AUTH_GUEST' => 'Návštěvníci',
	'GYM_AUTH_ALL' => 'Všichni',
	'GYM_AUTH_NONE' => 'Nikdo',
	// XSLT
	'GYM_STYLE' => 'Vzhled',

	// Cache status
	'SEO_CACHE_FILE_TITLE' => 'Stav Cache',
	'SEO_CACHE_STATUS' => 'Adresář cache je nastaven v: <b>%s</b>',
	'SEO_CACHE_FOUND' => 'Adresář cache nalezen.',
	'SEO_CACHE_NOT_FOUND' => 'Adresář cache nenalezen.',
	'SEO_CACHE_WRITABLE' => 'Adresář cache je zapisovatelný.',
	'SEO_CACHE_UNWRITABLE' => 'Adresář cache <strong>není</strong> zapisovatelný. Prosím změnte práva na 0777.',
	
	// Mod Rewrite type
	'ACP_SEO_SIMPLE' => 'Základní',
	'ACP_SEO_MIXED' => 'Střední',
	'ACP_SEO_ADVANCED' => 'Rozšířený',
	'ACP_PHPBB_SEO_VERSION' => 'Verze',
	'ACP_SEO_SUPPORT_FORUM' => 'fórum podpory',
	'ACP_SEO_RELEASE_THREAD' => 'Aktuální verze',
	'ACP_SEO_REGISTER_TITLE' => 'Registrovat',
	'ACP_SEO_REGISTER_UPDATE' => 'informováni o aktualizacích',
	'ACP_SEO_REGISTER_MSG' => 'Možná se budete chtít %1$s abyste byli %2$s',

	// Maintenance
	'GYM_MAINTENANCE' => 'Údržba',
	'GYM_MODULE_MAINTENANCE' => '%1$s údržba',
	'GYM_MODULE_MAINTENANCE_EXPLAIN' => 'Zde můžete spravovat soubory cache pro %1$s modul.<br/>
	Možné dva typy: první slouží k ukládání výstupních dat pro veřejné stránky, druhý pro moduly v ACP.
	Můžete smazat cache modulu ACP pokud vyberete volbu smazání cache;
	základním nastavením je smazání cache vybraného modulu.',
	'GYM_CLEAR_CACHE' => 'Smazat %1$s cache',
	'GYM_CLEAR_CACHE_EXPLAIN' => 'Smazání cahce pro modul %1$s. Soubory cache obsahují výstupní data pro %1$s.<br/>Vhodné pro aktualizaci cache.',
	'GYM_CLEAR_ACP_CACHE' => 'Smazat %1$s ACP cache',
	'GYM_CLEAR_ACP_CACHE_EXPLAIN' => 'Vyčistit cache nastavení pro ACP modul %1$s. Soubory cache obsahují výstupní data pro %1$s.<br/>Vhodné použít po provedední změn v nastavení a v&nbsp;nastavení výstupním možností.',
	'GYM_CACHE_CLEARED' => 'Cache úspěšně smazána v: ',
	'GYM_CACHE_NOT_CLEARED' => 'Chyba při mazání cache, prosím zkontrolujte práva (CHMOD 0666 nebo 0777).<br/>The folder currently set up for caching is : ',
	'GYM_FILE_CLEARED' => 'Soubor(y) vymazány: ',
	'GYM_CACHE_ACCESSED' => 'Cache adresář je dostupný, ale žádný soubor nebyl smazán: ',
	'MODULE_CACHE_CLEARED' => 'Cache ACP modulu úspěšně smazána, pokud jste přidali modul, měl by se vám nyní zobrazit.',
	
	// set defaults
	'GYM_SETTINGS' => 'Nastavení',
	'GYM_RESET_ALL' => 'Obnovit vše',
	'GYM_RESET_ALL_EXPLAIN' => 'Vyberte pokud si přejete nastavit všechny původní hodnty.',
	'GYM_RESET' => 'Reset %1$s config',
	'GYM_RESET_EXPLAIN' => 'Below you can reset %1$s modules config, either a whole module at once or only a given set of module config.',

	'GYM_INSTALL' => 'Instalace',
	'GYM_MODULE_INSTALL' => 'Instalace module %1$s',
	'GYM_MODULE_INSTALL_EXPLAIN' => 'Below you can activate/deactivate the %1$s module.<br/>If you just uploaded a module, you need to activate it before you will be able to use it.<br/>If you cannot see new module, try clearing the ACP module’s cache in the maintenance page.',

	// Titles
	'GYM_MAIN' => 'Nastavení GYM Sitemap',
	'GYM_MAIN_EXPLAIN' => 'Zde jsou nastavení společná pro všechny výstupy a moduly.<br />Mohou být použity pro všechny typy výstupů (HTML, RSS, Google Sitemaps, Yahoo! Url list) a pro všechny moduly, v závislosti na vašem nastavení přepisu.',
	'MAIN_MAIN' => 'Přehled GYM Sitemap',
	'MAIN_MAIN_EXPLAIN' => 'GYM sitemap je velmi flexibilní a optimalizovatelný MOD phpBB pro vyhledávače. To vám umožní vybodovat Google Sitemap, RSS 2.0 kanály, URL Yahoo! seznamy a HTML sitemap pro vaše celé fórum nebo pouze pro jeho část.<br /><br />
	Pro každý typ výstupu (Google, RSS, html & Yahoo) můžete získat informace do seznamu z několika aplikací na fóru (fórum, album, atd.) za použití příslušného modulu.<br/>
	Můžete aktivovat/deaktivovat moduly použitím instalačního odkazu v ACP pro každý typ, každý modul má své vlastní nastavení.<br/><br/>
	Ujistěte se, že máte "%1$s", podpora je poskytována na %2$s.<br/>
	Obecná podpora a diskuse je poskytována na %3$s<br/>%4$s<br/>Užijte si ;-)',

	'GYM_GOOGLE' => 'Google Sitemaps',
	'GYM_GOOGLE_EXPLAIN' => 'Zde jsou nastavení společná pro všechny moduly Google sitemap (fórum, různé, atd.).<br />
	Mohou být použity na všechny Google Sitemap moduly v závislosti na vašem nastavení přepisu tohoto výstupu a všech modulů.',
	'GYM_RSS' => 'RSS feeds',
	'GYM_RSS_EXPLAIN' => 'Zde jsou nastavení společná pro všechny moduly RSS (fórum, různé, atd.).<br />
	Mohou být použity na všechny RSS moduly v závislosti na vašem nastavení přepisu tohoto výstupu a všech modulů.',
	'GYM_HTML' => 'HTML Stránky',
	'GYM_HTML_EXPLAIN' => 'Zde jsou nastavení společná pro všechny moduly HTML (fórum, různé, atd.).<br />
	Mohou být použity na všechny HTML moduly v závislosti na vašem nastavení přepisu tohoto výstupu a všech modulů.',
	'GYM_MODULES_INSTALLED' => 'Aktivní modul(y)',
	'GYM_MODULES_UNINSTALLED' => 'Neaktivní modul(y)',

	// Overrides
	'GYM_OVERRIDE_GLOBAL' => 'Globální',
	'GYM_OVERRIDE_OTYPE' => 'Typ výstupu',
	'GYM_OVERRIDE_MODULE' => 'Modul',
	
	// override messages
	'GYM_OVERRIDED_GLOBAL' => 'Volba je nahrazena nastavením na nejvyšší úrovni (hlavní nastavení)',
	'GYM_OVERRIDED_OTYPE' => 'Volba je nahrazena nastavením na úrovni typ výstupu',
	'GYM_OVERRIDED_MODULE' => 'Volba je nahrazena nastavením na úrovni modulu',
	'GYM_OVERRIDED_VALUE' => 'Volba je nastavena na: ',
	'GYM_OVERRIDED_VALUE_NOTHING' => 'nic',
	'GYM_COULD_OVERRIDE' => 'Volba může být nahrazena, ale není.',

	// Overridable / common options
	'GYM_CACHE' => 'Cache',
	'GYM_CACHE_EXPLAIN' => 'Zde můžete nastavit různé možnosti pro cache. Nezapomeňte, tyto nastavení mohou být nahrazeny v závislosti na nastavení nahrazení.',
	'GYM_MOD_SINCE' => 'Activate Modified Since',
	'GYM_MOD_SINCE_EXPLAIN' => 'Modul se zeptá zda li je stránka v cache aktuální, před odesláním obsahu.<br /><u>Poznámka:</u> Tato volba se bude týkat všech typů výstupu.',
	'GYM_CACHE_ON' => 'Aktivovat cache',
	'GYM_CACHE_ON_EXPLAIN' => 'Můžete aktivovat/deaktivovat cache pro tento modul.',
	'GYM_CACHE_FORCE_GZIP' => 'Vynutit compresi cache',
	'GYM_CACHE_FORCE_GZIP_EXPLAIN' => 'Povolit vynucení gunzip komprese pro soubory v cache přestože gunzip není povolen. Ušetří vám to trochu místa na disku, ale více zatíží server a zpomalí načítání.',
	'GYM_CACHE_MAX_AGE' => 'Platnost cache',
	'GYM_CACHE_MAX_AGE_EXPLAIN' => 'Maximální doba platnosti cache. Soubory cache budou aktualizovány jednotlivě podle své vlastní platnosti, pokud je zapnuta re. Pokud se tak nestane, cache může být aktualizována ručně v ACP.',
	'GYM_CACHE_AUTO_REGEN' => 'Automatická aktualizace cache',
	'GYM_CACHE_AUTO_REGEN_EXPLAIN' => 'Pokud je aktivní, výstupy budou aktualizovány automaticky po skončení platnosti cache souboru. Pokud není aktivní, bude muset cache soubory aktualizovat ručně, aby seznamy byly aktuální.',
	'GYM_SHOWSTATS' => 'Statistika cache',
	'GYM_SHOWSTATS_EXPLAIN' => 'Vygeneruje statistiku do zdrojového kódu.<br /><u>Poznámka:</u> Zabere trochu času pro vytvoření výstupu. Krok není opakován, pokud je uloženo v cache.',
	'GYM_CRITP_CACHE' => 'Kódovat cache soubory',
	'GYM_CRITP_CACHE_EXPLAIN' => 'Nastavte pokud si přejete kódovat jména souborů cache. Je bezpečnější mít jména souborů zakódovány, pro ladění je lepší tuto možnost vypnout.<br /><u>Poznámka:</u> Tato možnost se vztahuje na všechy soubory uložené v cache.',

	'GYM_MODREWRITE' => 'Nahrazení URL',
	'GYM_MODREWRITE_EXPLAIN' => 'Zde je možné nastavit různé možnosti pro přepis URL. Nezapomeňte, že tato nastavení mohou být nahrazena v závislosti na hlavním nastavení.',
	'GYM_MODREWRITE_ON' => 'Povolit nahrazení URL',
	'GYM_MODREWRITE_ON_EXPLAIN' => 'Aktivace nahrazení URL v odkazech.<br /><u>Poznámka:</u> MUSÍTE použít Apache server s mod_rewrite modulem nebo IIS server běžící s isapi_rewrite modulem A správně nastavit pravidla pro přepis URL v .htaccess (nebo httpd.ini v IIS ).',
	'GYM_ZERO_DUPE_ON' => 'Nulová duplicita',
	'GYM_ZERO_DUPE_ON_EXPLAIN' => 'Nebude existovat duplicita odkazů v modulech.<br /><u>Poznámka:</u> Přesměrování nastane pouze tehdy, když je cache vytvářena v této verzi.',
	'GYM_MODRTYPE' => 'Typ nahrazení URL',
	'GYM_MODRTYPE_EXPLAIN' => 'Toto nastavení bude ignorováno v případě, že používáte phpBB SEO MOD (autodetekce).<br/>
	Čtyři úrovně nahrazení URL: Žádné, Základní, Střední a Rozšířené :<br/><ul><li style="font-size:0.99em;"><b>Žádné:</b> nenahrazuje URL<br></li><li style="font-size:0.99em;"><b>Základní:</b>Statické nahrazení URL pro všechny odkazy, nevkládá titulky<br></li><li style="font-size:0.99em;"><b>Střední:</b> Názvy fór a kategorií jsou vloženy do odkazů, ale názvy témat nikoli, pouze staticky<br></li><li style="font-size:0.99em;"><b>Rozšířené:</b> Všechny titulky jsou vloženy do odkazů</li></ul>',

	'GYM_GZIP' => 'GUNZIP',
	'GYM_GZIP_EXPLAIN' => 'Zde je možné nastavit různé možnosti gunzip.  Nezapomeňte, tyto nastavení mohou být nahrazeny v závislosti na nastavení nahrazení.%1$s',
	'GYM_GZIP_FORCED' => '<br/><b style="color:red;">Poznámka:</b> Gun-zip komprese je aktivována v konfiguraci phpBB. Nastavení je modulo vnuceno.',
	'GYM_GZIP_CONFIGURABLE' => '<br/><b style="color:red;">Poznámka:</b> Gun-zip komprese není aktivována v konfiguraci phpBB. Můžete nastavit gunzip dle vašich požadavků.',
	'GYM_GZIP_ON' => 'Povolit gunzip',
	'GYM_GZIP_ON_EXPLAIN' => 'Povolí gunzip kompresi pro výstup. To sníží množství přenesených dat do prohlížeče, navýší však dobu načítání.',
	'GYM_GZIP_EXT' => 'Gunzip přípona',
	'GYM_GZIP_EXT_EXPLAIN' => 'Vyberte pokud chcete použít příponu souboru .gz. Bude aplikováno použe pokud je gunzip a nahrazení URL aktivní.',
	'GYM_GZIP_LEVEL' => 'Úroveň Gunzip komprese',
	'GYM_GZIP_LEVEL_EXPLAIN' => 'Číslo od 1 do 9, 9 značí nejvyšší kompresi. Není nutné mít vyšší nastavení než 6.<br /><u>Poznámka:</u> Tato volba se bude týkat všech typů výstupu.',

	'GYM_LIMIT' => 'Limity',
	'GYM_LIMIT_EXPLAIN' => 'Zde můžete nastavit limity, které budou použity ve výstupech: počet odkazů, cyklů SQL (počet položek v jednom dotaze) a stáří vypisovaných položek.<br/>Tato nastavení mohou být nahrazeny v závislosti na vašem nastavení nahrazení.',
	'GYM_URL_LIMIT' => 'Počet položek',
	'GYM_URL_LIMIT_EXPLAIN' => 'Maximální počet položek ve výstupu.',
	'GYM_SQL_LIMIT' => 'Cykly SQL',
	'GYM_SQL_LIMIT_EXPLAIN' => 'Pro všechny typy výstupu, kromě html, SQL dotaz je rozdělen na několik SQL dotazů aby nebyl spouštěn jeden velký a rozsáhlý SQL dotaz.<br/>Zde definujete počet položek na jeden dotaz. Počet dotazů bude vypočítán v závislosti na počtu položek výstupu a počtu položek v jednom dotazu.',
	'GYM_TIME_LIMIT' => 'Časový limit',
	'GYM_TIME_LIMIT_EXPLAIN' => 'Omezení na dny. Maximální stáří položek ve výstupu, starší nebudou zobrazeny. Může být užitečné pro snížení zatížení serveru u velkých databází. 0 znamená bez omezení.',

	'GYM_SORT' => 'Řazení',
	'GYM_SORT_EXPLAIN' => 'Zde můžete nastavit způsob řazení.<br/>Tato nastavení mohou být nahrazeny v závislosti na vašem nastavení nahrazení.',
	'GYM_SORT_TYPE' => 'Základní řazení',
	'GYM_SORT_TYPE_EXPLAIN' => 'Všechny odkazy jsou defaultně řazeny podle poslední aktivity (sestupně).<br />
	Pokud chcete aby vyhledávače nalezly jako první váš starší odkaz, nastavte řazení na vzestupné.<br/>
	Tato nastavení mohou být nahrazeny v závislosti na vašem nastavení nahrazení.',

	'GYM_PAGINATION' => 'Stránkování',
	'GYM_PAGINATION_EXPLAIN' => 'Zde můžete nastavit různé možnosti stránkování. Tato nastavení mohou být nahrazeny v závislosti na vašem nastavení nahrazení.',
	'GYM_PAGINATION_ON' => 'Povolit stránkování',
	'GYM_PAGINATION_ON_EXPLAIN' => 'Můžete se rozhodnout zda li budou vygenerovány, v případě potřeby, odkazy na stránkování. For example, the module can additionally output links of the forum’s topic pages.',
	'GYM_LIMITDOWN' => 'Stránkování: dolní mez',
	'GYM_LIMITDOWN_EXPLAIN' => 'Enter here how many paginated pages, starting from the first page, to output.',
	'GYM_LIMITUP' => 'Stránkování: horní mez',
	'GYM_LIMITUP_EXPLAIN' => 'Enter here how many paginated pages, starting from the last one, to output.',

	'GYM_OVERRIDE' => 'Vynucené',
	'GYM_OVERRIDE_EXPLAIN' => 'GYM sitemap je plně modulární. Každý typ výstupu (Google, RSS ...) používá vlastní výstup pro seznam položek.
	Příklad, prvním modulem pro všechny typy výstupu je modul fóra, zobrazující položky z fóra.<br/>
	Mnoho vlastností, například nahrazení URL, ukládání cache, gunzip, komprese, atd., jsou k dispozici skoro pro každý modul.
	To vám umožní různá nastavení pro stejné vlastnosti různých modulů, v závislosti na typu výstupu.
	Může nastat situace, pro příklad, že budete chtít aktivovat nahrazení URL pro všechny moduly najednou (všechny moduly i všechny typy výstupu).<br/>
	To je to, co vám umožní vynucené nastavení pro mnoho různých vlastností.<br/>
	Proces dedění je nastaven od nejvyššího (Hlavní nastavení) přes typ výstupu (Google, RSS, ...) k výstupním modulům (fórum, album, ...).<br/>
	Vynucené nastavení může nabývat těchto hodnot:<br/><ul><li><b>Globální:</b> použití hlavního nastavení<br></li><li><b>Typ výstupu:</b> použítí nastavení výstupních modulů<br></li><li><b>Modul:</b> použití nastavení modulu.</li></ul>',
	'GYM_OVERRIDE_ON' => 'Aktivovat',
	'GYM_OVERRIDE_ON_EXPLAIN' => 'Povolit hlavní vynucení nastavení. Deaktivace povolí různá nastavení a vynucení na nižších úrovních.',
	'GYM_OVERRIDE_MAIN' => 'Základní vynucení',
	'GYM_OVERRIDE_MAIN_EXPLAIN' => 'Potlačí nastavení na nižších úrovních pro ostatní typy.',
	'GYM_OVERRIDE_CACHE' => 'Vynucená cache',
	'GYM_OVERRIDE_CACHE_EXPLAIN' => 'Jaký typ vynucení má být použit pro cache.',
	'GYM_OVERRIDE_GZIP' => 'Gunzip',
	'GYM_OVERRIDE_GZIP_EXPLAIN' => 'Jaký typ vynucení má být použit pro gunzip.',
	'GYM_OVERRIDE_MODREWRITE' => 'Náhrada URL',
	'GYM_OVERRIDE_MODREWRITE_EXPLAIN' => 'Jaký typ vynucení má být použit pro náhradu URL.',
	'GYM_OVERRIDE_LIMIT' => 'Limity',
	'GYM_OVERRIDE_LIMIT_EXPLAIN' => 'Jaký typ vynucení má být použit pro limity.',
	'GYM_OVERRIDE_PAGINATION' => 'Stránkování',
	'GYM_OVERRIDE_PAGINATION_EXPLAIN' => 'Jaký typ vynucení má být použit pro stránkování.',
	'GYM_OVERRIDE_SORT' => 'Řazení',
	'GYM_OVERRIDE_SORT_EXPLAIN' => 'Jaký typ vynucení má být použit pro řazení.',

	// Mod rewrite
	'GYM_MODREWRITE_ADVANCED' => 'Rozšířené',
	'GYM_MODREWRITE_MIXED' => 'Střední',
	'GYM_MODREWRITE_SIMPLE' => 'Základní',
	'GYM_MODREWRITE_NONE' => 'Žádné',

	// Sorting
	'GYM_ASC' => 'Vzestupně',
	'GYM_DESC' => 'Sestupně',

	// Other
	// robots.txt
	'GYM_CHECK_ROBOTS' => 'Kontrola robots.txt',
	'GYM_CHECK_ROBOTS_EXPLAIN' => 'Zkontrolujte seznamy odkazů s robots.txt, pokud existuje, pro vyloučení příslušných URL. Tento MOD bere v úvahu automaticky seznam z robots.txt.<br />Tato bolba je užitečná pro import TXT a XML, i když už není jisté, že seznam URL neobsahuje zakázané URL.<br/><br /><u>Poznámka:</u><br />Tato volba vyžeduje práci se zdrojovými soubory a je vhodné ji použí pokud máte zapnutou cache.',
	// summarize method
	'GYM_METHOD_CHARS' => 'podle znaků',
	'GYM_METHOD_WORDS' => 'podle slov',
	'GYM_METHOD_LINES' => 'podle řádek',
));
?>