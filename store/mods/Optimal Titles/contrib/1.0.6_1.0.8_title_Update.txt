##############################################################
## MOD Title:		Update phpBB SEO Optimal titles 1.0.6 => 1.0.8
## 
## MOD Author:		dcz <n/a> http://www.phpbb-seo.com/
##
## MOD Description:	This are the update steps for the phpBB SEO Optimal titles 1.0.6 => 1.0.8
##			Check http://www.phpbb-seo.com/en/phpbb-seo-toolkit/optimal-titles-t1289.html
## 			for the latest version or to get help with this MOD
##
## MOD Version: 	1.0
##
## Installation Level: 	Easy
## Installation Time: 	2 Minutes
## Files To Edit:	2
##       		viewforum.php
##			viewtopic.php
##
## Included Files: 	n/a
##
## License: 		http://opensource.org/licenses/gpl-license.php GNU General Public License v2
##############################################################
## Author Notes:
## _____________
##
## This are the update steps for the phpBB SEO Optimal titles 1.0.6 => 1.0.8 update.
## 
##############################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD
##############################################################

#
#-----[ OPEN ]------------------------------------------
#

viewforum.php

#
#-----[ FIND ]------------------------------------------
#

page_header($forum_data['forum_name'] . $extra_title);

#
#-----[ REPLACE WITH ]------------------------------------------
#

page_header($forum_data['forum_name'] . $extra_title, true, $forum_id);

#
#-----[ OPEN ]------------------------------------------
#

viewtopic.php

#
#-----[ FIND ]------------------------------------------
#

page_header($topic_data['topic_title'] . ' : ' .  $topic_data['forum_name'] . $extra_title);

#
#-----[ REPLACE WITH ]------------------------------------------
#

page_header($topic_data['topic_title'] . ' : ' .  $topic_data['forum_name'] . $extra_title, true, $forum_id);

#
#---[ SAVE/CLOSE ALL FILES ]-----------------------
#
# EoM
