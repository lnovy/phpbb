##############################################################
## MOD Title:		Update phpBB3 SEO Optimal titles 1.0.2 => 1.0.4
## 
## MOD Author:		dcz <n/a> http://www.phpbb-seo.com/
##
## MOD Description:	This are the update steps for the phpBB3 SEO Optimal titles 1.0.2 => 1.0.4
##			Check http://www.phpbb-seo.com/boards/phpbb3-seo-toolkit/seo-dynamic-meta-tags-vt1308.html
## 			for the latest version or to get help with this MOD
##
## MOD Version: 	1.0
##
## Installation Level: 	Easy
## Installation Time: 	3 Minutes
## Files To Edit:	2
##       	styles/prosilver/template/overall_header.html,
##			styles/subsilver2/template/overall_header.html.
##
## Included Files: 	n/a
##
## License: 		http://opensource.org/licenses/gpl-license.php GNU General Public License v2
##############################################################
## Author Notes:
## _____________
##
## This are the update steps for the phpBB3 SEO Optimal titles 1.0.2 => 1.0.4 update.
## 
##############################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD
##############################################################
#
#-----[ DIY INSTRUCTIONS ]------------------------------------------
#

subsilver2/ code changes are only required if you never edited it, otherwise the same code change
as per prosilver is the one to to on subsilver2/ as well.

#
#-----[ OPEN ]------------------------------------------
#

styles/prosilver/template/overall_header.html

#
#-----[ FIND ]------------------------------------------
#

<title>{PAGE_TITLE}<!-- IF S_IN_MCP -->{L_MCP} &bull; <!-- ELSEIF S_IN_UCP -->{L_UCP} &bull; <!-- ENDIF --></title>

#
#-----[ REPLACE, WITH ]------------------------------------------
#

<title>{PAGE_TITLE}<!-- IF S_IN_MCP --> &bull; {L_MCP}<!-- ELSEIF S_IN_UCP --> &bull; {L_UCP}<!-- ENDIF --></title>

#
#-----[ OPEN ]------------------------------------------
#

styles/subsilver2/template/overall_header.html

#
#-----[ FIND ]------------------------------------------
#

<meta http-equiv="content-type" content="text/html; charset={S_CONTENT_ENCODING}" />

#
#-----[ AFTER, ADD]------------------------------------------
#

<title>{PAGE_TITLE}<!-- IF S_IN_MCP --> &bull; {L_MCP}<!-- ELSEIF S_IN_UCP --> &bull; {L_UCP}<!-- ENDIF --></title>

#
#-----[ FIND ]------------------------------------------
#

<title>{SITENAME} &bull; <!-- IF S_IN_MCP -->{L_MCP} &bull; <!-- ELSEIF S_IN_UCP -->{L_UCP} &bull; <!-- ENDIF -->{PAGE_TITLE}</title>

#
#-----[ REPLACE WITH ]------------------------------------------
# EG Delete

#
#---[ SAVE/CLOSE ALL FILES ]-----------------------
#
# EoM
