##############################################################
## MOD Title:		Update Optimisation des titres de phpBB 1.0.2 => 1.0.4
## 
## MOD Author: dcz <n/a> http://www.phpbb-seo.com/
##
## MOD Description: 	Ceci est la proc�dure de mise � jour du MOD Optimisation des titres de phpBB pour la transition version 1.0.2 => 1.0.4.
## 			Rendez-vous sur http://www.phpbb-seo.com/forums/toolkit-phpbb3-seo/meta-tags-dynamiques-seo-vt1678.html
## 			pour toujours obtenir la derni�re version ou de l'aide pour ce MOD.
##
## MOD Version: 	1.0
##
## Installation Level: 	Easy
## Installation Time: 	3 Minutes
## Files To Edit:	2
##       	styles/prosilver/template/overall_header.html,
##			styles/subsilver2/template/overall_header.html.
##
## Included Files: 	n/a
##
## License: 		http://opensource.org/licenses/gpl-license.php GNU General Public License v2
##############################################################
## Author Notes:
## _____________
##
## Voici les instructions de mise � jour : Optimisation des titres de phpBB 1.0.2 => 1.0.4
##
##############################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD
##############################################################
#
#-----[ DIY INSTRUCTIONS ]------------------------------------------
#

Les changement pour la mise � jour de subsilver2/ sont seulement n�cessaire si vous ne les avez jamais effectu�s,
vous devrez autrement utiliser le changement de prosilver pour la mise � jour.

#
#-----[ OPEN ]------------------------------------------
#

styles/prosilver/template/overall_header.html

#
#-----[ FIND ]------------------------------------------
#

<title>{PAGE_TITLE}<!-- IF S_IN_MCP -->{L_MCP} &bull; <!-- ELSEIF S_IN_UCP -->{L_UCP} &bull; <!-- ENDIF --></title>

#
#-----[ REPLACE, WITH ]------------------------------------------
#

<title>{PAGE_TITLE}<!-- IF S_IN_MCP --> &bull; {L_MCP}<!-- ELSEIF S_IN_UCP --> &bull; {L_UCP}<!-- ENDIF --></title>

#
#-----[ OPEN ]------------------------------------------
#

styles/subsilver2/template/overall_header.html

#
#-----[ FIND ]------------------------------------------
#

<meta http-equiv="content-type" content="text/html; charset={S_CONTENT_ENCODING}" />

#
#-----[ AFTER, ADD]------------------------------------------
#

<title>{PAGE_TITLE}<!-- IF S_IN_MCP --> &bull; {L_MCP}<!-- ELSEIF S_IN_UCP --> &bull; {L_UCP}<!-- ENDIF --></title>

#
#-----[ FIND ]------------------------------------------
#

<title>{SITENAME} &bull; <!-- IF S_IN_MCP -->{L_MCP} &bull; <!-- ELSEIF S_IN_UCP -->{L_UCP} &bull; <!-- ENDIF -->{PAGE_TITLE}</title>

#
#-----[ REPLACE WITH ]------------------------------------------
# EG Delete

#
#---[ SAVE/CLOSE ALL FILES ]-----------------------
#
# EoM
