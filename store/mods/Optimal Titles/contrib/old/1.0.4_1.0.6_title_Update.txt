##############################################################
## MOD Title:		Update phpBB SEO Optimal titles 1.0.4 => 1.0.6
## 
## MOD Author:		dcz <n/a> http://www.phpbb-seo.com/
##
## MOD Description:	This are the update steps for the phpBB SEO Optimal titles 1.0.4 => 1.0.6
##			Check http://www.phpbb-seo.com/en/phpbb-seo-toolkit/optimal-titles-t1289.html
## 			for the latest version or to get help with this MOD
##
## MOD Version: 	1.0
##
## Installation Level: 	Easy
## Installation Time: 	1 Minutes
## Files To Edit:	1
##       		index.php
##
## Included Files: 	n/a
##
## License: 		http://opensource.org/licenses/gpl-license.php GNU General Public License v2
##############################################################
## Author Notes:
## _____________
##
## This are the update steps for the phpBB SEO Optimal titles 1.0.4 => 1.0.6 update.
## 
##############################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD
##############################################################

#
#-----[ OPEN ]----------------------------------------------
#

index.php

#
#-----[ FIND ]----------------------------------------------
#

page_header($user->lang['INDEX']);

#
#-----[ REPLACE WITH ]------------------------------------------
#

// www.phpBB-SEO.com SEO TOOLKIT BEGIN - TITLE
page_header($config['sitename']);
// www.phpBB-SEO.com SEO TOOLKIT END - TITLE

#
#---[ SAVE/CLOSE ALL FILES ]-----------------------
#
# EoM
