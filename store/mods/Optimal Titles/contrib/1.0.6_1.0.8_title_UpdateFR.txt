##############################################################
## MOD Title:		Update Optimisation des titres de phpBB 1.0.6 => 1.0.8
## 
## MOD Author: dcz <n/a> http://www.phpbb-seo.com/
##
## MOD Description: 	Ceci est la procédure de mise à jour du MOD Optimisation des titres de phpBB pour la transition version 1.0.6 => 1.0.8.
## 			Rendez-vous sur http://www.phpbb-seo.com/fr/toolkit-phpbb-seo/optimisation-titres-t1653.html
## 			pour toujours obtenir la dernière version ou de l'aide pour ce MOD.
##
## MOD Version: 	1.0
##
## Installation Level: 	Easy
## Installation Time: 	2 Minutes
## Files To Edit:	2
##       		viewforum.php
##			viewtopic.php
##
## Included Files: 	n/a
##
## License: 		http://opensource.org/licenses/gpl-license.php GNU General Public License v2
##############################################################
## Author Notes:
## _____________
##
## Voici les instructions de mise à jour : Optimisation des titres de phpBB 1.0.6 => 1.0.8
##
##############################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD
##############################################################

#
#-----[ OPEN ]------------------------------------------
#

viewforum.php

#
#-----[ FIND ]------------------------------------------
#

page_header($forum_data['forum_name'] . $extra_title);

#
#-----[ REPLACE WITH ]------------------------------------------
#

page_header($forum_data['forum_name'] . $extra_title, true, $forum_id);

#
#-----[ OPEN ]------------------------------------------
#

viewtopic.php

#
#-----[ FIND ]------------------------------------------
#

page_header($topic_data['topic_title'] . ' : ' .  $topic_data['forum_name'] . $extra_title);

#
#-----[ REPLACE WITH ]------------------------------------------
#

page_header($topic_data['topic_title'] . ' : ' .  $topic_data['forum_name'] . $extra_title, true, $forum_id);

#
#---[ SAVE/CLOSE ALL FILES ]-----------------------
#
# EoM
