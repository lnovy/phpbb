##############################################################
## MOD Title: 		phpBB SEO Rewrite Avancé MAJ 0.6.6 => Ultimate SEO URL 0.6.8
## MOD Author: 		dcz <n/a> http://www.phpbb-seo.com/
## MOD Description: 	Ceci est la procédure de mise à jour du MOD phpBB SEO Rewrite Avancé pour la transition version 0.6.6 => Ultimate SEO URL 0.6.8
## 			Rendez-vous sur http://www.phpbb-seo.com/fr/mod-rewrite-phpbb/ultimate-seo-url-t4489.html
## 			pour toujours obtenir la dernière version ou de l'aide pour ce MOD.
##
## MOD Version: 	1.0
##
## Installation Level: 	(Intermediate)
## Installation Time: 	15 Minutes
## Files To Edit: 	(11)
##			viewforum.php,
##			viewonline.php,
##			viewtopic.php,
##			includes/functions.php,
##			includes/functions_display.php,
##			includes/session.php,
##			styles/prosilver/template/forum_fn.js,
##			styles/subsilver2/template/overall_header.html,
##			styles/subsilver2/template/viewtopic_body.html,
##			phpbb_seo/includes/setup_phpbb_seo.php (see contrib/setup_phpbb_seo_class_0.6.6_to_0.6.8.diff)
##			phpbb_seo/phpbb_seo_class.php (see contrib/phpbb_seo_class_0.6.6_to_0.6.8.diff)
##
## Included Files: n/a
##############################################################
## Author Notes:
## _____________
##
## Voici les instructions de mise à jour : phpBB3 SEO Mod Rewrite Avancé 0.6.6 => Ultimate SEO URL 0.6.8
##
##############################################################
## MOD History:
##
##	2011-07-18 - 1.0
##		- First version released
##
##############################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD
##############################################################


#
#-----[ DIY INSTRUCTIONS ]--------------------------------------------------
#

_____________________________________________
>>>> INSTALLATION POUR VOTRE FORUM <<<<<
_____________________________________________

Vos réglages d'urls de la classe phpbb_seo ne seront pas altérés par la MAJ, mais de nouvelle options sont ajoutés.
Vous DEVREZ mettre à jour votre .htaccess un fois cette mise à jour effectuée.
Le fichier phpbb_seo_class.php ne contient plus les personnalisation des parties statiques et des délimiteurs de vos URLs, ces réglages se font désormais dans phpbb_seo/includes/setup_phpbb_seo.php. Si vous aviez personnalisé les réglages par défaut pour les parties statiques et les délimiteurs, vous DEVREZ reporter ces modification dans ce fichier. Veuillez vous reporter au fichier d'installation pour plus de détails.

Une fois les nouveaux fichiers en place, il vous faudra vider le cache de phpBB et rafraichir vos thèmes (ACP => STYLE => Thèmes => Rafraichir).

Vous devrez uploader / écraser :
	includes/acp/acp_phpbb_seo.php
	includes/acp/info/acp_phpbb_seo.php
	phpbb_seo/includes/setup_phpbb_seo.php (see contrib/setup_phpbb_seo_class_0.6.6_to_0.6.8.diff)
	phpbb_seo/phpbb_seo_class.php (see contrib/phpbb_seo_class_0.6.6_to_0.6.8.diff)
	phpbb_seo/phpbb_seo_install.php
	phpbb_seo/sync_url.php

#
#-----[ COPY ]------------------------------------------
#

root/includes/acp/acp_phpbb_seo.php		to 	phpBB/includes/acp/acp_phpbb_seo.php
root/includes/acp/info/acp_phpbb_seo.php	to 	phpBB/includes/acp/info/acp_phpbb_seo.php
root/language/en/mods/acp_phpbb_seo.php		to 	phpBB/language/en/mods/acp_phpbb_seo.php
root/language/fr/mods/acp_phpbb_seo.php		to 	phpBB/language/fr/mods/acp_phpbb_seo.php
root/phpbb_seo/includes/setup_phpbb_seo.php	to 	phpBB/phpbb_seo/includes/setup_phpbb_seo.php
root/phpbb_seo/phpbb_seo_install.php		to 	phpBB/phpbb_seo/phpbb_seo_install.php
root/phpbb_seo/sync_url.php			to 	phpBB/phpbb_seo/sync_url.php

#
#-----[ OPEN ]------------------------------------------
#

viewforum.php

#
#-----[ FIND ]------------------------------------------
#

		header('HTTP/1.1 404 Not Found');

#
#-----[ REPLACE WITH ]------------------------------------------
#

		send_status_line(404, 'Not Found');

#
#-----[ OPEN ]------------------------------------------
#

viewonline.php

#
#-----[ FIND ]------------------------------------------
#

			if ($forum_id && $auth->acl_get('f_list', $forum_id))
			{

#
#-----[ AFTER, ADD ]------------------------------------------
#

				// www.phpBB-SEO.com SEO TOOLKIT BEGIN
				$phpbb_seo->set_url($forum_data[$forum_id]['forum_name'], $forum_id, 'forum');
				// www.phpBB-SEO.com SEO TOOLKIT END

#
#-----[ OPEN ]------------------------------------------
#

viewtopic.php

#
#-----[ FIND ]------------------------------------------
#

if (!empty($phpbb_seo->seo_opt['sql_rewrite'])) {

#
#-----[ REPLACE WITH ]------------------------------------------
#

if (!empty($phpbb_seo->seo_opt['sql_rewrite']) || !empty($topic_data['topic_url'])) {

#
#-----[ FIND ]------------------------------------------
#

	'S_MOD_ACTION' 			=> append_sid("{$phpbb_root_path}mcp.$phpEx", "f=$forum_id&amp;t=$topic_id" . (($start == 0) ? '' : "&amp;start=$start") . "&amp;quickmod=1&amp;redirect=" . urlencode(str_replace('&amp;', '&', $viewtopic_url)), true, $user->session_id),

#
#-----[ REPLACE WITH ]------------------------------------------
#

	// www.phpBB-SEO.com SEO TOOLKIT BEGIN
	'S_MOD_ACTION' 			=> append_sid("{$phpbb_root_path}mcp.$phpEx", "f=$forum_id&amp;t=$topic_id" . (($start == 0) ? '' : "&amp;start=$start") . "&amp;quickmod=1&amp;redirect=" . urlencode(str_replace('&amp;', '&', "{$phpbb_root_path}viewtopic.$phpEx?f=$forum_id&amp;t=$topic_id") . (($start == 0) ? '' : "&amp;start=$start") . ((strlen($u_sort_param)) ? "&amp;$u_sort_param" : '') . (($highlight_match) ? "&amp;hilit=$highlight" : '')), true, $user->session_id),
	// www.phpBB-SEO.com SEO TOOLKIT END

#
#-----[ OPEN ]------------------------------------------
#

includes/functions.php

#
#-----[ FIND ]------------------------------------------
#

			$pagin_find = array('`(https?\://[a-z0-9_/\.-]+/[a-z0-9_\.-]+)(\.[a-z0-9]+)(\?[\w$%&~\-;:=,@+\.]+)?(#[a-z0-9_\.-]+)?(&amp;|\?)start=([0-9]+)`i', '`(https?\://[a-z0-9_/\.-]+/[a-z0-9_\-]+)/(\?[\w$%&~\-;:=,@+\.]+)?(#[a-z0-9_\.-]+)?(&amp;|\?)start=([0-9]+)`i');
			$pagin_replace = array( '\1' . $phpbb_seo->seo_delim['start'] . '\6\2\3\4', '\1/' . $phpbb_seo->seo_static['pagination'] . '\5' . $phpbb_seo->seo_ext['pagination'] . '\2\3');

#
#-----[ REPLACE WITH ]------------------------------------------
#

			$pagin_find = array(
				// http://example.com/a_n-y/d.i.r/with.ext
				'`(https?\://[a-z0-9_/\.-]+/[a-z0-9_\.-]+)(\.[a-z0-9]+)(\?[\w$%&~\-;:=,@+\. ]+)?(#[a-z0-9_\.-]+)?(&amp;|\?)start=([0-9]+)`i',
				// http://example.com/a_n-y/d.i.r/withoutext
				'`(https?\://[a-z0-9_/\.-]+/[a-z0-9_-]+)/?(\?[\w$%&~\-;:=,@+\. ]+)?(#[a-z0-9_\.-]+)?(&amp;|\?)start=([0-9]+)`i'
			);
			$pagin_replace = array(
				// http://example.com/a_n-y/d.i.r/with-xx.ext
				'\1' . $phpbb_seo->seo_delim['start'] . '\6\2\3\4',
				// http://example.com/a_n-y/d.i.r/withoutext/pagexx.html
				'\1/' . $phpbb_seo->seo_static['pagination'] . '\5' . $phpbb_seo->seo_ext['pagination'] . '\2\3'
			);

#
#-----[ FIND ]------------------------------------------
#

			if (strpos($base_url, '#') !== false) {

#
#-----[ AFTER, ADD ]------------------------------------------
#

				// since hashes are not originally handled,
				// we need to make sure we put it at the end of the url

#
#-----[ FIND ]------------------------------------------
#

	$template->assign_vars( array( 'PHPBB_FULL_URL' => $phpbb_seo->seo_path['phpbb_url'],
		'SEO_BASE_HREF' => $phpbb_seo->seo_opt['seo_base_href'],
		'SEO_START_DELIM' => $phpbb_seo->seo_delim['start'],
		'SEO_SATIC_PAGE' => $phpbb_seo->seo_static['pagination'],
		'SEO_EXT_PAGE' => $phpbb_seo->seo_ext['pagination'],
		'SEO_CANONICAL_URL' => !empty($phpbb_seo->seo_opt['no_dupe']['on']) ? $phpbb_seo->get_canonical() : '',
		'SEO_EXTERNAL' => !empty($config['seo_ext_links']) ? 'true' : 'false',
		'SEO_EXTERNAL_SUB' => !empty($config['seo_ext_subdomain']) ? 'true' : 'false',
		'SEO_EXT_CLASSES' => !empty($config['seo_ext_classes']) ? "'" . preg_replace('`[^a-z0-9_|-]+`', '', str_replace(',', '|', trim($config['seo_ext_classes'], ', '))) . "'" : 'false',
		'SEO_HASHFIX' => $phpbb_seo->seo_opt['url_rewrite'] && $phpbb_seo->seo_opt['virtual_folder'] ? 'true' : 'false',
	));

#
#-----[ REPLACE WITH ]------------------------------------------
#

	if (!empty($phpbb_seo)) {
		$template->assign_vars( array(
			'PHPBB_FULL_URL' => $phpbb_seo->seo_path['phpbb_url'],
			'SEO_BASE_HREF' => $phpbb_seo->seo_opt['seo_base_href'],
			'SEO_START_DELIM' => $phpbb_seo->seo_delim['start'],
			'SEO_SATIC_PAGE' => $phpbb_seo->seo_static['pagination'],
			'SEO_EXT_PAGE' => $phpbb_seo->seo_ext['pagination'],
			'SEO_CANONICAL_URL' => !empty($phpbb_seo->seo_opt['zero_dupe']['on']) ? $phpbb_seo->get_canonical() : '',
			'SEO_EXTERNAL' => !empty($config['seo_ext_links']) ? 'true' : 'false',
			'SEO_EXTERNAL_SUB' => !empty($config['seo_ext_subdomain']) ? 'true' : 'false',
			'SEO_EXT_CLASSES' => !empty($config['seo_ext_classes']) ? "'" . preg_replace('`[^a-z0-9_|-]+`', '', str_replace(',', '|', trim($config['seo_ext_classes'], ', '))) . "'" : 'false',
			'SEO_HASHFIX' => $phpbb_seo->seo_opt['url_rewrite'] && $phpbb_seo->seo_opt['virtual_folder'] ? 'true' : 'false',
		));
	}

#
#-----[ FIND ]------------------------------------------
#

		'UA_POPUP_PM'			=> addslashes(append_sid($phpbb_seo->seo_path['phpbb_url'] . "ucp.$phpEx", 'i=pm&amp;mode=popup')),

#
#-----[ REPLACE WITH ]------------------------------------------
#

		'UA_POPUP_PM'			=> addslashes(append_sid((!empty($phpbb_seo) ? $phpbb_seo->seo_path['phpbb_url'] : $phpbb_root_path) . "ucp.$phpEx", 'i=pm&amp;mode=popup')),

#
#-----[ OPEN ]------------------------------------------
#

includes/functions_display.php

#
#-----[ FIND ]------------------------------------------
#

				$pagin_find = array('`(https?\://[a-z0-9_/\.-]+/[a-z0-9_\.-]+)(\.(?!' . $phpEx . ')[a-z0-9]+)(\?[\w\#$%&~\-;:=,@+\.]+)?(&amp;|\?)start=([0-9]+)`i', '`(https?\://[a-z0-9_/\.-]+/[a-z0-9_\.-]+)/(\?[\w\#$%&~\-;:=,@+\.]+)?(&amp;|\?)start=([0-9]+)`i' );
				$pagin_replace = array( '\1' . $phpbb_seo->seo_delim['start'] . '\5\2\3', '\1/' . $phpbb_seo->seo_static['pagination'] . '\4' . $phpbb_seo->seo_ext['pagination'] . '\2' );

#
#-----[ REPLACE WITH ]------------------------------------------
#

				$pagin_find = array(
					// http://example.com/a_n-y/d.i.r/with.ext
					'`(https?\://[a-z0-9_/\.-]+/[a-z0-9_\.-]+)(\.[a-z0-9]+)(\?[\w$%&~\-;:=,@+\. ]+)?(#[a-z0-9_\.-]+)?(&amp;|\?)start=([0-9]+)`i',
					// http://example.com/a_n-y/d.i.r/withoutext
					'`(https?\://[a-z0-9_/\.-]+/[a-z0-9_-]+)/?(\?[\w$%&~\-;:=,@+\. ]+)?(#[a-z0-9_\.-]+)?(&amp;|\?)start=([0-9]+)`i'
				);
				$pagin_replace = array(
					// http://example.com/a_n-y/d.i.r/with-xx.ext
					'\1' . $phpbb_seo->seo_delim['start'] . '\6\2\3\4',
					// http://example.com/a_n-y/d.i.r/withoutext/pagexx.html
					'\1/' . $phpbb_seo->seo_static['pagination'] . '\5' . $phpbb_seo->seo_ext['pagination'] . '\2\3'
				);


#
#-----[ OPEN ]------------------------------------------
#

includes/session.php

#
#-----[ FIND ]------------------------------------------
#

			// www.phpBB-SEO.com SEO TOOLKIT BEGIN
			// This part will be fixed in 3.0.9 (see http://tracker.phpbb.com/browse/PHPBB3-9908)
			// But as is or updated, it will not mix well with USU and Zero Duplicate (which already removes them)
			// Bot user, if they have a SID in the Request URI we need to get rid of it
			// otherwise they'll index this page with the SID, duplicate content oh my!
			/*if (isset($_GET['sid']))
			{
				redirect(build_url(array('sid')));
			}*/
			// www.phpBB-SEO.com SEO TOOLKIT END

#
#-----[ REPLACE WITH ]------------------------------------------
# EG : Delete


#
#-----[ FIND ]------------------------------------------
#

		if ($bot && isset($_GET['sid']))
		{
			send_status_line(301, 'Moved Permanently');
			redirect(build_url(array('sid')));
		}

#
#-----[ REPLACE WITH ]------------------------------------------
#

		// www.phpBB-SEO.com SEO TOOLKIT BEGIN
		// This does not mix well with USU and Zero Duplicate (which already removes them)
		// Bot user, if they have a SID in the Request URI we need to get rid of it
		// otherwise they'll index this page with the SID, duplicate content oh my!
		/*if ($bot && isset($_GET['sid']))
		{
			send_status_line(301, 'Moved Permanently');
			redirect(build_url(array('sid')));
		}*/
		// www.phpBB-SEO.com SEO TOOLKIT END

#
#-----[ OPEN ]------------------------------------------
#

styles/prosilver/template/forum_fn.js

#
#-----[ FIND ]------------------------------------------
#

		if ( base_url.indexOf('?') >= 0 ) {
			document.location.href = base_url.replace(/&amp;/g, '&') + '&start=' + seo_page + anchor;
		} else if ( seo_page > 0 ) {
			var seo_type1 = base_url.match(/\.[a-z0-9]+$/i);
			if (seo_type1 !== null) {
				document.location.href = base_url.replace(/\.[a-z0-9]+$/i, '') + seo_delim_start + seo_page + seo_type1 + anchor;
			}
			var seo_type2 = base_url.match(/\/$/);
			if (seo_type2 !== null) {
				document.location.href = base_url + seo_static_pagination + seo_page + seo_ext_pagination + anchor;
			}
		} else {
			document.location.href = base_url + anchor;
		}

#
#-----[ REPLACE WITH ]------------------------------------------
#

		if ( seo_page > 0 ) {
			var phpEXtest = false;
			if ( base_url.indexOf('?') >= 0 || ( phpEXtest = base_url.match(/\.php$/i))) {
				document.location.href = base_url.replace(/&amp;/g, '&') + (phpEXtest ? '?' : '&') + 'start=' + seo_page + anchor;
			} else {
				var ext = base_url.match(/\.[a-z0-9]+$/i);
				if (ext) {
					// location.ext => location-xx.ext
					document.location.href = base_url.replace(/\.[a-z0-9]+$/i, '') + seo_delim_start + seo_page + ext + anchor;
				} else {
					// location and location/ to location/pagexx.html
					var slash = base_url.match(/\/$/) ? '' : '/';
					document.location.href = base_url + slash + seo_static_pagination + seo_page + seo_ext_pagination + anchor;
				}
			}
		} else {
			document.location.href = base_url + anchor;
		}

#
#-----[ OPEN ]------------------------------------------
#

styles/subsilver2/template/overall_header.html

#
#-----[ FIND ]------------------------------------------
#

		if ( base_url.indexOf('?') >= 0 ) {
			document.location.href = base_url.replace(/&amp;/g, '&') + '&start=' + seo_page + anchor;
		} else if ( seo_page > 0 ) {
			var seo_type1 = base_url.match(/\.[a-z0-9]+$/i);
			if (seo_type1 !== null) {
				document.location.href = base_url.replace(/\.[a-z0-9]+$/i, '') + seo_delim_start + seo_page + seo_type1 + anchor;
			}
			var seo_type2 = base_url.match(/\/$/);
			if (seo_type2 !== null) {
				document.location.href = base_url + seo_static_pagination + seo_page + seo_ext_pagination + anchor;
			}
		} else {
			document.location.href = base_url + anchor;
		}

#
#-----[ REPLACE WITH ]------------------------------------------
#

		if ( seo_page > 0 ) {
			var phpEXtest = false;
			if ( base_url.indexOf('?') >= 0 || ( phpEXtest = base_url.match(/\.php$/i))) {
				document.location.href = base_url.replace(/&amp;/g, '&') + (phpEXtest ? '?' : '&') + 'start=' + seo_page + anchor;
			} else {
				var ext = base_url.match(/\.[a-z0-9]+$/i);
				if (ext) {
					// location.ext => location-xx.ext
					document.location.href = base_url.replace(/\.[a-z0-9]+$/i, '') + seo_delim_start + seo_page + ext + anchor;
				} else {
					// location and location/ to location/pagexx.html
					var slash = base_url.match(/\/$/) ? '' : '/';
					document.location.href = base_url + slash + seo_static_pagination + seo_page + seo_ext_pagination + anchor;
				}
			}
		} else {
			document.location.href = base_url + anchor;
		}

#
#-----[ OPEN ]------------------------------------------
#

styles/subsilver2/template/viewtopic_body.html

#
#-----[ FIND ]------------------------------------------
#

<a href="#p{postrow.POST_ID}">

#
#-----[ REPLACE WITH ]------------------------------------------
#

<a href="{U_VIEW_TOPIC}#p{postrow.POST_ID}">

#
#-----[ SAVE/CLOSE ALL FILES ]------------------------------------------
#
# EoM
