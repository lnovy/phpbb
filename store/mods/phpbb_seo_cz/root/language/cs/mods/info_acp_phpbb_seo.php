<?php
/**
*
* info_acp_phpbb_seo [Čeština]
*
* @package Ultimate SEO URL phpBB SEO
* @version $Id: info_acp_phpbb_seo.php 174 2010-02-28 10:58:28Z hroudel $
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @license http://www.opensource.org/licenses/rpl1.5.txt Reciprocal Public License 1.5
*
*/
/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'ACP_CAT_PHPBB_SEO' => 'phpBB SEO',
	'ACP_MOD_REWRITE' => 'Nastavení přepisu URL odkazů',
	'ACP_PHPBB_SEO_CLASS' => 'Nastavení phpBB SEO',
	'ACP_FORUM_URL' => 'Správa URL odkazů fóra',
	'ACP_HTACCESS' => '.htaccess',
	'ACP_SEO_EXTENDED' => 'Rozšířené nastavení',
	'ACP_PREMOD_UPDATE' => '<h1>Oznámení o vydání</h1>
	<p>Tento update se týká jen premod, nikoli jádra phpBB.</p>
	<p>Nová verze phpBB SEO premod je k dispozici : %1$s<br/>Zkontrolujte<a href="%2$s" title="vlákno o vydání"><b> vlákno o vydání</b></a> a aktualizujte instalaci.</p>',
	'SEO_LOG_INSTALL_PHPBB_SEO' => '<strong>phpBB SEO mod rewrite nainstalován (v%s)</strong>',
	'SEO_LOG_INSTALL_PHPBB_SEO_FAIL' => '<strong>Pokus o nainstalování phpBB SEO mod rewrite selhal</strong><br/>%s',
	'SEO_LOG_UNINSTALL_PHPBB_SEO' => '<strong>phpBB SEO mod rewrite odinstalován (v%s)</strong>',
	'SEO_LOG_UNINSTALL_PHPBB_SEO_FAIL' => '<strong>Pokus o odinstalování phpBB SEO mod rewrite selhal</strong><br/>%s',
	'SEO_LOG_CONFIG_SETTINGS' => '<strong>Nastavení phpBB SEO změněno</strong>',
	'SEO_LOG_CONFIG_FORUM_URL' => '<strong>URL odkazy změněny</strong>',
	'SEO_LOG_CONFIG_HTACCESS' => '<strong>Nový .htaccess vygenerován</strong>',
	'SEO_LOG_CONFIG_EXTENDED' => '<strong>Rozšířené nastavení phpBB SEO změněno</strong>',
));

?>