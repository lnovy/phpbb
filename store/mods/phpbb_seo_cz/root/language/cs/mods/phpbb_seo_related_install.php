<?php
/**
*
* acp_phpbb_seo [Čeština]
*
* @package Ultimate SEO URL phpBB SEO
* @version $Id: phpbb_seo_related_install.php 178 2010-03-06 09:19:24Z hroudel $
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License v2
*
*/
/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	// ACP
	'SEO_RELATED' => 'Aktivace Příbuzných Témat',
	'SEO_RELATED_EXPLAIN' => 'Zobrazit seznam příbuzných témat na stránkach s tématy.<br/><b style="color:red;">Poznámka :</b><br/>S mySQL >=4.1 a tabulkou témat používající MyISAM, budou příbuzná témata získána za použití FullTextového indexu v názvu téma a budou seřazena dle důležitosti, v ostatních případech bude použito SQL LIKE a výsledky budou řazeny dle času zveřejnění',
	'SEO_RELATED_CHECK_IGNORE' => 'Ignorace cenzury slov',
	'SEO_RELATED_CHECK_IGNORE_EXPLAIN' => 'Ignorovat výjimky ze souboru search_ignore_words.php při vyhledávání příbuzných témat',
	'SEO_RELATED_LIMIT' => 'Limit příbuzných témat',
	'SEO_RELATED_LIMIT_EXPLAIN' => 'Maximální počet zobrazených příbuzných témat',
	'SEO_RELATED_ALLFORUMS' => 'Vyhledávání ve všech fórech',
	'SEO_RELATED_ALLFORUMS_EXPLAIN' => 'Prohledá všechna fóra namísto aktuálního.<br/><b style="color:red;">Poznámka :</b><br/>Vyhledávání ve všech fórech může být pomalé a nemusí nutně zobrazit lepší výsledky',
	// Install
	'INSTALLED' => 'phpBB SEO MOD Příbuzná Témata nainstalován',
	'ALREADY_INSTALLED' => 'phpBB SEO MOD Příbuzná Témata je již nainstalován',
	'FULLTEXT_INSTALLED' => 'MySQL FullTextový Index nainstalován',
	'FULLTEXT_NOT_INSTALLED' => 'MySQL FullTextový Index není k dispozici na tomto serveru, Namísto toho bude použito SQL LIKE',
	'INSTALLATION' => 'Instalace phpBB SEO Příbuzná Témata',
	'INSTALLATION_START' => '&rArr; <a href="%1$s" ><b>Pokračovat s instalací MODu</b></a><br/><br/>&rArr; <a href="%2$s" ><b>Opakovat nastavení FullTextového Indexu</b></a> (MySQL >= 4.1 používá MyISAM jen pro tabulku témat)<br/><br/>&rArr; <a href="%3$s" ><b>Pokračovat s odinstalací MODu</b></a>',
	// un-install
	'UNINSTALLED' => 'phpBB SEO MOD Příbuzná Témata odinstalován',
	'ALREADY_UNINSTALLED' => 'phpBB SEO MOD Příbuzná Témata je již odinstalován',
	'UNINSTALLATION' => 'Odinstalace phpBB SEO Příbuzná Témata',
	// SQL message
	'SQL_REQUIRED' => 'Zvolený db uživatel nemá dostatečná oprávnění ke změnám v tabulkách, musíte spustit tento dotaz ručně k aktivaci MySQL FullTextovéhu indexu :<br/>%1$s',
	// Security
	'SEO_LOGIN'		=> 'Board vyžaduje, abyste byl zaregistrován a přihlášen pro zobrazení požadované stránky.',
	'SEO_LOGIN_ADMIN'	=> 'Board vyžaduje, abyste byl přihlášen jako administrátor pro zobrazení požadované stránky.<br/>Vaše session byla z bezpečnostních důvodu zničena.',
	'SEO_LOGIN_FOUNDER'	=> 'Board vyžaduje, abyste byl přihlášen jakojeho zakladatel pro zobrazeni požadované stránky.',
	'SEO_LOGIN_SESSION'	=> 'Kontrola session selhala.<br/>Nastavení nebylo uloženo.<br/>Vaše session byla z bezpečnostních důvodu zničena.',
));
?>