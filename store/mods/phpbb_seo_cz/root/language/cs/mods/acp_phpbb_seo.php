<?php
/**
*
* acp_phpbb_seo [Čeština]
*
* @package Ultimate SEO URL phpBB SEO
* @version $Id: acp_phpbb_seo.php 178 2010-03-06 09:19:24Z hroudel $
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @license http://www.opensource.org/licenses/rpl1.5.txt Reciprocal Public License 1.5
*
*/
/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	// ACP Main CAT
	'ACP_CAT_PHPBB_SEO'	=> 'phpBB SEO',
	'ACP_MOD_REWRITE'	=> 'Nastavení přepisu URL odkazů',
	// ACP phpBB seo class
	'ACP_PHPBB_SEO_CLASS'	=> 'Nastavení phpBB SEO',
	'ACP_PHPBB_SEO_CLASS_EXPLAIN'	=> 'Zde můžete nastavit mnohé z voleb phpBB3 SEO mod rewrite.<br/>Různé defaultní nastavení jako oddělovače a přípony je nutné nastavit přímo v phpbb_seo_class.php, touto změnou je zahrnuta i aktualizace .htaccess a s největší pravděpodobností i správná přesměrování. %s',
	'ACP_PHPBB_SEO_VERSION' => 'Verze',
	'ACP_PHPBB_SEO_MODE' => 'Typ',
	'ACP_SEO_SUPPORT_FORUM' => 'Fórum podpory',
	// ACP forum urls
	'ACP_FORUM_URL'	=> 'Správa URL odkazů fóra',
	'ACP_FORUM_URL_EXPLAIN'		=> 'Zde můžete vidět, co obsahuje cache zahrnujíce i název fóra pro vkládání do URL odkazů.<br/>Fóra zabarvená zeleně jsou uložena v cache, ta zabarvená červeně nikoliv.<br/><br/><b style="color:red">Prosím, berte na vědomí, že</b><ul><b>jakýkoliv-název-fxx/</b> bude vždy správně přesměrován s aktivovaným módem "Nulový duplikát", ale nestane se tak, pokud editujete URL odkaz z <b>jakýkoliv-název/</b> na <b>něco-ještěněco/</b>.<br/> V takovém případě, bude s <b>jakýkoliv-název/</b> nyní zacházeno jako s neexistujícím fórem, pokud tedy nenastavíte příslušná přesměrování.</ul>',
	'ACP_NO_FORUM_URL'	=> '<b>Správa URL odkazů fóra deaktivována<b><br/>Správa URL odkazů fóra je dostupná pouze v Pokročilém a Smíšeném módu za předpokladu, že je povoleno ukládání URL odkazů do cache.<br/>Již nastavené URL odkazy zůstanou aktivní v Pokročilém a Smíšeném módu.',
	// ACP .htaccess
	'ACP_HTACCESS'	=> '.htaccess',
	'ACP_HTACCESS_EXPLAIN'	=> 'Tento nástroj vám pomůže s vytvořením vašeho .htaccess.<br/>Verze vypsaná níže je založena na nastavení ze souboru phpbb_seo/phpbb_seo_class.php.<br/>Editací hodnot proměnných $seo_ext a $seo_static před instalací .htaccess, dosáhnete vzhledu dle své představy.<br/>Např. místo .htm přípony můžete zvolit .html, \'message\' místo \'post\', \'mysite-team\' místo \'the-team\' atd.<br/>Pokud editujete tyto hodnoty a již jste oindexováni vyhledávači(SE), musíte si nastavit další přesměrování, aby odkazy byly stále platné.<br/>To ale neznamená, že by bylo původní nastavení špatné, pokud chcete, můžete tento krok bez obav přeskočit.<br/>Ačkoliv se doporučuje nastavit všechna přesměrování, tak jak jste měli doposud. Reindexace chvíli trvá a může vás tak okrást o nové návštěvníky, kteří tak budou přesměrováni na neexistující stránky.<br/>Následující původní .htaccess by měl být nahrán v root adresáři domény (např. www.example.com/.htaccess).<br/>Pokud je phpBB3 nainstalováno v podadresáři, zaškrtněte níže uvedené "Další možnosti", což přidá možnost nahrát .htaccess do daného adresáře.',
	'SEO_HTACCESS_RBASE'	=> 'Změnit umístění .htaccess?',
	'SEO_HTACCESS_RBASE_EXPLAIN' => 'Nastavení ¨"RewriteBase" umožnuje uložit .htaccess do adresáře s fórem. Obvykle je více příjemnější uložit .htaccess v root adresáři domény, ikdyž je phpBB3 nainstalováno v podadresáři. Ale někdo upřednostňuje uložení v adresáři, kde je umístěno fórum.',
	'SEO_HTACCESS_SLASH'	=> 'Odstranit RegEx lomítko pravé části "RewriteRule"',
	'SEO_HTACCESS_SLASH_EXPLAIN'	=> 'V závislosti na vašem hostiteli, možná budete chtít odstranit nebo přidat lomítka ("/") na začátku pravé části každého "Rewriterule". Tato konkrétní lomítka jsou defaultně použita, pokud je .htaccess umístěn v root adresáři. Je to opak pro případ, že je phpBB3 nainstalováno v podadresáři a vy byste chtěli umístit .htaccess do stejného adresáře.<br/>Původní nastavení by mělo obecně fungovat. V případě, žě nikoliv, zkuste reaktualizovat .htaccess stiknutím tlačítka "Reset" nebo tlačítkem "Odeslat".',
	'SEO_HTACCESS_WSLASH'	=> 'Přidat RegEx lomítko levé části "RewriteRule"',
	'SEO_HTACCESS_WSLASH_EXPLAIN'	=> 'V závislosti na vašem hostiteli, možná budete chtít přidat lomítka ("/") na začátku levé části každého "Rewriterule". Tato konkrétní lomítka ("/") nejsou defaultně použita.<br/>Původní nastavení by mělo obecně fungovat. V případě, žě nikoliv, zkuste reaktualizovat .htaccess stiknutím tlačítka "Reset" nebo tlačítkem "Odeslat".',
	'SEO_MORE_OPTION'	=> 'Další možnosti',
	'SEO_MORE_OPTION_EXPLAIN' => 'Pokud výše uvedený .htaccess nefunguje, tak jak má,<br/> ujistěte se, že je na serveru zapnut mod_rewrite nebo isapi_rewrite.<br/>Poté se ujistěte, že je umístěn ve správném adresáři a že se vám tzv. "netluče" s nastavením nadřazeného .htaccess.<br/>Pokud jste vše zkontrolovali a stále máte problémy zapněte "Další možnosti" a dejte odeslat.',
	'SEO_HTACCESS_SAVE' => 'Uložit .htaccess',
	'SEO_HTACCESS_SAVE_EXPLAIN' => 'Pokud tuto volbu zapnete, .htaccess soubor bude generován a uložen do adresáře phpbb_seo/cache/. Bude v něm uloženo vaše poslední nastavení, ale stále ho musíte přesunout na správné místo.',
	'SEO_HTACCESS_ROOT_MSG'	=> 'Jakmile jste připraveni, můžete zkopírovat .htaccess kód a vložit ho do vašeho .htaccess souboru anebo použít níže uvedenou možnost "Uložit .htaccess".<br/> Tento .htaccess je určen k použití v root adresáři dané domény, což je ve vašem případě <u>%1$s</u> na vašem FTP.<br/><br/>Můžete generovat .htaccess určený k použití v phpBB3 podadresáři pomocí volby "Další možnosti" uvedené níže.',
	'SEO_HTACCESS_FOLDER_MSG' => 'Jakmile jste připraveni, můžete zkopírovat .htaccess kód a vložit ho do vašeho .htaccess souboru anebo použít níže uvedenou možnost "Uložit .htaccess".<br/> Tento .htaccess je určen k použití v adresáři, kde je phpBB3 nainstalováno, což je ve vašem případě <u>%1$s</u> na vašem FTP.',
	'SEO_HTACCESS_CAPTION' => 'Legenda',
	'SEO_HTACCESS_CAPTION_COMMENT' => 'Komentáře',
	'SEO_HTACCESS_CAPTION_STATIC' => 'Statické části, editovatelné v phpbb_seo_class.php',
	'SEO_HTACCESS_CAPTION_DELIM' => 'Oddělovače, editovatelné v phpbb_seo_class.php',
	'SEO_HTACCESS_CAPTION_SUFFIX' => 'Přípony, editovatelné v phpbb_seo_class.php',
	'SEO_HTACCESS_CAPTION_SLASH' => 'Volitelná lomítka',
	'SEO_SLASH_DEFAULT'	=> 'Původní',
	'SEO_SLASH_ALT'		=> 'Nahradit',
	'SEO_MOD_TYPE_ER'	=> 'Daný typ mod rewrite není správně nastaven v phpbb_seo/phpbb_seo_class.php.',
	'SEO_SHOW'		=> 'Ukázat',
	'SEO_HIDE'		=> 'Skrýt',
	'SEO_SELECT_ALL'	=> 'Označit vše',
	// ACP extended
	'ACP_SEO_EXTENDED_EXPLAIN' => 'Rozšířené nastavení phpBB SEO MODu.',
	'SEO_EXTERNAL_LINKS' => 'Externí odkazy',
	'SEO_EXTERNAL_LINKS_EXPLAIN' => 'Otevírat externí odkazy v novém oknu/panelu prohlížeče',
	'SEO_EXTERNAL_SUBDOMAIN' => 'Subdoménové odkazy',
	'SEO_EXTERNAL_SUBDOMAIN_EXPLAIN' => 'Otevírat subdoménové odkazy (v rámci domény fóra) v novém oknu/panelu prohlížeče',
	'SEO_EXTERNAL_CLASSES' => 'Externí CSS třídy',
	// Titles
	'SEO_PAGE_TITLES' => '<a href="http://www.phpbb-seo.com/en/phpbb-seo-toolkit/optimal-titles-t1289.html" title="Optimal Titles mod" onclick="window.open(this.href); return false;">Názvy stránek</a>',
	'SEO_APPEND_SITENAME' => 'Přidát název fóra do názvu stránky',
	'SEO_APPEND_SITENAME_EXPLAIN' => 'Umožní přidat název fóra do názvu stránek.<br /><b style="color:red;">Upozornění :</b><br/>Tato vlastnost vyžaduje správné editované overall_header.html použitím Optimal titles mod, název fóra může být opakován v názvech stránky',
	// Meta
	'SEO_META' => '<a href="http://www.phpbb-seo.com/en/phpbb-seo-toolkit/seo-dynamic-meta-tags-t1308.html" title="Dynamic Meta tags mod" onclick="window.open(this.href); return false;">Meta tags (HTML značka, která je umístěna v záhlaví stránek v tagu head. Je v ní obsažen například popis obsahu webu, klíčová slova. ...)</a>',
	'SEO_META_TITLE' => 'Meta title (nadpis, dále jen title)',
	'SEO_META_TITLE_EXPLAIN' => 'Výchozí Meta tag title je použit na stránce, kde není definován. Pokud necháte toto nastavení prázdné, bude ignorováno.',
	'SEO_META_DESC' => 'Meta description (popis, dále jen description)',
	'SEO_META_DESC_EXPLAIN' => 'Výchozí Meta tag description je použit na stránce, kde není definován.',
	'SEO_META_DESC_LIMIT' => 'Omezení Meta description',
	'SEO_META_DESC_LIMIT_EXPLAIN' => 'Omezí na určitý počet slov Meta tag description',
	'SEO_META_BBCODE_FILTER' => 'Filtr BBcode',
	'SEO_META_BBCODE_FILTER_EXPLAIN' => 'Čárkou oddělený seznam BBcode, které budou úplně filtrovány v meta tags. Ostatní mohou být použity jako zdroj pro obsah meta tagu.<br/> Výchozí filtrované BBcode jsou : <b>img,url,flash,code</b>.<br/><b style="color:red;">Upozornění :</b><br/>Nefiltrování tagů img, url a flash není dobrý nápad, stejně tak jako tag code ve většině případů. Důvodem jsou obecná témata, která se skrzy tyto tagy publikují, Nefiltrujte jen BBcode tagy, které obsahují informace, jenž by mohly být z hlediska SEO důležité pro meta tagy, tj. obsah, na který se vaše fórum zaměřuje.',
	'SEO_META_KEYWORDS' => 'Meta keywords (klíčová slova, dále jen keywords)',
	'SEO_META_KEYWORDS_EXPLAIN' => 'Výchozí Meta tag keywords je použit na stránce, kde není definován. Jednoduše vložte vaše klíčová slova',
	'SEO_META_KEYWORDS_LIMIT' => 'Omezení Meta keywords',
	'SEO_META_KEYWORDS_LIMIT_EXPLAIN' => 'Omezí na určitý počet slov Meta tag keywords',
	'SEO_META_MIN_LEN' => 'Filtr krátkých slov',
	'SEO_META_MIN_LEN_EXPLAIN' => 'Minimální počet znaků, které musí slova mít, aby mohly být zahrnuty v Meta tagu keywords, jen slova, která splňují tento limit budou počítána',
	'SEO_META_CHECK_IGNORE' => 'Ignorování filtru slov',
	'SEO_META_CHECK_IGNORE_EXPLAIN' => 'Použití tohoto nastavení má za následek ignoraci výjimek v search_ignore_words.php a tím připustí použití v Meta tagu keywords',
	'SEO_META_LANG' => 'Meta lang (jazyk, dále jen lang)',
	'SEO_META_LANG_EXPLAIN' => 'Kód jazyku použitý v meta tagu lang',
	'SEO_META_COPY' => 'Meta copyright (autorské právo, dále jen copyright)',
	'SEO_META_COPY_EXPLAIN' => 'Copyright je použit v meta tagu. Pokud necháte toto nastavení prázdné, bude ignorováno.',
	'SEO_META_FILE_FILTER' => 'Filtr souborů',
	'SEO_META_FILE_FILTER_EXPLAIN' => 'Čárkou oddělený seznam názvů php souborů, které by neměly být indexovány (robots:noindex,follow). Příklad : ucp,mcp',
	'SEO_META_GET_FILTER' => '_GET filtr',
	'SEO_META_GET_FILTER_EXPLAIN' => 'Čárkou oddělený seznam _GET proměnných, které by neměly být indexovány (robots:noindex,follow). Příklad : style,hilit,sid',
	'SEO_META_ROBOTS' => 'Meta Robots',
	'SEO_META_ROBOTS_EXPLAIN' => 'Meta tag Robots napovídá botům, jak mají indexovat vaše stránky. Výchozí nastavení je "index,follow", které botům umožňuje indexovat, cachovat vaše stránky a následovat v nich odkazy. Pokud necháte toto nastavení prázdné, bude ignorováno.<br/><b style="color:red;">Varování :</b><br/>Tento tag může být nebezpečný, pokud někde použijete např. "noindex", tak tyto stránky nebudou jednoduše indexovány boty.',
	'SEO_META_NOARCHIVE' => 'Noarchive Meta Robots (nearchivovat, dále už jen Noarchive)',
	'SEO_META_NOARCHIVE_EXPLAIN' => 'Noarchive Meta Robots tag napovídá botům, zda mají, či nemají cachovat danou stránku. Týká se jen cachování, nemá význam pro indexování či SERPs(výsledky vyhledávání v search enginech) stránky.<br/>Zde můžete ze seznamu vybrat fóra, která ponesou znak "noarchive", který bude přidán do meta tagu robots.<br/>Tato funkce může být užitečná např. v situaci, kdy máte některá fóra přístupná botům, ale ne pro neregistrované uživatele. Přidáním znaku "noarchive" zabráníte neregistrovaným uživatelům v přístupu obsahu skrze search engine cache, dokud budou fórum a jeho témata zobrazovány v SERPs',
	// Install
	'SEO_INSTALL_PANEL'	=> 'Instalační panel phpBB3 SEO',
	'SEO_ERROR_INSTALL'	=> 'Vyskytla se chyba v průběhu instalace. Doporučujeme odinstalovat a začít znovu.',
	'SEO_ERROR_INSTALLED'	=> 'Modul %s je již nainstalován.',
	'SEO_ERROR_ID'	=> 'Modul %1$ nemá žádné ID.',
	'SEO_ERROR_UNINSTALLED'	=> 'Modul %s byl již odinstalován.',
	'SEO_ERROR_INFO'	=> 'Informace :',
	'SEO_FINAL_INSTALL_PHPBB_SEO'	=> 'Přihlásit do ACP',
	'SEO_FINAL_UNINSTALL_PHPBB_SEO'	=> 'Návrat na obsah fóra',
	'CAT_INSTALL_PHPBB_SEO'	=> 'Instalace',
	'CAT_UNINSTALL_PHPBB_SEO'	=> 'Odinstalace',
	'SEO_OVERVIEW_TITLE'	=> 'Přehled phpBB3 SEO mod rewrite',
	'SEO_OVERVIEW_BODY'	=> 'Vítejte do veřejného vydání %1$s phpBB3 SEO mod rewrite modifikace %2$s.</p><p>Prosím, přečtěte si <a href="%3$s" title="Zkontrolujte vlákno o vydání" target="_phpBBSEO"><b>vlákno o vydání</b></a> pro více informací</p><p><strong style="text-transform: uppercase;">Poznámka:</strong> Musíte mít již provedeny potřebné změny v kódu a nahrány všechny nové soubory předtím než budete pokračovat v této instalaci.</p><p><strong style="text-transform: uppercase;">Poznámka k překladu:</strong> Záměrně není všude přeloženo mod rewrite jako přepisovací mód či jiné hezké české oblůdky. Většina z vás používá server na bázi Apache, čili pro vás bude srozumitelnější mod rewrite než přepisovací mód apod. A vy, kteří máte server na bázi IIS, si prostě musíte zvyknout :) V některých důležitých pasážích jsou uvedeny v závorkách hrubé překlady těchto slovních spojení.</p><p>Tento instalační systém vás provede celým procesem instalace modifikace phpBB3 SEO a jeho ACP. To vám umožní bezpečně zvolit vaší techniku přepisu phpBB3 URL odkazů pro nejlepší výsledky ve vyhledávačích typu Google, Seznam atd.</p>.',
	'CAT_SEO_PREMOD'	=> 'phpBB SEO premod',
	'SEO_PREMOD_TITLE'	=> 'Přehled phpBB SEO premod',
	'SEO_PREMOD_BODY'	=> 'Vítejte do veřejného vydání phpBB SEO premod.</p><p>Prosím, přečtěte si <a href="http://www.phpbb-seo.com/en/phpbb-seo-premod/seo-url-premod-t1549.html" title="Zkontrolujte vlákno o vydání" target="_phpBBSEO"><b>vlákno o vydání</b></a> pro více informací</p><p><strong style="text-transform: uppercase;">Poznámka:</strong> V nastavení phpBB3 SEO si budete moci vybrat mezi třemi mod rewrite modifikacemi pro phpBB3 SEO.</p><p><strong style="text-transform: uppercase;">Poznámka k překladu:</strong> Záměrně není všude přeloženo mod rewrite jako přepisovací mód či jiné hezké české oblůdky. Většina z vás používá server na bázi Apache, čili pro vás bude srozumitelnější mod rewrite či přepis URL odkazů než jen přepisovací mód apod. A vy, kteří máte server na bázi IIS, si prostě musíte zvyknout :) V některých důležitých pasážích jsou uvedeny v závorkách hrubé překlady těchto slovních spojení.</p><br/><br/><b>Jsou dostupné následující mod rewrite modifikace pro přepis URL odkazů :</b><ul><li><a href="http://www.phpbb-seo.com/en/simple-seo-url/simple-phpbb-seo-url-t1566.html" title="Více informací o Prosté modifikaci"><b>Prostá modifikace</b></a>,</li><li><a href="http://www.phpbb-seo.com/en/mixed-seo-url/mixed-phpbb-seo-url-t1565.html" title="Více informací o Smíšené modifikaci"><b>Smíšená modifikace</b></a>,</li><li><a href="http://www.phpbb-seo.com/en/advanced-seo-url/advanced-phpbb-seo-url-t1219.html" title="Více informací o Pokročilé modifikaci"><b>Pokročilá modifikace</b></a>.</li></ul>Tato volba je velice důležitá, doporučujeme vám, udělat si čas a prozkoumat jednotlivé aspekty a rysy SEO modifikace předtím než svoje fórum zpřístupníte online.<br/>Tato modifikace je stejně jednoduchá k instalaci jako samotné phpBB3, stačí projít následujícími kroky.<br/><br/>
	<p>Požadavky pro mod rewrite modifikaci pro přepis URL odkazů :</p>
	<ul>
		<li>Apache server (linux OS) obsahujicí modul mod_rewrite.</li>
		<li>IIS server (windows OS) obsahující modul isapi_rewrite, ale budete muset přízpůsobit přepisovací pravidla v httpd.ini</li>
	</ul>
	<p>Po instalaci, musíte aktivovat a nastavit mód v ACP.</p>',
	'SEO_LICENCE_TITLE'	=> 'RECIPROCAL PUBLIC LICENSE',
	'SEO_LICENCE_BODY'	=> 'phpBB3 SEO mod rewrite (modifikace pro přepis URL odkazů) modifikace jsou vydávány pod RPL licencí, ve které je ukotveno, že nemůžete odstranit či měnit phpBB3 SEO autorské odkazy.<br/>Pro více informací o možných výjimkách, prosím, kontaktujte phpBB3 SEO administrátory (přednostně SeO nebo dcz).',
	'SEO_PREMOD_LICENCE'	=> 'phpBB3 SEO mod rewrite (modifikace pro přepis URL odkazů) a Zero duplicate (Nulový duplikát) modifikace, jež jsou obsaženy v této pre-modifikaci, jsou vydávány pod RPL licencí, ve které je ukotveno, že nemůžete odstranit či měnit phpBB3 SEO autorské odkazy.<br/>Pro více informací o možných výjimkách, prosím, kontaktujte phpBB3 SEO administrátory (přednostně SeO nebo dcz).',
	'SEO_SUPPORT_TITLE'	=> 'Podpora',
	'SEO_SUPPORT_BODY'	=> 'Plná podpora je poskytována na <a href="%1$s" title="Navštívit %2$s SEO URL fórum" target="_phpBBSEO"><b>%2$s SEO URL fóru</b></a>. Rádi vám pomůžeme s dotazy ohledně obecného nastavení, konfiguračních problémů a v neposlední řadě i s určením příčiny chyb.</p><p>Neváhejte a navštivte naše <a href="http://www.phpbb-seo.com/en/" title="SEO Fórum" target="_phpBBSEO"><b>fóra věnovaná optimalizaci webových stránek pro internetové vyhledávače typu Google apod. ve zkratce SEO (optimalizace pro vyhledávače)</b></a>.</p><p>Abyste se ujistili, že máte poslední verzi phpBB3 SEO a že vám neujdou žádné novinky ze světa phpBB3 SEO, měli byste se <a href="http://www.phpbb-seo.com/en/ucp.php?mode=register" title="Zaregistrovat na phpBB3 SEO" target="_phpBBSEO"><b>zaregistrovat</b></a>, přihlásit a <a href="%3$s" title="Upozornit na nové verze" target="_phpBBSEO"><b>začít odebírat novinky z vlákna o vydání</b></a> a být tedy upozorňováni emailem na každou novou aktualizaci.',
	'SEO_PREMOD_SUPPORT_BODY'	=> 'Plná podpora je poskytována na <a href="http://www.phpbb-seo.com/en/phpbb-seo-premod/seo-url-premod-t1549.html" title="Navštívit fórum phpBB SEO premod" target="_phpBBSEO"><b>fóru phpBB SEO premod</b></a>. Rádi vám pomůžeme s dotazy ohledně obecného nastavení, konfiguračních problémů a v neposlední řadě i s určením příčiny chyb.</p><p>Neváhejte a navštivte naše <a href="http://www.phpbb-seo.com/en/" title="SEO Fórum" target="_phpBBSEO"><b>fóra věnovaná optimalizaci webových stránek pro internetové vyhledávače typu Google apod. ve zkratce SEO (optimalizace pro vyhledávače)</b></a>.</p><p>Abyste se ujistili, že máte poslední verzi phpBB3 SEO a že vám neujdou žádné novinky ze světa phpBB3 SEO, měli byste se <a href="http://www.phpbb-seo.com/en/ucp.php?mode=register" title="Zaregistrovat na phpBB3 SEO" target="_phpBBSEO"><b>zaregistrovat</b></a>, přihlásit a <a href="http://www.phpbb-seo.com/en/viewtopic.php?t=1549&watch=topic" title="Upozornit na nové verze" target="_phpBBSEO"><b>začít odebírat novinky z vlákna o vydání</b></a> a být tedy upozorňováni emailem na každou novou aktualizaci.',
	'SEO_INSTALL_INTRO'		=> 'Vítejte v instalaci phpBB3 SEO',
	'SEO_INSTALL_INTRO_BODY'	=> '<p>Chystáte se nainstalovat %1$s phpBB3 SEO mod rewrite %2$s. Tento instalační nástroj aktivuje phpBB3 SEO mod rewrite ovládací panel phpBB3 ACP.</p><p>Po instalaci musíte v ACP mód nastavit a aktivovat.</p>
	<p><strong>Poznámka:</strong> Pokud jste novým uživatelem tohoto módu, vřele vám doporučujeme udělat si čas a otestovat všechny aspekty změn URL odkazů, a to nejlépe na soukromém nebo lokálním serveru. Tímto způsobem zabráníte nechtěné indexaci vašich stránek, abyste po čase zjistili, ze vyhledávače mají zaindexované jiné URL odkazy než ty vaše nyní zvolené. Trpělivost je jedna ruka se SEO optimalizací a to i v případě použití "Nulového duplikátu", který dělá HTTP přesměrování skutečně jednoduchým. Není ve vašem zájmu, aby byly URL odkazy fór přesměrovávány příliš často.</p><br/>
	<p>Požadavky :</p>
	<ul>
		<li>Apache server (linux OS) obsahujicí modul mod_rewrite.</li>
		<li>IIS server (windows OS) obsahující modul isapi_rewrite, ale budete muset přízpůsobit přepisovací pravidla v httpd.ini</li>
	</ul>',
	'SEO_INSTALL'		=> 'Nainstalovat',
	'UN_SEO_INSTALL_INTRO'		=> 'Vítejte v odinstalaci phpBB3 SEO',
	'UN_SEO_INSTALL_INTRO_BODY'	=> '<p>Chystáte se odinstalovat %1$s phpBB3 SEO mod rewrite %2$s ACP modul.</p>
	<p><strong>Poznámka:</strong> Tímto, nedeaktivujete přepis URL odkazů na vašem fóru, a to do té doby než odstraníte mod rewrite z modifikovaných phpBB3 souborů.</p>',
	'UN_SEO_INSTALL'		=> 'Odinstalovat',
	'SEO_INSTALL_CONGRATS'			=> 'Gratulujeme!',
	'SEO_INSTALL_CONGRATS_EXPLAIN'	=> '<p>Úspěšně jste nainstalovali %1$s phpBB3 SEO mod rewrite %2$s. Nyní byste měli nastavit pravidla módu v phpBB3 ACP.<p>
	<p>V nové phpBB3 SEO kategorii, budete moci :</p>
	<h2>Nastavit a aktivovat přepis URL odkazů</h2>
		<p>Udělejte si čas s nastavením URL odkazů a jejich vzhledu. Volba "Nulový duplikát" bude po instalaci také nastavena zde.</p>
	<h2>Opatrně zvolte URL odkaz vašeho fóra</h2>
		<p>Použitím Smíšeného nebo Pokročilého módu, budete moci odlišit URL odkaz fóra od jeho názvu a zvolit svá klíčová slova, která v něm použijete.</p>
	<h2>Vygenerovat váš .htaccess</h2>
	<p>Jakmile budete mít nastavené výše uvedené možnosti, budete moci vygenerovat váš .htaccess a zároveň uložit přímo na váš server.</p>',
	'UN_SEO_INSTALL_CONGRATS'	=> 'phpBB3 SEO ACP modul byl odstraněn.',
	'UN_SEO_INSTALL_CONGRATS_EXPLAIN'	=> '<p>Úspěšně jste odinstalovali %1$s phpBB3 SEO mod rewrite %2$s.<p>
	<p>Tímto, nedeaktivujete přepis URL odkazů na vašem fóru, a to do té doby než odstraníte mod rewrite z modifikovaných phpBB3 souborů.</p>',
	'SEO_VALIDATE_INFO'	=> 'Info o potvrzení :',
	'SEO_SQL_ERROR' => 'SQL chyba',
	'SEO_SQL_TRY_MANUALLY' => 'Zvolený db uživatel nemá dostatečná oprávnění ke spuštění požadovaného SQL dotazu, prosím, spusťte jej ručně (phpMyadmin) :',
	// Security
	'SEO_LOGIN'		=> 'Board vyžaduje, abyste byl zaregistrován a přihlášen pro zobrazení požadované stránky.',
	'SEO_LOGIN_ADMIN'	=> 'Board vyžaduje, abyste byl přihlášen jako administrátor pro zobrazení požadované stránky.<br/>Vaše session byla z bezpečnostních důvodu zničena.',
	'SEO_LOGIN_FOUNDER'	=> 'Board vyžaduje, abyste byl přihlášen jakojeho zakladatel pro zobrazeni požadované stránky.',
	'SEO_LOGIN_SESSION'	=> 'Kontrola session selhala.<br/>Nastavení nebylo uloženo.<br/>Vaše session byla z bezpečnostních důvodu zničena.',
	// Cache status
	'SEO_CACHE_FILE_TITLE'	=> 'Stav cache',
	'SEO_CACHE_STATUS'	=> 'Cache je umístěna v adresáři : <b>%s</b>',
	'SEO_CACHE_FOUND'	=> 'Adresář cache byl úspěšně nalezen.',
	'SEO_CACHE_NOT_FOUND'	=> 'Adresář cache nebyl nalezen.',
	'SEO_CACHE_WRITABLE'	=> 'Adresář cache je zapisovatelný.',
	'SEO_CACHE_UNWRITABLE'	=> 'Do adresáře cache nelze zapisovat. Musíte příslušný adresář CHMOD na  0777.',
	'SEO_CACHE_INNER_UNWRITABLE' => 'Některé soubory uvnitř cache adresáře nemůžou být zapsány, zkontrolujte, zda jste správně nastavili CHMOD adresáře cache a všem souborům v něm obsažených.',
	'SEO_CACHE_FORUM_NAME'	=> 'Název fóra',
	'SEO_CACHE_URL_OK'	=> 'URL odkaz uložený v cache',
	'SEO_CACHE_URL_NOT_OK'	=> 'URL odkaz není zatím uložen v cache',
	'SEO_CACHE_URL'		=> 'Finální URL odkaz',
	'SEO_CACHE_MSG_OK'	=> 'Cache byla úspěšně aktualizována.',
	'SEO_CACHE_MSG_FAIL'	=> 'Nastala chyba při aktualizaci cache.',
	'SEO_CACHE_UPDATE_FAIL'	=> 'Tento URL odkaz nemůže být použit, cache nebude aktualizována.',
	// Seo advices
	'SEO_ADVICE_DUPE'	=> 'Byl detekován duplikátní záznam v názvu URL odkazu fóra : <b>%1$s</b>.<br/>Zůstane nedotčený, dokud ho neaktualizujete.',
	'SEO_ADVICE_RESERVED'	=> 'Bylo detekováno rezervované slovo (používané v jiných URL odkazech, jako je uživatelský profil apod.) v názvu URL odkazu fóra : <b>%1$s</b>.<br/>Zůstane nedotčený, dokud ho neaktualizujete.',
	'SEO_ADVICE_LENGTH'	=> 'URL odkaz uložený v cache je příliš dlouhý.<br/>Zvolte si jiný a kratší.',
	'SEO_ADVICE_DELIM'	=> 'URL odkaz uložený v cache obsahuje SEO oddělovač a ID.<br/>Zvolte si jiný/é.',
	'SEO_ADVICE_WORDS'	=> 'URL odkaz uložený v cache obsahuje příliš mnoho slov.<br/>Zvolte si nějaký lepší.',
	'SEO_ADVICE_DEFAULT'	=> 'Finální URL odkaz je po zformátování bohužel stejná jako původní.<br/>Zvolte si jinou.',
	'SEO_ADVICE_START'	=> 'URL odkazy fór nemůžou končit parametry "pagination" (stránkování).<br/>Proto byly odstraněny z vašeho požadavku.',
	'SEO_ADVICE_DELIM_REM'	=> 'Požadované URL odkazy fór nemůžou končit oddělovačem fóra.<br/>Proto byly odstraněny z vašeho požadavku.',
	// Mod Rewrite type
	'ACP_SEO_SIMPLE'	=> 'Jednoduchý',
	'ACP_SEO_MIXED'		=> 'Smíšený',
	'ACP_SEO_ADVANCED'	=> 'Pokročilý',
	'ACP_ULTIMATE_SEO_URL'	=> 'Ultimate SEO URL',
	// URL Sync
	'SYNC_REQ_SQL_REW' => 'Musíte aktivovat SQL Přepis, abyste mohli tento skript používat!',
	'SYNC_TITLE' => 'URL Synchronizace',
	'SYNC_WARN' => 'Upozornění, neukončujte skript dokud neskončí. Před použitím výrazně doporučujeme zálohovat vaši databázi!',
	'SYNC_COMPLETE' => 'Synchronizace dokončena!',
	'SYNC_RESET_COMPLETE' => 'Reset dokončen!',
	'SYNC_PROCESSING' => '<b>Pracuji, prosím strpení ...</b><br/><br/><b>%1$s%%</b> bylo již zpracováno. <br/>Zatím, <b>%2$s</b> položek bylo zpracováno.<br/><b>%3$s</b> položek celkem, <b>%4$s</b> je zpracováno v čase.<br/>Rychlost : <b>%5$s polož-ka/ek.</b><br/>Čas strávený provedením této operace : <b>%6$ss</b><br/>Odhadovaný zbývající čas : <b>%7$s minut-a(y)</b>',
	'SYNC_ITEM_UPDATED' => '<b>%1$s</b> položek bylo aktualizováno',
	'SYNC_TOPIC_URLS' => 'Začít synchronizaci URL odkazů témat',
	'SYNC_RESET_TOPIC_URLS' => 'Resetovat všechny URL odkazy témat',
	'SYNC_TOPIC_URL_NOTE' => 'Právě jste aktivovali volbu SQL Přepis, nyní byste měli synchronizovat URL odkazy všech témat, a to na %stéto stránce%s, pokud jste to již neudělali.<br/>Toto nezmění žádný ze stávajících URL odkazů<br/><b style="color:red">Prosím, čtěte :</b><br/><em>Měli byste synchronizovat URL odkazy témat teprve tehdy až dokončíte nastavení vzor vašich odkazů. V podstatě se nic nestane, pokud změníte vzor vašich odkazů, poté co jste synchronizovali odkazy témat, ale měli byste provést synchronizaci pokaždé, když změníte vzor.<br/>Pokud synchronizaci neprovedete, tak stále není důvod k obavám. URL odkazy témat budou v takovém případě aktualizovány při každé návštěvě tématu, za podmínky, že odkaz v db nebyl uložen nebo neodpovídá stávajícímu vzoru URL odkazů.</em>',
	// phpBB SEO Class option
	'url_rewrite' => 'Aktivovat přepis URL odkazů',
	'url_rewrite_explain' => 'Jakmile budete mít nastaveny níže uvedené možnosti a vygenerován váš .htaccess, můžete aktivovat přepis URL odkazů a zkontrolovat zda vaše URL odkazy fungují správně. Pokud vidíte chybová hlášení 404, je to s největší pravděpodobností problém s .htaccess. Zkuste speciální volby nástroje pro vygenerování .htaccess.',
	'modrtype' => 'Druh přepisu URL odkazů',
	'modrtype_explain' => 'phpBB SEO premod je kompatibilní s třemi phpBB3 SEO mod rewrite módy.<br/><a href="http://www.phpbb-seo.com/en/simple-seo-url/simple-phpbb-seo-url-t1566.html" title="Více detailů o Jednoduchém módu"><b>Jednoduchý</b></a>, <a href="http://www.phpbb-seo.com/en/mixed-seo-url/mixed-phpbb-seo-url-t1565.html" title="Více detailů o Smíšeném módu"><b>Smíšený</b></a> a <a href="http://www.phpbb-seo.com/en/advanced-seo-url/advanced-phpbb-seo-url-t1219.html" title="Více detailů o Pokročilém módu"><b>Pokročilý</b></a>.<br/><b style="color:red">Prosím, čtěte</b><br/><ul style="margin-left:20px">Změna této volby bude mít vliv na vzhled všech URL odkazů na vašem fóru.<br/>Pokud již máte zaindexované fórum boty, měli byste tuto změnu či aktivaci zvážit, jelikož bude mít vliv na zpětnou vazbu s vyhledávači. <br/>Snažte, proto měnit toto nastavení minimálně.</ul>',
	'sql_rewrite' => 'Aktivovat SQL Přepis',
	'sql_rewrite_explain' => 'Tato volba vám umožní zvolit si URL odkaz pro každé téma. Budete moci přesně zvolit URL při publikaci nového tématu nebo při editaci stávajícího. Tato funkce je nicméně omezena jen pro administrátory a moderátory.<br/><br/><b style="color:red">Prosím, čtěte :</b><br/><em>Zapnutím této volby nezměníte URL odkazy témat. Stávající URL odkazy budou uloženy v databázi, tak jak byly zobrazovány. Ale v případě vypnutí této volby se nové odkazy mohou lišit od těch předešlých. V takovém případě se individualizované URL odkazy mohou chovat jako by individualizované nebyly.<br/>Tato funkce má také obrovskou výhodu v rychlejším přepisu URL odkazů, obzvláště pokud používáte volbu Virtuální adresář v pokročilém modu. Tímto velmi usnadníte získávání přepsaných URL odkazů jakékoli stránky.</em>',
	'profile_inj' => 'Injekce profilů a skupin',
	'profile_inj_explain' => 'Zde si můžete zvolit zda vkládat přezdívky, názvy skupin a uživatelských příspěvků (volitelné, vizte níže) v URL odkazech místo původního statického přepisu, <b>phpBB/nickname-uxx.html</b> místo <b>phpBB/memberxx.html</b>.<br/><b style="color:red">Prosím, čtěte</b><br/><ul style="margin-left:20px">Změna tohoto nastavení vyžaduje aktualizaci .htaccess</ul>',
	'profile_vfolder' => 'Virtuální adresáře profilů',
	'profile_vfolder_explain' => 'Zde můžete simulovat adresářovou strukturu pro profily a uživatelské příspěvky (volitelné, vizte níže) v URL odkazech, <b>phpBB/nickname-uxx/(topics/)</b> nebo <b>phpBB/memberxx/(topics/)</b> místo <b>phpBB/nickname-uxx(-topics).html</b> a <b>phpBB/memberxx(-topics).html</b>.<br/><b style="color:red">Prosím, čtěte</b><br/><ul style="margin-left:20px">Odstranění ID profilu ruší toto nastavení.<br/>Změna tohoto nastavení vyžaduje aktualizaci .htaccess</ul>',
	'profile_noids' => 'Odstranění ID profilu',
	'profile_noids_explain' => 'V případě, že je aktivována injekce profilů a skupin, můžete si vybrat mezi <b>example.com/phpBB/member/nickname</b> místo původního zobrazení <b>example.com/phpBB/nickname-uxx.html</b>. phpBB3 používá zvláštní, ikdyž jednoduchý, SQL dotaz pro tento druh zobrazení bez uživatelského ID.<br/><b style="color:red">Prosím, čtěte</b><br/><ul style="margin-left:20px">Speciální znaky nejsou zpracovány všemi prohlížeči stejným způsobem. FF vždy používá "urlencode" (<a href="http://www.php.net/urlencode"><b>urlencode()</b></a>) a jak se zdá, primárně používá Latin1, kdežto IE a Opera nikoliv. Pro pokročilé "urlencoding" nastavení, prosím, čtěte instalační soubor.<br/>Změna tohoto nastavení vyžaduje aktualizaci .htaccess</ul>',
	'rewrite_usermsg' => 'Přepis běžného vyhledávání a uživatelských příspěvků',
	'rewrite_usermsg_explain' => 'Toto nastavení má smysl v případě, že máte povolen veřejný přístup jak do profilů, tak do vyhledávání.<br/> Použitím této volby pravděpodobně zaznamenáte zvýšenou aktivitu používání funkcí vyhledávání a tím vyšší zátěž serveru.<br/> Typ přepisu URL odkazů (s anebo bez ID) se bude řídit nastavením pro profily a skupiny(vizte výše).<br/><b>phpBB/messages/nickname/topics/</b> versus <b>phpBB/nickname-uxx-topics.html</b> versus <b>phpBB/memberxx-topics.html</b>.<br/>Dodatečně tato volba aktivuje přepis klasického vyhledávání, stejně tak jako přepis aktivních a nezodpovězených témat a stránek s novými příspěvky.<br/><b style="color:red">Prosím, čtěte</b><br/><ul style="margin-left:20px">Odstranění ID bude znamenat stejné omezení jak je výše popsáno u uživatelského profilu.<br/>Změna tohoto nastavení vyžaduje aktualizaci .htaccess</ul>',
	'rewrite_files' => 'Přepis příloh',
	'rewrite_files_explain' => 'Aktivovat přepis příloh. Tato volba může být velkým pomocníkem, pokud se v přílohách objevují soubory(obrázky), které chcete mít zaznamenány v indexu vyhledávacích enginů. Soubory musejí být přístupné ke stažení i pro indexovací boty, aby to bylo kompletní SEO rozhodnutí.<br/><br/><b style="color:red">Prosím, čtěte :</b><br/><em>Ujistěte se, že máte nastavena požadovaná RewriteRule (# PHPBB FILES ALL MODES) ve vašem .htaccess, pokud chcete aktivovat tuto funkci.</em>',
	'rem_sid' => 'Odstranění SID (Session ID)',
	'rem_sid_explain' => 'SID bude tímto nastavením odstraněna ze 100% URL odkazů skrze phpbb_seo_class pro anonymní uživatele, a tedy i boty.<br/>Tak zajistíte, že boti neuvidí žádné SID v URL odkazech fór, témat a příspěvků, avšak návštěvníci, kteří nepřijmou cookies pravěpodobně vytvoří víc než jednu session.<br/>"Nulový duplikát" defaultně přesměruje URL odkazy se SID na http 301 pro anonymní uživatele a boty.',
	'rem_hilit' => 'Odstranění zvýraznění (Highlights)',
	'rem_hilit_explain' => 'Zvýraznění bude tímto nastavením odstraněno ze 100% URL odkazů skrze phpbb_seo_class pro anonymní uživatele, a tím pádem i boty.<br/>Tak zajistíte, že boti neuvidí žádné zvýraznění v URL odkazech fór, témat a příspěvků.<br/>"Nulový duplikát" se bude automaticky řídit tímto nastavením pro anonymní uživatele a boty, např. URL odkazů se zvýrazněními přesměruje na http 301.',
	'rem_small_words' => 'Odstranit krátká slova',
	'rem_small_words_explain' => 'Povolí odstranění všech slov, v přepisovaných URL odkazech, kratších než 3 znaky.<br/><b style="color:red">Prosím, čtěte</b><br/><ul style="margin-left:20px">Filtrování teoreticky změní mnoho URL odkazů na vašem fóru.<br/>Ačkoli by se měl "Nulový duplikát" postarat o požadované přesměrování, pečlivě zvažte aktivaci či změnu na již zaindexované stránce. <br/>Snažte, proto měnit toto nastavení minimálně.</ul>',
	'virtual_folder' => 'Virtuální Adresář',
	'virtual_folder_explain' => 'Povolí přidání URL odkazu fóra jako virtuálního adresáře do URL odkazů témat.<br/><u>Příklad :</u><ul style="margin-bottom:0px;margin-left:20px"><b>forum-title-fxx/topic-title-txx.html</b> místo <b>topic-title-txx.html</b><br/>pro URL odkaz tématu.</ul><b style="color:red">Prosím, čtěte</b><br/><ul style="margin-left:20px">Vkládání virtuálního adresáře změní mnoho URL odkazů na vašem fóru.<br/>Pokud již máte zaindexované fórum boty, měli byste tuto změnu či aktivaci zvážit, jelikož bude mít vliv na zpětnou vazbu s vyhledávači. <br/>Snažte, proto měnit toto nastavení minimálně.</ul>',
	'virtual_root' => 'Virtuální Root',
	'virtual_root_explain' => 'Pokud je phpBB3 nainstalován v podadresáři (např. phpBB3/), můžete tímto nasimulovat jeho umístění v rootu.<br/><u>Příklad :</u><ul style="margin-bottom:0px;margin-left:20px"><b>phpBB3/forum-title-fxx/topic-title-txx.html</b> oproti <b>forum-title-fxx/topic-title-txx.html</b><br/>URL odkazu tématu.</ul>Toto může být užitečné pro zkrácení URL odkazů, zvláště pokud používáte "Virtuální Adresář". Nepřepsané odkazy v phpBB3 adresáři budou nadále viditelné a funkční.<br/><b style="color:red">Prosím, čtěte :</b><br/><ul style="margin-left:20px">Používání tohoto nastavení vyžaduje "domovskou stránku" pro index fóra (např. forum.html).<br/> Tato volba změní mnoho URL odkazů na vašem fóru.<br/>Pokud již máte zaindexované fórum boty, měli byste tuto změnu či aktivaci zvážit, jelikož bude mít vliv na zpětnou vazbu s vyhledávači. <br/>Snažte, proto měnit toto nastavení minimálně.</ul>',
	'cache_layer' => '"Cachování" URL odkazů fóra',
	'cache_layer_explain' => 'Zapne cache pro URL odkazy fóra a umožní tím jejich oddělení z názvu URL odkazů<br/><u>Příklad :</u><ul style="margin-bottom:0px;margin-left:20px"><b>forum-title-fxx/</b> versus <b>any-title-fxx/</b><br/>URL odkazu tématu.</ul><b style="color:red">Prosím, čtěte</b><br/><ul style="margin-left:20px">Tato volba vám umožní změnit URL odkaz vašeho fóra, a tedy pravděpodobně i mnoho URL odkazů témat, pokud používáte "Virtuální Adresář".<br/>URL odkazy témat budou vždy správně přesměrovány s volbou "Nulový Duplikát".<br/>To bude platit i pro URL odkaz fóra, pokud ponecháte oddělovače a IDčka, vizte níže.</ul>',
	'rem_ids' => 'Odstranění ID fór',
	'rem_ids_explain' => 'Odstraní ID a oddělovače v URL odkazech fór. Použijte jen v případě, že máte aktivováno "Cachování URL odkazů fóra".<br/><u>Příklad :</u><ul style="margin-bottom:0px;margin-left:20px"><b>any-title-fxx/</b> versus <b>any-title/</b><br/>URL odkazu.</ul><b style="color:red">Prosím, čtěte</b><br/><ul style="margin-left:20px">Tato volba vám umožní změnit URL odkaz vašeho fóra, a tedy pravděpodobně i mnoho URL odkazů témat, pokud používáte "Virtuální Adresář".<br/>URL odkazy témat budou vždy správně přesměrovány s volbou "Nulový Duplikát".<br/><u>To nebude platit pro všechny případy URL odkazů fóra :</u><br/><ul style="margin-left:20px"><b>any-title-fxx/</b> budou vždy správně přesměrovány s volbou "Nulový Duplikát", ale nestane se tak v případě, že upravíte <b>any-title/</b> na <b>something-else/</b>.<br/> V takovémto případě, <b>any-title/</b> se nyní bude chovat jako neexistující fórum.<br/>Dobře se tedy rozhodněte, zda je tato volba pro vás nezbytná, ale z hlediska SEO optimalizace se jedná o dobrý krok.</ul></ul>',
	// copyrights
	'copyrights' => 'Copyrights',
	'copyrights_img' => 'Nalinkovat obrázek',
	'copyrights_img_explain' => 'Zde si můžete vybrat, jakým způsobem bude zobrazen phpBB3 SEO copyright link. Buď jako obrázkový nebo jak textový odkaz.',
	'copyrights_txt' => 'Alternativní text',
	'copyrights_txt_explain' => 'Zde si můžete zvolit text, který bude použit v phpBB3 SEO copyright linku jako alternativní text. Ponechte prázdné pro defaultní.',
	'copyrights_title' => 'Titulek linku',
	'copyrights_title_explain' => 'Zde si můžete zvolit text, který bude použit v phpBB3 SEO copyright linku jako titulek. Ponechte prázdné pro defaultní.',
	// Zero duplicate
	// Options
	'ACP_ZERO_DUPE_OFF' => 'Vypnout',
	'ACP_ZERO_DUPE_MSG' => 'Příspěvek',
	'ACP_ZERO_DUPE_GUEST' => 'Návstěvník',
	'ACP_ZERO_DUPE_ALL' => 'Vše',
	'zero_dupe' =>'Zero duplicate',
	'zero_dupe_explain' => 'Následující nastavení se týká Zero duplicate, můžete je měnit podle vašich potřeb.<br/>Nemusí nutně vést k aktualizaci .htaccess.',
	'zero_dupe_on' => 'Aktivovat Zero duplicate',
	'zero_dupe_on_explain' => 'Povolit aktivaci a deaktivaci přesměrování s pomocí Zero duplicate.',
	'zero_dupe_strict' => 'Přesný způsob',
	'zero_dupe_strict_explain' => 'Pokud je aktivováno, Zero duplicate zkontroluje zda požadovaný URL odkaz přesně souhlasí s navštíveným.<br/>Pokud je deaktivováno, Zero duplicate zkontroluje zda navštívený URL odkaz je první částí požadovaného URL odkazu.<br/> Je v zájmu, aby přesměrování fungovalo s módy, které by mohly rušit Zero duplicate přidáním GET proměnných.',
	'zero_dupe_post_redir' => 'Přesměrování příspěvků',
	'zero_dupe_post_redir_explain' => 'Tato volba určí, jak zacházet s URL odkazy příspěvků. Může nabývat těchto hodnot :<ul style="margin-left:20px"><li><b>&nbsp;vypnuté</b>, nepřesměrovávat URL odkazy příspěvků, v žádném případě,</li><li><b>&nbsp;příspěvek</b>, přesvědčit se zda postxx.html je použit jako URL odkaz příspěvku,</li><li><b>&nbsp;návštěvník</b>, přesměrovat návštěvníky, pokud je to vyžadováno, na URL odkaz odpovídajícího  tématu než na postxx.html a přesvědčit se zda postxx.html je použit pro přihlášené uživatele,<li><b>&nbsp;vše</b>, přesměrovat vše, pokud je to vyžadováno, na URL odkaz odpovídajícího tématu.</li></ul><br/><b style="color:red">Prosím, čtěte</b><br/><em>Ponechání <b>postxx.html</b> URL odkazů je bezpečné SEO rozhodnutí, ale jen do té doby, dokud ponecháte "disallow" na URL odkazech příspěvků ve vašem robots.txt.<br/>Přesměrování všech tšchto odkazů pravěpodobně způsobí přesměrování i&nbsp;všeho souvisejícího.<br/>Přesměrování postxx.html ve všech případech také způsobí, že zpráva, která by byla odeslána do vlákna a poté přesunuta do jiného, změnu jejího URL odkazu. Díky Zero duplicate je to bezpečné SEO rozhodnutí, avšak předchozí&nbsp;link na příspěvek nebude v takovém případě nikdy přesměrován k novému. Čili bude nefunkční.</em>',
	// no duplicate
	'no_dupe' => 'No duplicate',
	'no_dupe_on' => 'Aktivovat No duplicate',
	'no_dupe_on_explain' => 'Mód No duplicate nahrazuje URL odkazy příspěvků s odpovídajícími URL odkazy témat (s pagination / stránkováním).<br/>Nepřidává žádný SQL dotaz, jen provede navíc "LEFT JOIN". To může v některých případech znamenat větší zátěž serveru, ale ve většinou by to neměl být problém.',
));
?>