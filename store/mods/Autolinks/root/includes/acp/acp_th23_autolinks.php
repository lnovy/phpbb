<?php
/*
*
* includes/acp/acp_th23_autolinks.php
*
* @package th23_autolinks
* @author Thorsten Hartmann (www.th23.net)
* @copyright (c) 2008 by Thorsten Hartmann (www.th23.net)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * @package acp
 */
class acp_th23_autolinks
{
	var $u_action;
	var $new_config = array();
	
	function main($id, $mode)
	{
		global $cache, $config, $db, $user, $auth, $template;
		global $phpbb_root_path, $phpbb_admin_path, $phpEx, $table_prefix;

		$user->add_lang('acp/th23_autolinks');	

		$this->page_title = 'ACP_TH23_AUTOLINKS';
		$this->tpl_name = 'acp_th23_autolinks';

		$form_key = 'acp_th23_autolinks';
		add_form_key($form_key);

		$this->new_config = $config;

		$submit = (isset($_POST['submit'])) ? true : false;
		$cancel = (isset($_POST['cancel'])) ? true : false;

		switch ($mode)
		{
			case 'urls':

				// handle sorting
				$sort_ary = array(
					'sort' => false,
					'text' => false
				);
				$sort = request_var('sort', '');
				$sort = (isset($sort_ary[$sort])) ? $sort : 'sort';
				$template->assign_vars(array(
					'S_ACP_TH23_AUTOLINKS_SORT' => ($sort == 'sort') ? true : false,
					'U_ACP_TH23_AUTOLINKS_SORT_SORT' => $this->u_action . '&amp;sort=sort',
					'U_ACP_TH23_AUTOLINKS_SORT_WORD' => $this->u_action . '&amp;sort=text',
				));
				$this->u_action .= '&amp;sort=' . $sort;

				// output page header
				$template->assign_vars(array(
					'S_ACP_TH23_AUTOLINKS_URLS' => true,
					'L_ACP_TH23_AUTOLINKS_MODE' => $user->lang['ACP_TH23_AUTOLINKS_URLS'],
					'L_ACP_TH23_AUTOLINKS_EXPLAIN' => $user->lang['ACP_TH23_AUTOLINKS_URLS_EXPLAIN'],
					'U_ACTION' => $this->u_action,
				));

				// thats where we will collect errors
				$error = array();

				// what do we do?
				$action = (!$cancel) ? request_var('action', '') : '';

				// get action item data
				if ($action && $action != 'new')
				{
					// check for valid item id
					$item_id = request_var('id', 0);
					$sql = 'SELECT * 
						FROM ' . TH23_AUTOLINKS_TABLE . ' 
						WHERE id = ' . $item_id;
					$result = $db->sql_query($sql);
					if (!($item_data = $db->sql_fetchrow($result)))
					{
						$error[] = $user->lang['ACP_TH23_AUTOLINKS_NO_VALID_ITEM'];
					}
					$db->sql_freeresult($result);
				}

				if ($submit)
				{
					switch ($action)
					{
						case 'edit':
						case 'new':
							// retrieve item data (word and url)
							$item_ary = array(
								'text' => utf8_normalize_nfc(request_var('word', '', true)),
								'url' => request_var('url', ''),
							);

							// check for word
							if ($item_ary['text'] == '')
							{
								$error[] = $user->lang['ACP_TH23_AUTOLINKS_NO_WORD'];
							}

							// check for not allowed characters
							if ($item_ary['text'] == ' ')
							{
								$error[] = $user->lang['ACP_TH23_AUTOLINKS_NOT_ALLOWED_CHARACTER'];
							}

							// check for not allowed phrase "th23autolink"
							if (strpos($item_ary['text'], 'th23autolink') !== false || strpos($item_ary['url'], 'th23autolink') !== false)
							{
								$error[] = $user->lang['ACP_TH23_AUTOLINKS_NOT_ALLOWED_TH23AUTOLINK'];
							}

							if (!sizeof($error))
							{
								// handle local links
								if (!(strpos($item_ary['url'], 'http://') === 0))
								{
									$item_ary['url'] = (strpos($item_ary['url'], '?') !== false) ?  'th23autolink_root' . $item_ary['url'] . 'th23autolink_sid_and' : 'th23autolink_root' . $item_ary['url'] . 'th23autolink_sid_que';
								}

								if ($action == 'new')
								{
									// get max items for sort field entry
									$sql = 'SELECT COUNT(id) AS num_items 
										FROM ' . TH23_AUTOLINKS_TABLE;
									$result = $db->sql_query($sql);
									$num_items = (int) $db->sql_fetchfield('num_items');
									$db->sql_freeresult($result);
									$item_ary['sort'] = $num_items + 1;
									// set sql for new item
									$sql = 'INSERT INTO ' . TH23_AUTOLINKS_TABLE . ' ' . $db->sql_build_array('INSERT', $item_ary);
								}
								else
								{
									// set sql for edit item
									$sql = 'UPDATE ' . TH23_AUTOLINKS_TABLE . ' SET ' . $db->sql_build_array('UPDATE', $item_ary) . ' WHERE id=' . $item_data['id'];
								}
								// do the trick (add / edit)
								if ($db->sql_query($sql))
								{
									$cache->destroy('_th23_autolinks');
									trigger_error($user->lang['ACP_TH23_AUTOLINKS_SAVE_SUCCESS'] . adm_back_link($this->u_action . '&amp;page=' . request_var('page', 1)));
								}
								else
								{
									$error[] = $user->lang['ACP_TH23_AUTOLINKS_SAVE_NO_SUCCESS'];
								}
							}
							break;

						case 'delete':
							if (!sizeof($error))
							{
								// do the trick (add / edit)
								$sql = 'DELETE FROM ' . TH23_AUTOLINKS_TABLE . ' WHERE id = ' . $item_data['id'];
								if ($db->sql_query($sql))
								{
									$sql = 'UPDATE ' . TH23_AUTOLINKS_TABLE . ' SET sort = sort - 1 WHERE sort > ' . $item_data['sort'];
									$result = $db->sql_query($sql);
									$cache->destroy('_th23_autolinks');
									trigger_error($user->lang['ACP_TH23_AUTOLINKS_DELETE_SUCCESS'] . adm_back_link($this->u_action . '&amp;page=' . request_var('page', 1)));
								}
								else
								{
									$error[] = $user->lang['ACP_TH23_AUTOLINKS_DELETE_NO_SUCCESS'];
								}
							}
							break;
					}
					
					// no action if everything went well
					if (!sizeof($error))
					{
						$action = '';
					}
				}

				// get number of items in db
				$sql = 'SELECT COUNT(id) AS num_items 
					FROM ' . TH23_AUTOLINKS_TABLE;
				$result = $db->sql_query($sql);
				$num_items = (int) $db->sql_fetchfield('num_items');
				$db->sql_freeresult($result);

				// do we wanna move something?
				switch ($action)
				{
					case 'move':
					case 'move_up':
					case 'move_down':
						// check if operation is possible
						if ($action == 'move')
						{
							$new_item_sort = request_var('pos', 0);
						}
						else
						{
							$new_item_sort = ($action == 'move_up') ? $item_data['sort'] - 1 : $item_data['sort'] + 1;
						}

						if ($new_item_sort < 1 || $new_item_sort > $num_items)
						{
							$error[] = $user->lang['ACP_TH23_AUTOLINKS_NO_VALID_MOVE'];
						}
						// do the move ;-)
						if (!sizeof($error) && $new_item_sort != $item_data['sort'])
						{
							$sql = 'UPDATE ' . TH23_AUTOLINKS_TABLE . ' SET sort = 0 WHERE id = ' . $item_data['id'];
							$result = $db->sql_query($sql);

							if ($new_item_sort < $item_data['sort'])
							{
								$sql = 'UPDATE ' . TH23_AUTOLINKS_TABLE . ' SET sort = sort + 1 WHERE sort < ' . $item_data['sort'] . ' AND sort >= ' . $new_item_sort;
							}
							else
							{
								$sql = 'UPDATE ' . TH23_AUTOLINKS_TABLE . ' SET sort = sort - 1 WHERE sort > ' . $item_data['sort'] . ' AND sort <= ' . $new_item_sort;
							}
							$result = $db->sql_query($sql);
							
							$sql = 'UPDATE ' . TH23_AUTOLINKS_TABLE . ' SET sort = ' . $new_item_sort . ' WHERE id = ' . $item_data['id'];
							$result = $db->sql_query($sql);
							
							$db->sql_freeresult($result);
							$cache->destroy('_th23_autolinks');
		
							$action = '';
						}
				}

				// if error occured, print it out
				if (sizeof($error))
				{
					$template->assign_vars(array(
						'S_ERROR' => (sizeof($error)) ? true : false,
						'ERROR_MSG' => implode('<br />', $error),
					));
				}

				// set max items per page
				$items_page = 25;
				
				// get max pages
				$num_pages = ceil($num_items / $items_page);

				// get act page - and adjust when invalid
				$act_page = request_var('page', 1);
				if ($act_page < 1)
				{
					$act_page = 1;
				}
				elseif ($act_page > $num_pages)
				{
					$act_page = $num_pages;
				}

				// create pagination
				if ($num_items > $items_page)
				{
					$pagination = '';
					if ($num_pages > 5)
					{
						$start_cnt = min(max(1, $act_page - 4), $num_pages - 5);
						$end_cnt = max(min($num_pages, $act_page + 4), 6);
						$pagination .= ($act_page == 1) ? '<strong>1</strong>' : '<a href="' . $this->u_action . '&amp;page=1">1</a>';
						$pagination .= ($start_cnt > 1) ? '...' : '';
						for ($i = $start_cnt + 1; $i < $end_cnt; $i++)
						{
							$pagination .= ($i == $act_page) ? '<strong>' . $i . '</strong>' : '<a href="' . $this->u_action . '&amp;page=' . $i . '">' . $i . '</a>';
						}
						$pagination .= ($end_cnt < $num_pages) ? '...' : '';					
						$pagination .= ($act_page == $num_pages) ? '<strong>'.$act_page.'</strong>' : '<a href="' . $this->u_action . '&amp;page='.$num_pages.'">'.$num_pages.'</a>';
					}
					else
					{
						for ($i = 1; $i <= $num_pages; $i++)
						{
							$pagination .= ($i == $act_page) ? '<strong>' . $i . '</strong>' : '<a href="' . $this->u_action . '&amp;page=' . $i . '">' . $i . '</a>';
						}
					}
					$template->assign_vars(array(
						'S_PAGINATION' => $pagination,
					));
				}

				// prepare new / edit box
				$template->assign_vars(array(
					'S_ACP_TH23_AUTOLINKS_EDIT' => false,
					'L_ACP_TH23_AUTOLINKS_NEW_EDIT_DELETE' => $user->lang['ACP_TH23_AUTOLINKS_NEW'],
					'L_ACP_TH23_AUTOLINKS_NEW_EDIT_DELETE_EXPLAIN' => $user->lang['ACP_TH23_AUTOLINKS_NEW_EXPLAIN'],
					'U_ACP_TH23_AUTOLINKS_NEW_EDIT_DELETE' => $this->u_action . '&amp;page=' . $act_page . '&amp;action=new',
					'S_ACP_TH23_AUTOLINKS_WORD' => (isset($error) && sizeof($error)) ? str_replace(array('&lt;', '&gt;'), array('<', '>'), $item_ary['text']) : '',
					'S_ACP_TH23_AUTOLINKS_URL' => (isset($error) && sizeof($error)) ? str_replace(array('&lt;', '&gt;'), array('<', '>'), $item_ary['url']) : '',
					'L_ACP_TH23_AUTOLINKS_NEW_EDIT_DELETE_BTN' => $user->lang['ADD'],
				));
				if ($action == 'edit' || $action == 'delete')
				{
					$template->assign_vars(array(
						'S_ACP_TH23_AUTOLINKS_EDIT' => ($action == 'edit') ? true : false,
						'S_ACP_TH23_AUTOLINKS_DELETE' => ($action == 'delete') ? true : false,
						'L_ACP_TH23_AUTOLINKS_NEW_EDIT_DELETE' => ($action == 'edit') ? $user->lang['ACP_TH23_AUTOLINKS_EDIT'] : $user->lang['ACP_TH23_AUTOLINKS_DELETE'],
						'L_ACP_TH23_AUTOLINKS_NEW_EDIT_DELETE_EXPLAIN' => ($action == 'edit') ? $user->lang['ACP_TH23_AUTOLINKS_EDIT_EXPLAIN'] : $user->lang['ACP_TH23_AUTOLINKS_DELETE_EXPLAIN'],
						'U_ACP_TH23_AUTOLINKS_NEW_EDIT_DELETE' => $this->u_action . '&amp;t=' . time() . '&amp;page=' . $act_page . '&amp;action=' . $action . '&amp;id=' . $item_id,
						'S_ACP_TH23_AUTOLINKS_WORD' => (sizeof($error)) ? str_replace(array('&lt;', '&gt;'), array('<', '>'), $item_ary['text']) : str_replace(array('&lt;', '&gt;', 'th23autolink_root', 'th23autolink_sid_and', 'th23autolink_sid_que'), array('<', '>', '', '', ''), $item_data['text']),
						'S_ACP_TH23_AUTOLINKS_URL' => (sizeof($error)) ? str_replace(array('&lt;', '&gt;'), array('<', '>'), $item_ary['url']) : str_replace(array('&lt;', '&gt;', 'th23autolink_root', 'th23autolink_sid_and', 'th23autolink_sid_que'), array('<', '>', '', '', ''), $item_data['url']),
						'L_ACP_TH23_AUTOLINKS_NEW_EDIT_DELETE_BTN' => ($action == 'edit') ? $user->lang['EDIT'] : $user->lang['DELETE'],
					));
				}

				// get list of links to display
				$sql = 'SELECT * 
					FROM ' . TH23_AUTOLINKS_TABLE . ' 
					ORDER BY ' . $sort . ' ASC';
				$result = $db->sql_query_limit($sql, $items_page, (($act_page - 1) * $items_page));
				while($row = $db->sql_fetchrow($result))
				{
					$template->assign_block_vars('items', array(
						'SORT' => $row['sort'],
						'WORD' => $row['text'],
						'U_URL' => str_replace(array('th23autolink_root', 'th23autolink_sid_and', 'th23autolink_sid_que'), array($phpbb_root_path, '&amp;sid=' . $user->session_id, '?sid=' . $user->session_id), $row['url']),
						'URL' => str_replace(array('th23autolink_root', 'th23autolink_sid_and', 'th23autolink_sid_que'), '', $row['url']),
						'U_EDIT' => $this->u_action . '&amp;t=' . time() . '&amp;page=' . $act_page . '&amp;action=edit&amp;id=' . $row['id'] . '#action',
						'U_DELETE' => $this->u_action . '&amp;t=' . time() . '&amp;page=' . $act_page . '&amp;action=delete&amp;id=' . $row['id'] . '#action',
						'U_MOVE_UP' => ($row['sort'] > 1) ? $this->u_action . '&amp;page=' . $act_page . '&amp;action=move_up&amp;id=' . $row['id'] : false,
						'U_MOVE_DOWN' => ($row['sort'] < $num_items) ? $this->u_action . '&amp;page=' . $act_page . '&amp;action=move_down&amp;id=' . $row['id'] : false,
					));
				}
				$db->sql_freeresult($result);

				break;

			case 'settings':
			default:

				$th23_autolinks_settings = array(
					'title'	=> 'ACP_TH23_AUTOLINKS_SETTINGS',
					'vars'	=> array(
						'legend1' => 'ACP_TH23_AUTOLINKS_STATUS',
						'th23_autolinks' => array('lang' => 'ACP_TH23_AUTOLINKS_ENABLE', 'validate' => 'bool', 'type' => 'radio:enabled_disabled', 'explain' => true),
						'legend2' => 'ACP_TH23_AUTOLINKS_ACTIONS',
						'th23_autolinks_posts' => array('lang' => 'ACP_TH23_AUTOLINKS_POSTS', 'validate' => 'bool', 'type' => 'radio:yes_no', 'explain' => true),
						'th23_autolinks_forum' => array('lang' => 'ACP_TH23_AUTOLINKS_FORUM', 'validate' => 'int', 'type' => 'custom', 'method' => 'acp_th23_autolinks_forum', 'explain' => true),
						'th23_autolinks_pms' => array('lang' => 'ACP_TH23_AUTOLINKS_PMS', 'validate' => 'bool', 'type' => 'radio:yes_no', 'explain' => true),
						'th23_autolinks_preview' => array('lang' => 'ACP_TH23_AUTOLINKS_PREVIEW', 'validate' => 'bool', 'type' => 'radio:yes_no', 'explain' => true),
						'th23_autolinks_code' => array('lang' => 'ACP_TH23_AUTOLINKS_CODE', 'validate' => 'bool', 'type' => 'radio:yes_no', 'explain' => true),
						'legend3' => 'ACP_TH23_AUTOLINKS_USERNAMES',
						'th23_autolinks_user' => array('lang' => 'ACP_TH23_AUTOLINKS_USER', 'validate' => 'bool', 'type' => 'radio:yes_no', 'explain' => true),
						'th23_autolinks_user_expire' => array('lang' => 'ACP_TH23_AUTOLINKS_USER_EXPIRE', 'validate' => 'int', 'type' => 'select', 'method' => 'user_expire_select', 'explain' => true),
						'legend4' => 'ACP_TH23_AUTOLINKS_APPEARANCE',
						'th23_autolinks_title' => array('lang' => 'ACP_TH23_AUTOLINKS_TITLE', 'validate' => 'string', 'type' => 'text:50:255', 'explain' => true),
						'th23_autolinks_css' => array('lang' => 'ACP_TH23_AUTOLINKS_CSS', 'validate' => 'string', 'type' => 'text:50:255', 'explain' => true),
					)
				);
				
				if ($submit)
				{
					$cfg_array = (isset($_REQUEST['config'])) ? utf8_normalize_nfc(request_var('config', array('' => ''), true)) : $this->new_config;
					$error = array();

					validate_config_vars($th23_autolinks_settings['vars'], $cfg_array, $error);

					if (!check_form_key($form_key))
					{
						$error[] = $user->lang['FORM_INVALID'];
					}

					if (sizeof($error) == 0)
					{
						// handle th23 autolinks forum ids special
						$th23_autolinks_forum_ids_value = '-';
						$th23_autolinks_forum_ids_request = request_var('th23_autolinks_forum_ids', array(0));
						foreach ($th23_autolinks_forum_ids_request as $th23_autolinks_forum_id)
						{
							$th23_autolinks_forum_ids_value .= $th23_autolinks_forum_id . '-';
						}
						set_config('th23_autolinks_forum_ids', $th23_autolinks_forum_ids_value);

						// handle "standard" config values
						foreach ($th23_autolinks_settings['vars'] as $config_name => $null)
						{
							if (!isset($cfg_array[$config_name]) || strpos($config_name, 'legend') !== false)
							{
								continue;
							}
							$this->new_config[$config_name] = $cfg_array[$config_name];
							set_config($config_name, $cfg_array[$config_name]);
						}

						// add log entry
						add_log('admin', 'ACP_TH23_AUTOLINKS_LOG_SETTINGS');
						$cache->destroy('_th23_autolinks');
						trigger_error($user->lang['CONFIG_UPDATED'] . adm_back_link($this->u_action));
					}
					else
					{
						$template->assign_vars(array(
							'S_ERROR' => (sizeof($error)) ? true : false,
							'ERROR_MSG' => implode('<br />', $error),
						));
					}
				}

				$template->assign_vars(array(
					'S_ACP_TH23_AUTOLINKS_SETTINGS' => true,
					'L_ACP_TH23_AUTOLINKS_MODE' => $user->lang['ACP_TH23_AUTOLINKS_SETTINGS'],
					'L_ACP_TH23_AUTOLINKS_EXPLAIN' => $user->lang['ACP_TH23_AUTOLINKS_SETTINGS_EXPLAIN'],
					'U_ACTION' => $this->u_action,
				));

				// Output relevant page
				foreach ($th23_autolinks_settings['vars'] as $config_key => $vars)
				{
					if (!is_array($vars) && strpos($config_key, 'legend') === false)
					{
						continue;
					}

					if (strpos($config_key, 'legend') !== false)
					{
						$template->assign_block_vars('options', array(
							'S_LEGEND'		=> true,
							'LEGEND'		=> (isset($user->lang[$vars])) ? $user->lang[$vars] : $vars)
						);

						continue;
					}

					$type = explode(':', $vars['type']);

					$l_explain = '';
					if ($vars['explain'] && isset($vars['lang_explain']))
					{
						$l_explain = (isset($user->lang[$vars['lang_explain']])) ? $user->lang[$vars['lang_explain']] : $vars['lang_explain'];
					}
					else if ($vars['explain'])
					{
						$l_explain = (isset($user->lang[$vars['lang'] . '_EXPLAIN'])) ? $user->lang[$vars['lang'] . '_EXPLAIN'] : '';
					}

					$template->assign_block_vars('options', array(
						'KEY'			=> $config_key,
						'TITLE'			=> (isset($user->lang[$vars['lang']])) ? $user->lang[$vars['lang']] : $vars['lang'],
						'S_EXPLAIN'		=> $vars['explain'],
						'TITLE_EXPLAIN'	=> $l_explain,
						'CONTENT'		=> build_cfg_template($type, $config_key, $this->new_config, $config_key, $vars),
						)
					);
				
					unset($th23_autolinks_settings['vars'][$config_key]);
				}
				
				break;
		}

	}

	function acp_th23_autolinks_forum($value, $key)
	{
		global $db, $config;

		// prepare radio buttons
		$radio_ary = array(
			1 => 'ACP_TH23_AUTOLINKS_FORUM_ALL',
			2 => 'ACP_TH23_AUTOLINKS_FORUM_ALLOW',
			3 => 'ACP_TH23_AUTOLINKS_FORUM_DENY',
		);
		$html_radio = h_radio('config[th23_autolinks_forum]', $radio_ary, $value, $key);

		// get forum structure
		$sql = 'SELECT forum_id, forum_name, parent_id, forum_type, left_id, right_id
			FROM ' . FORUMS_TABLE . '
			ORDER BY left_id ASC';
		$result = $db->sql_query($sql, 600);

		// set initial vars to capture structure
		$right = $padding = 0;
		$padding_store = array('0' => 0);
		$html_select = '<select name="th23_autolinks_forum_ids[]" id="th23_autolinks_forum_ids" size="8" multiple="multiple">';

		// get the select options
		while ($row = $db->sql_fetchrow($result))
		{
			if ($row['left_id'] < $right)
			{
				$padding++;
				$padding_store[$row['parent_id']] = $padding;
			}
			else if ($row['left_id'] > $right + 1)
			{
				$padding = (isset($padding_store[$row['parent_id']])) ? $padding_store[$row['parent_id']] : $padding;
			}

			$right = $row['right_id'];
			
			// we don't show links here
			if ($row['forum_type'] != FORUM_LINK)
			{			
				// is this one selected
				$selected = (strpos($config['th23_autolinks_forum_ids'], '-' . $row['forum_id'] . '-') !== false) ? ' selected="selected"' : ''; 
				$html_select .= '<option' . $selected . ' value="' . $row['forum_id'] . '">' . str_repeat('-', $padding) . ' ' . $row['forum_name'] . '</option>';
			}

		}
		$db->sql_freeresult($result);
		unset($padding_store);
		$html_select .= '</select>';	

		return $html_radio . '<br /><br />' . $html_select;
	}

	function user_expire_select($value, $key)
	{
		global $user;

		$value_ary = array(
			'60' => $user->lang['ACP_TH23_AUTOLINKS_USER_EXPIRE_MINUTE'],
			'3600' => $user->lang['ACP_TH23_AUTOLINKS_USER_EXPIRE_HOUR'],
			'86400' => $user->lang['ACP_TH23_AUTOLINKS_USER_EXPIRE_DAY'],
			'604800' => $user->lang['ACP_TH23_AUTOLINKS_USER_EXPIRE_WEEK'],
		);

		$select_html = '';
		foreach ($value_ary as $val => $lang)
		{
			$select_html .= '<option value="' . $val . '"' . (($value == $val) ? ' selected="selected"' : '') . '>' . $lang . '</option>';
		}

		return $select_html;
	}

}

?>