<?php
function add($item, $value) {
	if (!$value)
		return;
	switch ($item) {
		case 'medium':
		case 'title':
		case 'description':
		case 'audio_type':
		case 'audio_title':
		case 'audio_artist':
		case 'audio_album':
		case 'video_height':
		case 'video_width':
		case 'video_type':
			print '<meta name="' . htmlspecialchars($item) . '" content="' . htmlspecialchars($value) . '" />';
			break;
		case 'image_src':
		case 'video_src':
		case 'audio_src':
			print '<link rel="' . htmlspecialchars($item) . '" href="' . htmlspecialchars($value) . '" />';
			break;
	}
}
if (array_key_exists('q', $_GET) and ($id = $_GET['q'])) {
chdir("/vhosts2/cz/stranapiratska/www/socialbar");
	$fn = "data/" . ($id = $_GET['q']);
	if (file_exists($fn)) {
		$data = unserialize(file_get_contents($fn));
		$user_browser = strtolower($_SERVER['HTTP_USER_AGENT']);
		$browsers_array = array('240x320', '320x240','blackberry', 'iemobile', 'minimobile', 'mobile', 'opera mini', 'pda', 'phone', 'pocket', 'psp', 'symbian', 't-shark', 'wireless');
		if (strpos($user_browser, 'facebook') !== false) 
			$browser_array = array();

		foreach ($browsers_array as $ua_match) {
			if (strpos($user_browser, $ua_match) !== false) {
				Header('Location: '. $data['url']);
				exit;
			}
		}
		print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" . PHP_EOL;
		print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"
		\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" . PHP_EOL;
		print "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:fb=\"http://apps.facebook.com/ns/1.0\" xml:lang=\"cs\">";
		print "<head>";
		print "<meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />";
		foreach ($data as $item => $value)
			add($item, $value);
		print "<title>" . htmlspecialchars($data['title']) . " (ČPS Socialbar)</title>";
?>
	<style type="text/css">
		<!--
			body {
				font-family: "lucida grande" ,tahoma,verdana,arial,sans-serif;
				font-size: 11px;
				line-height: 11px;
				color: #3b5998;
			}
			dl {
				display: block;
				margin: 0px;
				padding: 0px;
				width: 300px;
			}
			dt {
				display: inline-block;
				text-align: right;
				width: 75px;
				font-size: 11px;
				color: white;
				font-weight: bold;
				margin: 0px;
				padding: 3px;
				vertical-align: top;
			}
			dd {
				display: inline-block;
				font-size: 11px;
				color: white;
				margin: 0px;
				padding: 3px;
				width: 150px;
			}
			#title {
				font-weight: bold;
				padding: 3px;
				display: block;
				font-size: 15px;
				line-height: 16px;
			}
			#timestamp {
				font-variant: italic;
				padding: 3px;
				clear: both;
				font-size: 9px;
				color: #999;
				display: block;
			}
			#description {
				display: none;
				width: 200px;
				line-height: 17px;
				margin-left: 20px;
				color: #ddd;
				height: 17px;
				overflow: hidden;
			}
			#toolbar {
				z-index: 42;
			}
			#closediv {
				display: inline-block; width: 38px; height: 38px;
				background: transparent;
				position: absolute;
				right: 0px;
				top: 0px;
				border: 1px solid transparent;
				height: 40px;
				z-index: 43;
				background: url(close.png) no-repeat -500px 0px;
			}

			#toolbar:hover #closediv{
				background-position: center center;
			}

			#closediv:hover {
//				border: 1px solid #999;
			}
			#closediv a {
				font-size: 1px;
				overflow:hidden;
				color: transparent;
				width: 38px;
				height: 38px;
				line-height: 38px;
			}
			#commentbox {
				display: block;
				height: 0px;
				padding: 0px;
				overflow-y:hidden;
				position: fixed;
				width: 600px;
				top: 50px;
				left: 50%;
				margin-left: -300px;
				background: #eee;
				z-index: 600;
				border: 0px;
				opacity: 1;
			}
			#commentwrap {
				position: fixed;
				height: 0px;
			}
			#toolbar.fbopened:hover #commentwrap {
				position: fixed;
				width: 700px;
				height: 100%;
				left: 50%;
				margin-left: -350px;
				padding:25px;
				background: transparent;
				z-index: 599;
			}
			#toolbar.fbopened:hover  #commentbox {
				display: block;
				overflow-y: auto;
				height: 60%;
				padding: 25px;
				border: 1px solid #ddd;
			}
		-->
	</style>
	<script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php/cs_CZ" type="text/javascript"></script>
	<script type="text/javascript">
		function fbinit() {
			document.getElementById('toolbar').className = 'fbopened';
			FB.init("32c3fa6625699d3e64bcf7c1029814d6");
		}
	</script>
<?php
		print "</head>";
		print "<body style=\"padding: 0px; margin: 0px; overflow: hidden;\">";
?>
	<script type="text/javascript">
		<!--
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		-->
	</script>
	<script type="text/javascript">
		<!--
		try {
		var pageTracker = _gat._getTracker("UA-9975819-3");
		pageTracker._trackPageview();
		} catch(err) {}
		-->
	</script>
<?php
		print "<div style=\"position: fixed; background: no-repeat #eceff5 right center; height: 40px; width: 100%; border-bottom: #c3cddf solid 1px; margin: 0px; top: 0px; left: 0px; z-index: 999; vertical-align: top;\" id=\"toolbar\" onmouseover=\"fbinit();\">";
?>
	<div style="display: inline-block; width: 50px; height: 40px; background: url(fb.png) no-repeat center center;" id="logo">
	</div>
	<div id="commentwrap">
		<div id="commentbox">
			<fb:comments url="http://fb.ceskapiratskastrana.cz/<?php print $id; ?>" publish_feed="false" reverse="false" quiet="true" xid="cps-sharer-<?php print $id; ?>">
			</fb:comments>
		</div>
	</div>
	<div style="display: inline-block; vertical-align: top; ">
			<span id="title"><?php print htmlspecialchars($data['title']); ?></span>
			<span id="timestamp">Přidáno <?php print date ("d.m.Y H:i:s", filemtime($fn)); ?></span>
	</div>
	<div style="display: inline-block;">
			<span id="description"><?php print htmlspecialchars($data['description']); ?></span>
	</div>
	<div style="display: inline-block; position: absolute; top: 10px; right: 150px;">
		<a name="fb_share" type="button_count" href="http://www.facebook.com/sharer.php">Sdílet</a><script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
	</div>
	<div id="closediv">
		<a href="<?php print htmlspecialchars($data['url']); ?>">Zavřít</a>
	</div>
<?php
		print "</div>";
		print "<div style=\"position: absolute; width: 100%; padding-top: 40px; top: 0px; height: 100%; overflow: hidden;\">";
		print "<iframe id=\"ifr\" src=\"" . htmlspecialchars($data['url']) . "\" style=\"margin: 0px; width: 100%; height: 100%; border: 0px; position: relative; top: 0px;\">";
		print "</iframe></div>";

?>
<script src='http://toolbar.wibiya.com/toolbarLoader.php?toolbarId=14126' type='text/javascript'></script>
<?php
		print "</body>";
		print "</html>";
		exit;
	} else {
		print "<b>Invalid link sry :(</b>";
    exit;
	}
}
if ($_SERVER['SERVER_NAME'] != 'www.ceskapiratskastrana.cz') {
	Header('Location: https://www.ceskapiratskastrana.cz/forum/socialbar.php');
 exit;
}
  chdir("/vhosts2/cz/stranapiratska/www/forum");
	define('IN_PHPBB', true);
	$phpbb_root_path = './';
	$phpEx = substr(strrchr(__FILE__, '.'), 1);
	include($phpbb_root_path . 'common.' . $phpEx);

	// Start session management
	$user->session_begin();
	$auth->acl($user->data);
		if ($user->data['user_id'] == ANONYMOUS)
		{
			login_box('', $user->lang['LOGIN_EXPLAIN_SOCIALBAR']);
		}

		if (!$user->data['is_registered'])
		{
			// TODO: Add better lang string here
			trigger_error('NOT_AUTHORISED');
		}

if (array_key_exists('doShare', $_POST)) {
	if (empty($_SERVER['HTTPS'])) {
	    die('HTTPS required');
	}
chdir("/vhosts2/cz/stranapiratska/www/socialbar");


	do {
		$fn = "data/" . ($id = base_convert("" . rand(), 10, 36));
	} while (file_exists($fn));
	file_put_contents($fn, serialize($_POST));
//	$isgd = trim(file_get_contents("http://u.nu/unu-api-simple?url=" . urlencode('http://fb.ceskapiratskastrana.cz/' . $id)));
	$isgd = file_get_contents("http://is.gd/api.php?longurl=" . urlencode('http://fb.ceskapiratskastrana.cz/' . $id));
//	Header('Location: http://www.facebook.com/share.php?u=' . urlencode('http://fb.ceskapiratskastrana.cz/' . $id));
	Header('Location: http://www.facebook.com/share.php?u=' . urlencode($isgd));
	exit;
}
	$user->setup();
  page_header('Socialbar');
  $template->set_filenames(array('body' => 'socialbar_body.html'));
  page_footer();
?>
