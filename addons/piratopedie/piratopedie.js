(function($) {
	$(function() {
		$('a.postlink').each(enclass = function() {
			if ($(this).parents().hasClass('rules'))
				return;
			if (this.href.match(/^https?:\/\/www\.ceskapiratskastrana\.cz\/wiki\/[a-zA-Z0-9\:\-_#]+$/) && this.innerHTML.match(/^https?:\/\/www\.ceskapiratskastrana\.cz\/wiki/)) {
				this.hrefsave = this.href;
				$(this).addClass('piratopedie');
			}
		});
		$('a.piratopedie').each(enwiki = function() {
			if(!this.hrefsave)
				this.hrefsave = this.innerHTML;
			wikinode = this.hrefsave;
			anchor = wikinode.match(/#(.+)$/);
			if (x = wikinode.match(/^(.+)#/)) {
				wikinode = x[1];
			}
			if (anchor) {
				anchor = anchor[1];
			} else {
				anchor = '';
			}
			$.ajax({
				type: "GET",
				url: "https://www.ceskapiratskastrana.cz/forum/addons/piratopedie/piratopedie.php?wikinode=" + wikinode + "&chapter=" + anchor,
				dataType: "text",
				context: this,
				success: function(data, textStatus, r) {
					c = $(this.context);
					c.html(data);
					this.context.title = 'Odkaz do Pirátopedie';
				},
				error: function(r, msg) {
					c = this.context;
					$(this.context).html($(this.context).html() + ' <i>[error: ' + r.statusText + ']</i>');
				}
			});
		});
		$(document).bind('DOMNodeInserted', function(a) {
			if (a.target.id == 'page-body') {
				$(a.target).find('#page-body a.postlink').each(enclass);
				$(a.target).find('a.piratopedie').each(enwiki);
			}
		});
	});
})(jQuery);

