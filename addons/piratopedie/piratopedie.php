<?php
 error_reporting(E_ALL);
 ini_set("display_errors", 1); 
$wikinode = $_GET['wikinode'];

if (!preg_match('/^https?:\/\/www\.ceskapiratskastrana\.cz\/wiki\/[a-zA-Z0-9:\-_]+$/', $wikinode)) {
	Header('HTTP/1.0 400 Invalid wiki node format');
	exit;
}
$wikinode = str_replace('http://www.ceskapiratskastrana.cz/wiki/','', $wikinode);
$wikinode = str_replace('https://www.ceskapiratskastrana.cz/wiki/','', $wikinode);
$wikinode = preg_replace('/:$/', ':start', $wikinode);

$path = '/var/www/cz/stranapiratska/www/wiki/data/meta/' . strtr($wikinode, ':', '/') . ".meta";
if (($meta = @file_get_contents($path)) === FALSE) {
	Header('HTTP/1.0 404 Wiki node not found or has no metadata');
	exit;
}
$meta = @unserialize($meta);
if (!array_key_exists('current', $meta)) {
	Header('HTTP/1.0 400 Invalid metadata (missing current)');
	exit;
}
if (!array_key_exists('title', $meta['current']) or !$meta['current']['title']) {
	Header('HTTP/1.0 400 Invalid metadata (missing title)');
	exit;
}
$title = $meta['current']['title'];
$anchor = @$_GET['chapter'];
if ($anchor) {
	if (array_key_exists('description', $meta['current']) && array_key_exists('tableofcontents', $meta['current']['description'])) {
		foreach ($meta['current']['description']['tableofcontents'] as $chapter) {
			if ($chapter['hid'] == $anchor) {
//				$title = $chapter['title'] . " (" . $title. ")";
				$title = $title . " – " . $chapter['title'];
			}
		}
	}
}

print $title;

?>
