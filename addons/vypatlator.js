//var net = require("net"),
  //  repl = require("repl");

function vypatlej(input, nahradit, Ud, smajliky, kecy) {
    var ratio = 1;
    var output = new String;
    var slint = new Array(5);
    var smajl = new Array(5);
    replace = new Object;
    replace.str = function (i) {
        return this.data[i].str;
    }
    replace.rpl = function (i) {
        return this.data[i].rpl;
    }
    replace.rnd = function (i) {
        return this.data[i].rnd;
    }
    replace.noregex = function (i) {
        return this.data[i].noregex;
    }
    replace.data = new Array;
    replace.add = function (string, replace, rand, noregex) {
        var ln = this.data.length;
        this.data[ln] = new Object;
        this.data[ln].str = string;
        this.data[ln].rpl = replace;
        this.data[ln].rnd = rand;
        this.data[ln].noregex = noregex;
        return 0;
    }
    replace.index_str = function (str) {
        for (var i in this.data) {
            if (this.data[i].str.toLowerCase() == str.toLowerCase()) return i;
        }
        return;
    }
    replace.rpl.index = function (rpl) {
        for (var i in this.data) {
            if (this.data[i].rpl == rpl) return i;
        }
        return;
    }
    replace.add("v", "w", 1);
    replace.add("!", "!!!", 1);
    replace.add("ú", "uuu", 1);
    replace.add("kw", "q", 1);
    replace.add("ů", "uuu", 1);
    replace.add("č", "cz", 1);
    replace.add("j", "y", 1);
    replace.add("š", "sh", 1);
    replace.add("ž", "zh", 1);
    replace.add("á", "aaa", 1);
    replace.add("é", "eee", 1);
    replace.add("eyt", "8", 1);
    replace.add("ř", "rz", 1);
    replace.add("í", "iii", 1);
    replace.add("ý", "yyy", 1);
    replace.add("ó", "ooo", 1);
    replace.add("ě", "e", 1);
    replace.add("ť", "t", 1);
    replace.add("ň", "n", 1);
    replace.add("ä", "a", 1);
    replace.add("ë", "e", 1);
    replace.add("ö", "o", 1);
    replace.add("ü", "u", 1);
    replace.add("ľ", "l", 1);
    replace.add("ŕ", "r", 1);
    replace.add("ĺ", "l", 1);
    replace.add("ô", "o", 1);
    replace.add("ł", "l", 1);
    replace.add("ş", "s", 1);
    replace.add("ç", "c", 1);
    replace.add("ü", "u", 1);
    replace.add("ć", "c", 1);
    replace.add("ś", "s", 1);
    replace.add("ź", "z", 1);
    replace.add("ń", "n", 1);
    replace.add("ä", "a", 1);
    replace.add("ë", "e", 1);
    replace.add("ö", "o", 1);
    replace.add("ü", "u", 1);
    replace.add(". ", " =D. ", 5);
    replace.add(", ", " woe, ", 5);
    replace.add("to yo", "tj", 1);
    replace.add("to ano", "tj", 1);
    replace.add("srdce", "srdiiiczkooo", 1);
    replace.add("spaaat", "hajat", 2);
    replace.add("spinkat", "hajinkat", 1);
    replace.add("spinka", "hajinka", 1);
    replace.add("spinkaaa", "hayinkaaa", 1);
    replace.add("howno", "howiiinkooo", 1);
    replace.add("polibek", "muckaaaniii", 1);
    replace.add("liiibaaaniii", "mucinkaaaniii", 1);
    replace.add("dobryyy", "good", 2);
    replace.add("dobraaa", "good", 2);
    replace.add("dobreee", "good", 2);
    replace.add("picz", "piczk", 1);
    replace.add("prdel ", "kakaaaczek ", 1);
    replace.add("prdel,", "kakaaaczek,", 1);
    replace.add("prdel.", "kakaaaczek.", 1);
    replace.add("prdel!", "kakaaaczek!", 1);
    replace.add("do prdele", "do kakaaaczka", 1);
    replace.add("v prdeli", "v kakaaaczku", 1);
    replace.add("prdeliii", "kakaaaczkem", 1);
    replace.add("dobry", "good", 2);
    replace.add("piwo", "piiivo", 1);
    replace.add("dobrze", "OK", 2);
    replace.add("diiik.", "thx.", 1);
    replace.add("diiiky.", "thx.", 1);
    replace.add("diiik!", "thx!!!", 1);
    replace.add("diiiky!", "thx!!!", 1);
    replace.add("diiik,", "thx,", 1);
    replace.add("diiiky,", "thx,", 1);
    replace.add("dekuju", "thx", 1);
    replace.add("dekuji.", "thx.", 1);
    replace.add("mrdka", "mrdaaanek", 1);
    replace.add("mrdky", "mrdaaanky", 1);
    replace.add("mrdkou", "mrdaaanekm", 1);
    replace.add("mrdkami", "mrdaaankama", 1);
    replace.add("mrdkma", "mrdaaankama", 1);
    replace.add("kraaaw", "klawisht", 1);
    replace.add("koza", "koziczka", 1);
    replace.add("kozy", "koziczky", 1);
    replace.add("kozataaa", "koziczkataaa", 1);
    replace.add(" moc ", " mocinky ", 1);
    replace.add(" uuuplne ", " upe ", 1);
    replace.add(" uplne ", " upe ", 1);
    replace.add("wole ", "woe ", 1);
    replace.add(" ano", " jj", 1);
    replace.add("newiiim", "nwm", 1);
    replace.add("newim", "nwm", 1);
    replace.add(" ty vole", " twe", 1);
    replace.add(" ty woe", " twe", 1);
    replace.add("milaaacz", "milaaash", 1);
    replace.add("miluy", "lowiiiskuy", 1);
    replace.add("milov", "lowiiiskow", 1);
    replace.add("neylepshiii ", "best ", 1);
    replace.add("promin ", "sry ", 1);
    replace.add("prominte", "soracz", 1);
    replace.add("ď", "d", 1);
    replace.add("smrt ", "death ", 1);
    replace.add("kurva", "kua", 1);
    replace.add("protoze", "ptz", 1);
    replace.add("protozhe", "ptz", 1);
    replace.add("kurwa", "kua", 1);
    replace.add("prosim", "pllls ", 1);
    replace.add("prosiiim", "pls ", 1);
    replace.add("pawou", "pabou", 1);
    replace.add("huste", "cool", 1);
    replace.add("husteee", "cool", 1);
    replace.add("hustyyy", "cool", 1);
    replace.add(" oka ", " kukucz ", 1);
    replace.add(" oka,", " kukucz,", 1);
    replace.add(" oka.", " kukucz.", 1);
    replace.add(" oka!", " kukucz!", 1);
    replace.add("koczk", "koshisht", 1);
    replace.add("prase", "prasaaatko", 1);
    replace.add("sran", "kakan", 1);
    replace.add("seru", "kakaaam", 1);
    replace.add("spaaat", "dadynkat", 1);
    replace.add("spi ", "dadynkej ", 1);
    replace.add("draaat", "dlaaat", 3);
    replace.add("czay ", "czayiczek ", 1);
    replace.add("puuuydu", "pudu", 1);
    replace.add("boliii", "bolinkaaa", 1);
    replace.add("bill ", "billiiishek ", 1);
    replace.add("bolest.", "bebiiiczkooo.", 1);
    replace.add("bolest,", "bebiii,", 1);
    replace.add("bolest!", "bebiii!!!", 1)
    replace.add("bolestiw", "bebiiiczkow", 1);
    replace.add("ale ", "ae ", 1);
    replace.add("ale ", "aue ", 1);
    replace.add("wolat", "telefooonowat", 1);
    replace.add("kunda", "kundiczkaaa", 1);
    replace.add("czuuuraaak", "czuuulaaaczek", 1);
    replace.add("moye", "moe", 1);
    replace.add("twoye", "twoe", 1);
    replace.add("rziiik", "powiiid", 1);
    replace.add("kamaraaad", "kaaamosh", 1);
    replace.add("tedy ", "teda ", 1);
    replace.add("peysek", "pesaaaczek", 1);
    replace.add("aaaczci", "aaashci", 1);
    replace.add("trochu", "kapishtu", 1);
    replace.add("troshku", "kapishtu", 1);
    replace.add("trocha", "kapishta", 1);
    replace.add("troshka", "kapishta", 1);
    replace.add("polshtaaarz ", "bucliiik ", 1);
    replace.add("polshtaaarzo", "bucliiiko", 1);
    replace.add("polshtaaarze ", "bucliiiky ", 1);
    replace.add("polshtaaarzem ", "bucliiikem ", 1);
    replace.add("polshtaaarzi", "bucliiiku", 1);
    replace.add("polshtaaarzema", "bucliiikama", 1);
    replace.add("polshtaaarzemi", "bucliiikama", 1);
    replace.add("perzin", "perzink", 1);
    replace.add(" ucho", " oushko", 1);
    replace.add(" ushi", " oushka", 1);
    replace.add(" ushat", " oushkat", 1);
    replace.add(" ucha", " ouszka", 1);
    replace.add("ruuuzhow", "ruuuzhowouck", 1);
    replace.add("slowniiik ", "slowniiiczek ", 1);
    replace.add("slowniiiku", "slowniiiczku", 1);
    replace.add("slowniiikuuu", "slowniiiczkuuu", 1);
    replace.add("slowniiiky", "slowniiiczky", 1);
    replace.add("slowniiikem", "slowniiiczkem", 1);
    replace.add("slowniiikama", "slowniiiczkama", 1);
    replace.add("hezk", "klaaasnoushk", 1);
    replace.add("eugeot ", "ezhotek ", 1);
    replace.add("rabant", "laaabik", 1);
    replace.add("kraaaw", "klawishk", 1);
    replace.add("yenom", "enom", 1);
    replace.add("pouze", "enom", 1);
    replace.add("zhaaarowk", "zhaaarowiczk", 1);
    replace.add("zhaaarziwk", "zhaaarziweczk", 1);
    replace.add("wyyyboyk", "wyyyboycziczk", 1);
    replace.add("ymenuyi se", "nadaaaway mi", 1);
    replace.add("ymenuyu se", "nadaaaway mi", 1);
    replace.add("ymenuyiii se", "nadaaaway yim", 1);
    replace.add("ymenuyou se", "nadaaaway yim", 1);
    replace.add("ahoy", "ayoy", 1);
    replace.add("hlawa", "hlawiczka", 1);
    replace.add("hlawo", "hlawiczko", 1);
    replace.add("hlawy", "hlawiczky", 1);
    replace.add("x", "xxx", 1);
    replace.add("hahaha", "hhh", 1);
    replace.add("ch", "x", 1);
    smajl[0] = ' :-***';
    smajl[1] = ' X_x';
    smajl[2] = ' =(';
    smajl[3] = ' =)';
    smajl[4] = ' ;-*';
    smajl[5] = ' O_o';
    smajl[6] = ' ^_^';
    smajl[7] = ' <3';
    smajl[8] = ' xD';
    smajl[9] = ' :-/';
    smajl[10] = ' </3';
    slint[0] = ' *MuUuUcK*';
    slint[1] = ' *LoWe*';
    slint[2] = ' *KiSs*';
    slint[3] = ' Emo Ye BeSt!!!!!';
    slint[4] = ' UmIiIrAaAm, ZhiWOT jE Na hOwNo!!!';
    slint[5] = ' ToKiO HoTeL RuLezZz!!!';
    slint[6] = ' BiLlIiIsHeK Ye BeSt!';
    slint[7] = ' FsHeCkY WaAaS LoWiIiSkUyU!';
    slint[8] = ' YsEm UpE DaAaRk A IiIwL!!!';
    slint[9] = ' Toe WoDWazZz Woe!!!';
    slint[10] = ' WoE NeWiIiIiIiIiSh!!!!!!!!!!';
    slint[11] = ' i hATe EWeRyOnE!!!';
    slint[12] = ' NeMaAa NeKdO ZhIlEtKu?';
    slint[13] = ' SeSh hUstEy!!! mEgA WoE!';
    slint[14] = ' MrTe Te MuCiNkAaAm PiCzO!';
    slint[15] = ' MTMMMMMR';
    slint[16] = ' BoLiNkAaA Me SrDiIiIiIcZkOoOoO </3 :\'(';
    slint[17] = ' <3 :-***';
    slint[18] = ' loWiIisKuYu EmO!!! :-**';
    slint[19] = ' YaAa Se PoDrZiIiZnU!!! :((((';
    slint[20] = ' SmUtNiIiIiIiIiIiM!!!!!!!!!!!! :((((((((';
    slint[21] = ' NiKdO Me NeMaAa LaAaAaAaD!!!!!! :((((((';
	input = ""  + input;
    if (nahradit) {
        for (var i in replace.data) {
            var regexp = new RegExp(replace.str(i).toUpperCase(), "gi");
            input = input.replace(regexp, function (matched) {
                if (replace.index_str(matched) != null) {
                    i = replace.index_str(matched);
                    var rand = Math.floor(Math.random()*replace.rnd(i));
                    if (matched != replace.str(i)) {
                        var rpl = replace.rpl(i).toUpperCase();
                    } else var rpl = replace.rpl(i).charAt(i).toUpperCase();
                    return rand == 0 ? replace.rpl(i) : matched;
                } else return matched;
            });
        }
    }
    if (Ud) {
        input = input.toLowerCase();
        for (var i = 0; i < input.length; i++) {
            if (i % (Math.floor(Math.random()*ratio) + 2) == 0) output = output + input.charAt(i).toUpperCase();
            else output = output + input.charAt(i).toLowerCase();
        }
    } else output = input;
    if (smajliky) output = output + smajl[Math.floor(Math.random()*smajl.length)];
    if (kecy) output = output + slint[Math.floor(Math.random()*slint.length)];
    return output;
}



