(function($) {
	var toggleMembership = function() {
		var $this = $(this);
		$this.prop('disabled', true);
		$.post('./ucp.php?i=groups&mode=membership', {
			action: ($this.prop('checked')?'join':'resign'),
			selected: $this.data('group_id'),
			submit: 'Odeslat'}, function(data, textStatus, xhr) {
				var $f;
				if ($f = $(data).find('form[id=confirm]')) {
					$.post($f.attr('action'), {
						selected: $f.find("input[name=selected]").val(),
						action:  $f.find("input[name=action]").val(),
						confirm: 'Ano',
						confirm_uid: $f.find("input[name=confirm_uid]").val(),
						sess: $f.find("input[name=sess]").val(),
						sid: $f.find("input[name=sid]").val(),
						submit: '1',
					}, function(data, status) {
						$this.prop('disabled', false);
					}, 'html');
				} else {
					$this.prop('disabled', false);
				}
			}, 'html');
		return true;
	}
	$(function() {
		if (typeof($('form#ucp').attr('action')) !== 'undefined' && $('form#ucp').attr('action').match(/i=groups.mode=membership/)) {
			var mode = 0;
			var t;
			$("form .topiclist").each(function() {
				var $this = $(this);
				if ($this.hasClass('cplist')) {
					if (mode == 2) {
						$this.remove().appendTo(t);
					}
					if (mode) {
						$this.find('li').each(function() {
							var $this = $(this);
							var c = $('<input type="checkbox" />')
								.data('group_id', $this.find('input').eq(0).val())
								.prop('checked', mode == 1)
								.prop('disabled', $this.find('input').eq(0).prop('disabled'))
								.css('position', 'absolute').css('margin-top', '6px')
								.css('margin-left', '-6em')
								.change(toggleMembership)
								.prependTo($this.find('dd').eq(0));
						});
					}
				} else {
					var $dt;
					$dt = $this.find('dt');
					var dtt = $dt.html();
					dtt = dtt.substring(0, dtt.indexOf("<"));
					switch(dtt) {
						case 'Člen': {
							$dt.html('Členství ve skupinách');
							$('<span style="position:absolute; margin-left: -7.5em;">Členem</a>').prependTo($dt.next());
							t = $this.next();
							mode = 1;
						} break;
						case 'Bez členství': {
							$this.remove();
							mode = 2;
						} break;
						default: {
							mode = 0;
						} break;
					}
				}
			});
		}
	});

})(jQuery);
