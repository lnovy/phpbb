<?php
/**
*
*  [Czech]
*
* @package language
* @version $Id: umil_groups_on_reg.php, v 1.0.1 2009/1/21 12:53:34  Exp $
* @copyright (c) 2009 mtrs, czech translation by Jakub Michálek
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang)) 
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM
//

$lang = array_merge($lang, array(
		'INSTALL_GROUPS_ON_REGISTRATION'				=> 'Instalovat Skupiny při registraci a vlastní pole v profilu',
		'INSTALL_GROUPS_ON_REGISTRATION_CONFIRM'		=> 'Jste připraveni instalovat Mod pro skupiny při registraci a vlastní pole v profilu?',
		'GROUPS_ON_REGISTRATION'						=> 'Skupiny při registraci a vlastní pole v profilu',
		'GROUPS_ON_REGISTRATION_EXPLAIN'				=> 'Instalování databáze Skupin při registraci a vlastních polí v profilu provede změnu UMIL auto metodou.',
		'UNINSTALL_GROUPS_ON_REGISTRATION'				=> 'Odinstalovat Skupiny při registraci a vlastní pole v profilu',
		'UNINSTALL_GROUPS_ON_REGISTRATION_CONFIRM'		=> 'Jste připraveni odinstalovat Mod pro skupiny při registraci a vlastní pole v profilu? Všechna nastavení a údaje uložené tímto rozšířením budou odstraněna!',
		'UPDATE_GROUPS_ON_REGISTRATION'					=> 'Aktualizovat Mod pro skupiny při registraci a vlastní pole v profilu',
		'UPDATE_GROUPS_ON_REGISTRATION_CONFIRM'			=> 'Jste připraveni aktualizovat Mod pro skupiny při registraci a vlastní pole v profilu?',

));

?>
