<?php
/**
*
*  [Czech]
*
* @package language
* @version $Id:  info_acp_group_on_reg.php, v1.0.1 2009/11/21 15:35:00 mtrs Exp $
* @copyright (c) 2009 mtrs, czech translation by Jakub Michálek
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//



$lang = array_merge($lang, array(
	'ACP_GROUPS_REGS'						=> 'Skupiny při registraci',
	'ACP_GROUPS_REGS_EXPLAIN'				=> 'U skupin, které nejsou speciální nebo administrátorské, můžete určit, aby se zobrazily při registraci.',
	'ADD_GROUP'								=> 'Přidat skupinu',
	'ADD_CPF_GROUP'							=> 'Přidat skupinu založenou na vlastních polích v profilu',
	
	'CPF_AUTO_GROUP'						=> 'Resynchronizovat skupinu založenou na vlastních polích v profilu',
	'CPF_AUTO_GROUP_EXPLAIN'				=> 'Dávkově přidat nebo odstranit uživatele do skupin založených na vlastních polích v profilu podle uživatelských údajů.',
	'CPF_ADD_GROUP_LIST_CHANGED'			=> 'Skupiny založené na vlastních polích v profilu byly synchronizovány',

	'CPF_LANG_VALUE'						=> 'Nastavení jazykové hodnoty lang',
	'CPF_FIELD_NAME'						=> 'Název vlastního pole v profilu',
	'CPF_TO_GROUPS'							=> 'Skupiny založené na vlastních polích v profilu',
	'CPF_TO_GROUPS_EXPLAIN'					=> 'Můžete určit skupiny, do kterých lze přidávat uživatele v závislosti na vyplnění vlastních polí v profilu při registraci nebo v uživatelském panelu. Také můžete přidávat uživatele do skupin dávkově podle vlastních polí v profilu. Uvědomte si prosím, že změna vlastního pole v profilu může resetovat nastavení skupin.',

	'ENTER_GROUP_ID_NAME'					=> 'Měli byste vybrat platný název nebo číslo skupiny',

	'GROUPS_ON_REGISTRATION'				=> 'Skupiny',
	'GROUPS_ON_REGISTRATION_EXPLAIN'		=> 'Vyberte jednu ze skupin, do které se přidáte.',
	'GROUPS_ENABLED'						=> 'Povolit zobrazování skupin při registraci',
	'GROUPS_ENABLED_EXPLAIN'				=> 'Povolení zobrazí u registrace skupiny, do kterých se lze přidat.',
	'GROUPS_REQUIRE'						=> 'Požadovat výběr skupiny při registraci',
	'GROUPS_REQUIRE_EXPLAIN'				=> 'Uživatelé budou muset vybrat alespoň jednu skupinu z nabídky.',
	'GROUPS_MULTIPLE_REGISTRATION'			=> 'Povolit přidání do více skupin při registraci',
	'GROUPS_MULTIPLE_REGISTRATION_EXPLAIN'	=> 'Povolení dá uživatelům možnost, aby se při registraci přidali do více než jedné skupiny. Pokud je tato volba povolena,<br /> skupiny při registraci nemohou být výchozí.',
	'GROUPS_DEFAULT'						=> 'Nastavit skupinu vybranou při registraci jako výchozí',
	'GROUPS_DEFAULT_EXPLAIN'				=> 'Skupina bude nastavena jako výchozí.',
	'GROUPS_TO_CPF_ENABLE'					=> 'Povolit skupiny založené na vlastních polích v profilu',
	'GROUPS_TO_CPF_ENABLE_EXPLAIN'			=> 'Povolení této možnosti vytvoří automatické skupiny založené na vlastních polích uživatelů v profilu zadaných při registraci,<br />	v profilu v uživatelském panelu and ACT uživatelského profilu.',
	'GROUPS_TO_CPF_NO_PENDING'				=> 'Přidat uživatele do skrytých nebo uzavřených skupin bez čekání',
	'GROUPS_TO_CPF_NO_PENDING_EXPLAIN'		=> 'Povolení této možnosti nastaví automatické přidání do skupin založených na vlastních polích v profilu, jde-li o skryté nebo uzavřené skupiny',
	'GROUPS_FIELD_NAME'						=> 'Označení vlastního pole v profilu',
	'GROUPS_FIELD_NAME_EXPLAIN'				=> 'Vyberte název vlastního pole v profilu. Zobrazuje se pouze pole typu rolovací menu.',
	'GROUPS_LANG_VALUE'						=> 'Hodnota jazyk u vlastního pole v profilu',
	'GROUPS_LANG_VALUE_EXPLAIN'				=> 'Pokud je tato volba při registraci vybrána, uživatel bude automaticky přidán do výše uvedené skupiny, např. Ženy',
	'GROUPS_CPF_NAME'						=> 'Hodnota možnosti u vlastního pole v profilu',
	'GROUPS_CPF_NAME_EXPLAIN'				=> 'Přejděte na možnosti vlastního pole v profilu, abyste přidali uživatele do výše uvedené skupiny.',
	'GROUP_NAME'							=> 'Název skupiny',
	'GROUP_NAME_EXPLAIN'					=> 'Vyberte prosím název skupiny, který bude zobrazen při registraci.',
	'GROUP_NAME_ID'							=> 'Skupina',
	'GROUP_NAME_ID_EXPLAIN'					=> 'Vyberte prosím skupinu, která bude zobrazena při registraci.',
	'GROUP_MEMBERS'							=> 'Členové',
	'GROUP_ID'								=> 'ID skupiny',
	'GROUP_DEFAULT'							=> 'Výchozí',
	'GROUP_ADDED'							=> 'Skupina byla úspěšně přidána do zobrazovaných při registraci.',
	'GROUP_CPF_ADDED'						=> 'Tabulka skupin založených na vlastních polích v profilu byla aktualizována.',
	'GROUP_REMOVED'							=> 'ID vybrané skupiny bylo úspěšně odstraněno.',
	'CPF_GROUP_REMOVED'						=> 'Skupina založená na vlastních polích v profilu byla úspěšně odstraněna',

	'LOG_GROUPS_REG_DELETE'					=> '<strong>Skupina číslo %s odstraněna z nabídky skupin při registraci</strong>',
	'LOG_GROUPS_REG_ADD'					=> '<strong>Skupina číslo %s přidána do nabídky skupin zobrazovaných při registraci</strong>',
	'LOG_GROUPS_CPF_DELETE'					=> '<strong>Skupiny založené na vlastních polích v profilu úspěšně aktualizovány</strong>',
	'LOG_GROUPS_CPF_ADD'					=> '<strong>Skupina číslo %s přidána mezi skupiny založené na vlastních polích v profilu</strong>',
	'LOG_CONFIG_GROUPS_REG_UPDATED'			=> '<strong>Konfigurace skupin při registraci aktualizována</strong>',
	'LOG_CPF_GROUPS_SYNCHRONISED'			=> '<strong>Skupiny založené na vlastních polích v profilu byly synchronizovány</strong>',

	'NO_GROUP'								=> 'Číslo skupiny neexistuje',
	'NO_GROUP_TO_ADD'						=> 'Neexistuje žádná skupina, kterou by bylo možné přidat, prosím vytvořte nejprve novou skupinu',
	'NO_CPF_TO_ADD'							=> 'Neexistuje žádné vlastní pole v profilu typu rolovací menu nebo jsou všechna pole přiřazena některé skupině. <br />Prosím vytvořte nové vlastní pole v profilu typu rolovací menu nebo přidejte možnosti k již existujícím polím.',
	'NO_CPF_GROUP_SELECTED'					=> 'Prosím vyberte některou skupinu a název vlastního pole v profilu a název hodnoty',
	'NO_CPF_GROUP_EXIST'					=> 'Neexistuje žádná skupina v seznamu skupin založených na vlastních polích v profilu. Před synchronizací prosím přidejte aspoň jednu skupinu.',
	'NO_VALUE_CHANGED'						=> 'Nezměnili jste žádnou hodnotu, která by se měla odeslat',

	'REMOVE'								=> 'Odstranit',
	'REMOVE_GROUP'							=> 'Odstranit ze zobrazení skupin při registraci',

));


?>
