<?php
/**
*
* thanks_mod[English]
*
* @package language
* @version $Id: thanks.php,v 127 2010-04-17 10:02:51Палыч $
* @copyright (c) 2008 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
   exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
'GIVEN'                  => 'Dal&nbsp;poděkování',
   'GRATITUDES'            => 'Poděkování',
   
   'INCORRECT_THANKS'         => 'Neplatné poděkování',
   
   'JUMP_TO_FORUM'            => 'Přejdi na fórum',
   'JUMP_TO_TOPIC'            => 'Přejdi na vlákno',

   'FOR_MESSAGE'            => ' za příspěvek',
   'FURTHER_THANKS'            => ' a další uživatel',
   'FURTHER_THANKS_PL'         => ' a %d dalších uživatelů',
   
   'NO_VIEW_USERS_THANKS'      => 'Nemáte povoleno prohlížet seznam poděkování.',

   'RECEIVED'               => 'Dostal&nbsp;poděkování',
   'REMOVE_THANKS'            => 'Odebrat poděkování: ',
   'REMOVE_THANKS_CONFIRM'      => 'Opravdu chcete odebrat poděkování?',
   'REPUT'                  => 'Hodnocení',
   'REPUT_TOPLIST'            => 'Toplist',
   'RETING_LOGIN_EXPLAIN'      => 'Nemáte povoleno prohlížet toplist.',
   'RATING_NO_VIEW_TOPLIST'   => 'Nemáte povoleno prohlížet toplist.',
   'RATING_VIEW_TOPLIST_NO'   => 'Toplist je prázdný nebo je zakázán administrátorem',
   'RATING_FORUM'            => 'Fórum',
   'RATING_POST'            => 'Příspěvek',
   'RATING_TOP_FORUM'         => 'Fóra s nejvíce poděkováními',
   'RATING_TOP_POST'         => 'Příspěvky s nejvíce poděkováními',
   'RATING_TOP_TOPIC'         => 'Vlákna s nejvíce poděkováními',   
   'RATING_TOPIC'            => 'Vlákna',
   'RETURN_POST'            => 'Zpět',

   'THANK'                  => 'krát',
   'THANK_FROM'            => 'z',
   'THANK_TEXT_1'            => 'Za tento příspěvek dostal jeho autor ',
   'THANK_TEXT_2'            => ' poděkování od: ',
   'THANK_TEXT_2pl'         => ' %d poděkování od: ',
   'THANK_POST'            => 'Poděkovat autorovi',
   'THANKS'               => 'krát',
   'THANKS_BACK'            => 'zpět',
   'THANKS_INFO_GIVE'         => 'Už jste poděkoval za tuto zprávu.',
   'THANKS_INFO_REMOVE'      => 'Už jste odebral poděkování.',
   'THANKS_LIST'            => 'Zobraz/zavři přehled',
   'THANKS_PM_MES_GIVE'      => 'Poděkovat za zprávu',
   'THANKS_PM_MES_REMOVE'      => 'Odeber poděkování',
   'THANKS_PM_SUBJECT_GIVE'   => 'Poděkovat za zprávu',
   'THANKS_PM_SUBJECT_REMOVE'   => 'Odeber poděkování',
   'THANKS_USER'            => 'Seznam poděkování',

// Install block
	'THANKS_INSTALLED'			=> 'Thanks for the message',
	'THANKS_INSTALLED_EXPLAIN'  => '<strong>CAUTION!<br />Strongly recommend to run this installation only after following the instructions on changes to the code files conference (or perform the installation using AutoMod)!<br />Also strongly recommend select Yes to Display Full Results (below)!</strong>',
	'THANKS_CUSTOM_FUNCTION'	=> 'Update values table _thanks',
));
?>