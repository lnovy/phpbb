<?php
/** 
*
* acp_mods [Czech]
*
* @package language
* @version $Id: mods.php 487 2009-05-18 09:44:06Z ameeck $
* @copyright (c) 2009 Vojtěch Vondra
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
* Original copyright: 2008 phpBB Group 
*/
/**
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE 
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine


$lang = array_merge($lang, array(
	'ADDITIONAL_CHANGES'	=> 'Dostupné změny',

	'AM_MOD_ALREADY_INSTALLED'	=> 'AutoMOD zjistil, že tento MOD už je nainstalovaný a že nelze pokračovat dále.',

	'APPLY_THESE_CHANGES'	=> 'Uplatnit tyto změny',
	'APPLY_TEMPLATESET'		=> 'této sadě šablon',
	'AUTHOR_EMAIL'			=> 'E-mail autora',
	'AUTHOR_INFORMATION'	=> 'Informace o autorovi',
	'AUTHOR_NAME'			=> 'Jméno autora',
	'AUTHOR_NOTES'			=> 'Poznámky autora',
	'AUTHOR_URL'			=> 'URL autora',
	'AUTOMOD'				=> 'AutoMOD',
	'AUTOMOD_CANNOT_INSTALL_OLD_VERSION'	=> 'Verze AutoMODu, kterou se pokoušíte nainstalovat už byla nainstalovaná. Odstraňte adresář install/.',
	'AUTOMOD_UNKNOWN_VERSION'	=>	'Nebylo možné aktualizovat AutoMOD, protože nešlo určit současně nainstalovanou verzi. Verze uvedená u vaší instalace je %s.',

	'CAT_INSTALL_AUTOMOD'	=> 'AutoMOD',
	'CHANGE_DATE'	=> 'Datum vydání',
	'CHANGE_VERSION'=> 'Verze',
	'CHANGES'		=> 'Změny',
	'CHECK_AGAIN'	=> 'Zkontrolovat znovu',
	'COMMENT'		=> 'Poznámka',
	'CREATE_TABLE'	=> 'Změny v databázi',
	'CREATE_TABLE_EXPLAIN'	=> 'AutoMOD provedl úpravy v databázi, včetně přidání oprávnění, kteřé bylo přiděleno roli "Hlavní administrátor".',

	'DIR_PERMS'			=> 'Oprávnění adresáře',
	'DIR_PERMS_EXPLAIN'	=> 'Některé systémy vyžadují, aby adresáře měli určitá oprávnění. Běžně stačí výchozí hodnota 0755. Toto nastavení nemá žádný vliv na Windows serverech.',
	'DIY_INSTRUCTIONS'	=> 'Instrukce "Udělej si sám"',
	'DEPENDENCY_INSTRUCTIONS'	=>	'MOD, který se snažíte nainstalovat závisí na jiném MODu. AutoMOD nemohl zjistit, jestli jej máte nainstalovaný. Před instalací věřte, že máte nainstalovaný MOD <strong><a href="%1$s">%2$s</a></strong>.',
	'DESCRIPTION'	=> 'Popis',
	'DETAILS'		=> 'Podrobnosti',

	'EDITED_ROOT_CREATE_FAIL'	=> 'AutoMOD nemohl vytvořit adresář, kam budou uloženy upravené soubory.',
	'ERROR'			=> 'Chyba',

	'FILE_EDITS'		=> 'Úpravy souborů',
	'FILE_EMPTY'		=> 'Prázdný soubor',
	'FILE_MISSING'		=> 'Nelze najít soubor',
	'FILE_PERMS'		=> 'Oprávnění souboru',
	'FILE_PERMS_EXPLAIN'=> 'Některé systémy vyžadují, aby soubory měli určitá oprávnění. Běžně stačí výchozí hodnota 0644. Toto nastavení nemá žádný vliv na Windows serverech.',
	'FILE_TYPE'			=> 'Typ komprimovaného souboru',
	'FILE_TYPE_EXPLAIN'	=> 'Tato položka je uplatněna pouze při metodě zápisu "Stáhnutí komprimovaného souboru".',
	'FILESYSTEM_NOT_WRITABLE'	=> 'AutoMOD zjistil, že soubory vašeho fóra nejsou zapisovatelné, nelze tedy použít metodu přímého zápisu.',
	'FIND'				=> 'Najít',
	'FIND_MISSING'		=> 'Obsah uvedený v instrukci "Najít" uvedené v MODu nebyl nalezený',
	'FORCE_INSTALL'		=> 'Vynutit instalaci',
	'FORCE_CONFIRM'		=> 'Možnost "Vynutit instalaci" znamená, že MOD nebude nainstalován úplně. Budete muset provést některé manuální úpravy, aby fungoval správně. Chcete pokračovat?',
	'FTP_INFORMATION'	=> 'Informace o FTP',
	'FTP_NOT_USABLE'	=> 'Funkce FTP nemůže být použita, neboť je zakázána vaším hostingem.',
	'FTP_METHOD_ERROR' => 'Nebyla nalezena žádná metoda FTP, zkontrolujte nastavení AutoMODu. Musí v něm být zvolena odpovídající možnost.',

	'INHERIT_NO_CHANGE'	=> 'Žádné změny nemohli být vykonány na tomto souboru, protože šablona %1$s závisí na %2$s.',
	'INLINE_FIND_MISSING'=> 'Obsah uvedený v instrukci "Najít na řádku" uvedené v MODu nebyl nalezený.',
	'INSTALL_AUTOMOD'	=> 'Installace AutoMODu',
	'INSTALL_TIME'		=> 'Doba instalace',
	'INSTALL_MOD'		=> 'Nainstalovat MOD',
	'INSTALL_ERROR'		=> 'Jedna nebo více instalačních procedur selhala. Prosím zkontrolute jednotlivé kroky níže, proveďte potřebné úpravy a zkuste postup znovu. Můžete pokračovat v instalaci, i když některé kroky selhaly. <strong>Toto silně nedoporučujeme, protože to může zapříčinit, že vaše fórum nebude fungovat správně.</strong>',
	'INSTALL_FORCED'	=> 'Vynutili jste instalaci tohoto MODu, přestože se v ní objevily chyby. Vaše fórum může být rozbité, poznamenejte si kroky instalace, které se nezdařili a ručně je opravte.',
	'INSTALLED'			=> 'MOD byl nainstalován',
	'INSTALLED_EXPLAIN'	=> 'Váš MOD byl nainstalován! Na této stránce vidíte některé z výsledků instalace. Pokud jste si všimli jakýchkoliv chyb nebo vám není něco jasné, zeptejte se na <a href="http://www.phpbb.com">phpBB.com</a>.',
	'INSTALLED_MODS'	=> 'Instalované MODy',
	'INSTALLATION_SUCCESSFUL'	=> 'AutoMOD byl nainstalován. Od této chvíle můžete spravovat MODy do phpBB přes záložku AutoMOD ve vaší administraci.',
	'INVALID_MOD_INSTRUCTION'	=> 'Tento MOD má neplatnou instrukci nebo selhalo hledání na konkrétním řádku.',

	'LANGUAGE_NAME'		=> 'Název jazyka',

	'MANUAL_COPY'				=> 'Neproběhl pokus o zkopírování',
	'MOD_CONFIG'				=> 'Nastavení AutoMODu',
	'MOD_CONFIG_UPDATED'	=> 'Nastavení AutoMODu bylo změněno.',
	'MOD_DETAILS'				=> 'Podrobnosti o MODu',
	'MOD_DETAILS_EXPLAIN'		=> 'Zde naleznete všechny informace o MODu, který jste vybrali.',
	'MOD_MANAGER'				=> 'AutoMOD',
	'MOD_NAME'					=> 'Název MODu',
	'MOD_OPEN_FILE_FAIL'		=> 'AutoMOD nemohl otevřít %s.',
	'AUTOMOD_INSTALLATION'		=> 'Instalace AutoMODu',
	'AUTOMOD_INSTALLATION_EXPLAIN'	=> 'Vítejte v instalaci AutoMODu, automatického instalátoru MODů pro phpBB. Budete potřebovat údaje FTP pro připojení k vašemu serveru, pokud AutoMOD zjistí, že je to nejlepší způsob zápisu. Výsledky testu požadavků jsou níže.',

	'MODS_CONFIG_EXPLAIN'		=> 'Můžete si vybrat to, jak bude AutoMOD upravovat vaše soubory. Nejuniverzálnější je možnost stažení souborů v komprimovaném formátu. Ostatní způsoby vyžadují dodatečná oprávnění na serveru.',
	'MODS_COPY_FAILURE'			=> 'Soubor %s nemohl být zkopírován na své místo. Zkontrolujte svá oprávnění nebo použijte jinou možnost zápisu.',
	'MODS_EXPLAIN'				=> 'Na této stránce máte přehled všech MODů dopstuných pro vaše fórum. MODy vám umožňují přizpůsobovat své fórum a přidávat na něj nové funkce. Pro více informací o MODech a AutoMODu navštivte <a href="http://www.phpbb.com/mods">web phpBB</a>. Pro přidání MODu do seznamu, rozbalte jej a nahrajte do adresáře store/mods/ na vašem serveru.',
	'MODS_FTP_FAILURE'			=> 'AutoMOD nemohl přesunout soubor %s na jeho místo pomocí FTP',
	'MODS_FTP_CONNECT_FAILURE'	=> 'AutoMOD se nemohl připojit na váš server FTP. Chybové hlášení: %s',
	'MODS_MKDIR_FAILED'			=> 'Adresář %s nemohl být vytvořen',
	'MODS_SETUP_INCOMPLETE'		=> 'Objevila se chyba ve vaší konfiguraci a AutoMOD nemůže fungovat. Toto se může stát pokud se změní některá nastavení jako je například uživatel FTP. Zkontrolujte prosím údaje v sekci Nastavení AutoMODu.',

	'NAME'			=> 'Název',
	'NEW_FILES'		=> 'Nové soubory',
	'NO_ATTEMPT'	=> 'Nebylo provedeno',
	'NO_INSTALLED_MODS'		=> 'Nebyly nalezeny žádné nainstalované MODy.',
	'NO_MOD'				=> 'Vybraný MOD nebyl nalezen.',
	'NO_UNINSTALLED_MODS'	=> 'Nebyly nalezeny žádné nenainstalované MODy.',	

	'ORIGINAL'	=> 'Původní',

	'PATH'					=> 'Cesta',
	'PREVIEW_CHANGES'		=> 'Náhled změn',
	'PREVIEW_CHANGES_EXPLAIN'	=> 'Zobrazí změny, které budou vykonány, ještě před tím, než budou spuštěny.',
	'PRE_INSTALL'			=> 'Příprava na instalaci',
	'PRE_INSTALL_EXPLAIN'	=> 'Na této stránce máte přehled všech úprav, které budou provedeny na vašem fóru, ještě před tím, než budou vykonány. <strong>POZOR</strong>, jakmile budou přijaty, základní soubory vaší instalace budou upraveny a mohou se provést i úpravy databáze. Pokud se instalace nezdaří a stále budete mít přístup k AutoMODu, budete mít možnost MOD odinstalovat a vrátit fórum do původního stavu.',
	'PRE_UNINSTALL'			=> 'Příprava na odinstalaci',
	'PRE_UNINSTALL_EXPLAIN'	=> 'Na této stránce máte přehled všech úprav, které budou provedeny na vašem fóru, pro odinstalaci MODu. <strong>POZOR</strong>, jakmile budou přijaty, základní soubory vaší instalace budou upraveny a mohou se provést i úpravy databáze. Tento proces používá procesy, které obrací instalační proces a nemusí být na 100% přesné. Pokud se odinstalace nezdaří a stále budete mít přístup k AutoMODu, budete mít možnost vrátit fórum do původního stavu.',

	'REMOVING_FILES'	=> 'Soubory k odstranění',
	'RETRY'				=> 'Zkusit znovu',
	'RETURN_MODS'		=> 'Vrátit se na AutoMOD',
	'REVERSE'			=> 'Obrátit proces',
	'ROOT_IS_READABLE'	=> 'Kořenový adresář phpBB má práva pro čtení.',
	'ROOT_NOT_READABLE'	=> 'AutoMOD nebyl schopen otevřít soubor index.php pro čtení. Oprávnění souborů jsou pravděpodobně příliš omezující, což zabrání správnému fungování AutoMODu. Upravte svá oprávnění a zkuste to znovu.',


	'SOURCE'		=> 'Zdroj',
	'SQL_QUERIES'	=> 'SQL dotazy',
	'STATUS'		=> 'Stav',
	'STORE_IS_WRITABLE'			=> 'Adresář store/ je zapisovatelný.',
	'STORE_NOT_WRITABLE_INST'	=> 'Instalační průvodce zjistil, že adresář store/ není zapisovatelný. To je nutné pro bezproblémový běh AutoMODu. Upravte tedy svá oprávnění a zkuste instalaci znovu.',
	'STORE_NOT_WRITABLE'		=> 'Adresář store/ není zapisovatelný.',
	'STYLE_NAME'	=> 'Název stylu',
	'SUCCESS'		=> 'Úspěch',

	'TARGET'		=> 'Cíl',

	'UNKNOWN_MOD_AUTHOR-NOTES'	=> 'Autor nepřidal žádné poznámky.',
	'UNKNOWN_MOD_COMMENT'		=> '',
	'UNKNOWN_MOD_INLINE-COMMENT'=> '',
	'UNKNOWN_QUERY_REVERSE' => 'Neznámý vratný dotaz',

	'UNINSTALL'				=> 'Odinstalovat',
	'UNINSTALLED'			=> 'MOD byl odinstalován',
	'UNINSTALLED_MODS'		=> 'Nenainstalované MODy',
	'UNINSTALLED_EXPLAIN'	=> 'MOD byl odinstalován. Zde vidíte výsledky odinstalace. Pokud jste si všimli jakýchkoliv chyb nebo máte jiné připomínky, obraťte se na <a href="http://www.phpbb.com">phpBB.com</a>.',
	'UPDATE_AUTOMOD'		=> 'Aktualizovat AutoMOD',
	'UPDATE_AUTOMOD_CONFIRM'=> 'Opravdu chcete aktualizovat AutoMOD.',

	'VERSION'		=> 'Verze',

	'WRITE_DIRECT_FAIL'		=> 'AutoMOD nemohl zkopírovat soubor %s na místo použitím přímé metody zápisu. Prosím využijte jinou metodu a zkuste proces spustit znovu.',
	'WRITE_DIRECT_TOO_SHORT'=> 'AutoMOD nestihl dokončit zápis do souboru %s. Nejprve zkuste zmáčknout tlačítko "Zkusit znovu". Pokud to nepomůže, použijte jinou metodu zápisu.',
	'WRITE_MANUAL_FAIL'		=> 'AutoMOD nemohl přidat soubor %s do komprimovaného archivu. Použijte jinou možnost zápisu.',
	'WRITE_METHOD'			=> 'Metoda zápisu',
	'WRITE_METHOD_DIRECT'	=> 'Přímá',
	'WRITE_METHOD_EXPLAIN'	=> 'Můžete zvolit preferovaný způsob zápisu do souboru. Nejuniverzálnější možnost je "Stáhnutí komprimovaného souboru".',
	'WRITE_METHOD_FTP'		=> 'FTP',
	'WRITE_METHOD_MANUAL'	=> 'Stáhnutí komprimovaného souboru',

	// These keys for action names are purposely lower-cased and purposely contain spaces
	'after add'				=> 'Přidat za',
	'before add'			=> 'Přidat před',
	'find'					=> 'Najít',
	'in-line-after-add'		=> 'Na řádku přidat za',
	'in-line-before-add'	=> 'Na řádku přidat před',
	'in-line-edit'			=> 'Na řádku najít',
	'in-line-operation'		=> 'Na řádku navýšit hodnotu',
	'in-line-replace'		=> 'Na řádku nahradit pomocí',
	'in-line-replace-with'	=> 'Na řádku nahradit pomocí',
	'replace'				=> 'Nahradit pomocí',
	'replace with'			=> 'Nahradit pomocí',
	'operation'				=> 'Navýšit hodnotu',
));

?>
