<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $id: google_txt.php - 4382 11-20-2008 14:38:27 - 2.0.RC1 dcz $
* @copyright (c) 2006 - 2008 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* google_txt [English]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'GOOGLE_TXT' => 'TXT Sitemap',
	'GOOGLE_TXT_EXPLAIN' => 'Toto jsou parametry pro Google sitemap TXT jednotky.
	Tyto jednotky můžete plně integrovat z textového seznamu Vaších sitemap a využít vyrovnávací pamět XSLt.<br/>
	Některá nastavení mohou mít větší prioritu v závislosti na Google sitemaps a můžou převyšovat hlavní nastavení.<br/>
	Každý textový soubor který jste přidaly ze svých sitemaps/zdrojů či složek, bude funkční jenom po promazání vyrovnávací paměti všech modulů sitemaps v ACP.
	Toto můžete provést u výše uvedeného odkazu.<br/>
	Každý seznam v textovém souboru musí obsahovat na každé řádce jenom jeden záznam tj. pouze jednu URL, na každém řádku musí být dodržená přesná hierarchie složení Vaší URL.
	<b>google</b>složka_název<b>.txt</b>.<br />
	Vstup bude vytvořený v SitemapIndex s URL <b>example.com/sitemap.php?txt=txt_file_name</b> a <b>example.com/txt-txt_file_name.xml</b>, přepsaná URL.<br/>
	Ve jméně zdrojového souboru je třeba použít alfanumerické znaky (0-9A-Z) plus oba oddělovače "_" a "-".<br/>
	<u style="color:red;">Poznámka :</u><br/>
	Doporučuje se aby při velkých textových souborech byla vyrovnávací pomět zcela vyčištěna, aby se zabránilo příliš vysoké zátěži serveru.',
	// Main
	'GOOGLE_TXT_CONFIG' => 'Nastavení TXT sitemaps',
	'GOOGLE_TXT_CONFIG_EXPLAIN' => 'Z některým nastavením nelze manipulovat jelikož hlavní nastavení je nadřazeno tomuto nastavení.',
	'GOOGLE_TXT_RANDOMIZE' => 'Příkaz pro vytvoření náhodného čísla',
	'GOOGLE_TXT_RANDOMIZE_EXPLAIN' => 'Můžete vzít náhodné URL z Vašeho textového souboru. Změna pořadí v pravidelných intervalech může zatížit Váš server.Tato volba je vhodná zejména v případech kdy váš textový soubor obsahuje více jak 1000 URL a však maximálně 5000 URL. Jestliže Váš soubor obsahuje více jak 5000 URL je dobré tento soubor rozdělit na dva.',
	'GOOGLE_TXT_UNIQUE' => 'Kontrola duplikátů',
	'GOOGLE_TXT_UNIQUE_EXPLAIN' => 'Aktivujte, aby jste se ujistily, že se Vaše URL ve Vašem textovém souboru nachází pouze jednou, tímto zjistíte jestli se ve Vašem souboru nenachází duplikát.',
	'GOOGLE_TXT_FORCE_LASTMOD' => 'Poslední modifikace',
	'GOOGLE_TXT_FORCE_LASTMOD_EXPLAIN' => 'Zde můžete vidět kdy byla vydaná poslední modifikace ( i když není vyrovnávací paměť aktivní ) Všechny adresy ve vaší sitemaps nebudou změněny, jelikož tento modul spočítá veškeré priority všech Vašich URL.',
	// Reset settings
	'GOOGLE_TXT_RESET' => 'TXT Sitemaps Moduly',
	'GOOGLE_TXT_RESET_EXPLAIN' => 'Obnovit všechny výchozí hodnoty třídícího mechanizmu v TXT Sitemaps modulu.',
	'GOOGLE_TXT_MAIN_RESET' => 'Nastavení TXT sitemaps',
	'GOOGLE_TXT_MAIN_RESET_EXPLAIN' => 'Obnovit všechny výchozí nastavení všech možností v "TXT Sitemaps nastaveních" na hlavní kartě TXT Sitemaps modulu.',
	'GOOGLE_TXT_CACHE_RESET' => 'Vyrovnávací pamět TXT sitemaps',
	'GOOGLE_TXT_CACHE_RESET_EXPLAIN' => 'Obnovit všechny výchozí nastavení všech možností ukládání do vyrovnávací paměti na TXT Sitemap modulu.',
	'GOOGLE_TXT_GZIP_RESET' => 'TXT Sitemap Gunzip',
	'GOOGLE_TXT_GZIP_RESET_EXPLAIN' => 'Obnovit všechny výchozí nastavení všech možností gunzip v TXT Sitemap modulu.',
	'GOOGLE_TXT_LIMIT_RESET' => 'Limit TXT Sitemap',
	'GOOGLE_TXT_LIMIT_RESET_EXPLAIN' => 'Obnovit na výchozí hodnoty všech mezních možností v TXT Sitemap modulu.',
));
?>