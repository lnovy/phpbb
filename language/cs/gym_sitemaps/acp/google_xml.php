<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $id: google_xml.php - 5441 11-20-2008 14:38:27 - 2.0.RC1 dcz $
* @copyright (c) 2006 - 2008 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* google_xml [English]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'GOOGLE_XML' => 'XML Sitemap',
	'GOOGLE_XML_EXPLAIN' => 'Toto jsou parametry pro Google sitemap XML jednotky.
	Tyto jednotky můžete plně integrovat z xml seznamu Vaších sitemap a využít vyrovnávací pamět XSLt.<br/>
	Některá nastavení mohou mít větší prioritu v závislosti na Google sitemaps a můžou převyšovat hlavní nastavení.<br/>
	Každý xml soubor který jste přidaly ze svých sitemaps/zdrojů či složek, bude funkční jenom po promazání vyrovnávací paměti všech modulů sitemaps v ACP.
	Toto můžete provést u výše uvedeného odkazu.<br/>
	Každý seznam v xml souboru musí obsahovat na každé řádce jenom jeden záznam tj. pouze jednu URL,
	na každém řádku musí být dodržená přesná hierarchie složení Vaší URL. <b>google_/xml_složka_název.xml</b>.<br />
	Vstup bude vytvořený v SitemapIndex s URL <b>example.com/sitemap.php?txt=xml_file_name</b> a <b>example.com/xml-xml_file_name.xml</b>, nahrazená URL.<br/>
	Ve Jméně zdrojového souboru je třeba použít alfanumerické znaky (0-9A-Z) plus oba oddělovače "_" a "-".<br/><u style="color:red;">Poznámka :</u><br/> Doporučuje se aby při velkých xml souborech byla vyrovnávací pomět zcela vyčištěna, aby se zabránilo příliš vysoké zátěži serveru.',
	// Main
	'GOOGLE_XML_CONFIG' => 'Nastavení sitemaps XML',
	'GOOGLE_XML_CONFIG_EXPLAIN' => 'Z některým nastavením nelze manipulovat jelikož hlavní nastavení je nadřazeno tomuto nastavení.',
	'GOOGLE_XML_RANDOMIZE' => 'Příkaz pro vytvoření náhodného čísla',
	'GOOGLE_XML_RANDOMIZE_EXPLAIN' => 'Můžete vzít náhodné URL z Vašeho xml souboru.
	Změna pořadí v pravidelných intervalech může zatížit Váš server.
	Tato volba je vhodná zejména v případech kdy váš xml soubor obsahuje více jak 1000 URL a však maximálně 5000 URL.
	Jestliže Váš soubor obsahuje více jak 5000 URL je dobré tento soubor rozdělit na dva.<br/>
	<u>Poznámka:</u><br/>Tato volba vyžaduje plnou kontrolu syntakce a analýzu zdrojového souboru, toto je doporučené použít při aktivováné cache',
	'GOOGLE_XML_UNIQUE' => 'Kontrola duplikátů',
	'GOOGLE_XML_UNIQUE_EXPLAIN' => 'Aktivujte, aby jste se ujistily, že se Vaše URL ve Vašem xml souboru nachází pouze jednou, tímto zjistíte jestli se ve Vašem souboru nenachází duplikát.<br/><u>Poznámka :</u><br/>Tato volba vyžaduje plnou kontrolu syntakce a analýzu zdrojového souboru, toto je doporučené použít při aktivování vyrovnávací paměti',
	'GOOGLE_XML_FORCE_LASTMOD' => 'Poslední modifikace',
	'GOOGLE_XML_FORCE_LASTMOD_EXPLAIN' => 'Zde můžete vidět kdy byla vydaná poslední modifikace ( i když není vyrovnávací paměť aktivní ) Všechny adresy ve vaší sitemaps nebudou změněny, jelikož tento modul spočítá veškeré priority všech Vašich URL.<br/><u>Poznámka :</u><br/>Tato volba vyžaduje plnou kontrolu syntakce a analýzu zdrojového souboru, toto je doporučené použít při aktivování vyrovnávací paměti',
	'GOOGLE_XML_FORCE_LIMIT' => 'Nutně omezit',
	'GOOGLE_XML_FORCE_LIMIT_EXPLAIN' => 'Zde se můžete ujistit že není překročená maximální povolená hodnota množství URL ve Vašem souboru sitemaps..<br/><u>Poznámka :</u><br/>Tato volba vyžaduje plnou kontrolu syntakce a analýzu zdrojového souboru, toto je doporučené použít při aktivování vyrovnávací paměti.',
	// Reset settings
	'GOOGLE_XML_RESET' => 'XML Sitemaps Moduly',
	'GOOGLE_XML_RESET_EXPLAIN' => 'Obnovit všechny výchozí hodnoty třídícího mechanizmu v XML Sitemaps modulu.',
	'GOOGLE_XML_MAIN_RESET' => 'Nastavení XML sitemaps',
	'GOOGLE_XML_MAIN_RESET_EXPLAIN' => 'Obnovit všechny výchozí nastavení všech možností v "XML Sitemaps nastaveních" na hlavní kartě XML Sitemaps modulu.',
	'GOOGLE_XML_CACHE_RESET' => 'Vyrovnávací pamět XML sitemaps',
	'GOOGLE_XML_CACHE_RESET_EXPLAIN' => 'Obnovit všechny výchozí nastavení všech možností ukládání do vyrovnávací paměti na XML Sitemap modulu.',
	'GOOGLE_XML_GZIP_RESET' => 'XML Sitemap Gunzip',
	'GOOGLE_XML_GZIP_RESET_EXPLAIN' => 'Obnovit všechny výchozí nastavení všech možností gunzip v XML Sitemap modulu.',
	'GOOGLE_XML_LIMIT_RESET' => 'Limit XML Sitemap',
	'GOOGLE_XML_LIMIT_RESET_EXPLAIN' => 'Obnovit na výchozí hodnoty všech mezních možností v XML Sitemap modulu.',
));
?>