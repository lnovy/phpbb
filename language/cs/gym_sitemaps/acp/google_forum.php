<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $id:$
* @copyright (c) 2006 - 2008 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* google_forum [English]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'GOOGLE_FORUM' => 'Sitemap fóra',
	'GOOGLE_FORUM_EXPLAIN' => 'Toto je nastavení sitemap pro vyhledávač Google<br/>Některá jsou nahrazena v závislosti na Google Sitemap a nastavením, které je udávané jako hlavní a tím pádem se přepíše veškeré nastavení.',
	'GOOGLE_FORUM_SETTINGS' => 'Nastavení Sitemap pro fórum. ',
	'GOOGLE_FORUM_SETTINGS_EXPLAIN' => 'Následující nastaveni jsou specifické pro moduly Google Sitemap.',
	'GOOGLE_FORUM_STICKY_PRIORITY' => 'Priority',
	'GOOGLE_FORUM_STICKY_PRIORITY_EXPLAIN' => 'U daných priorit musí být hodnota udaná mezi -  0.0 & 1.0',
	'GOOGLE_FORUM_ANNOUCE_PRIORITY' => 'Hlavni priorita',
	'GOOGLE_FORUM_ANNOUCE_PRIORITY_EXPLAIN' => 'Hlavni priorita musí mít hodnoty mezi - 0.0 & 1.0.',
	'GOOGLE_FORUM_GLOBAL_PRIORITY' => 'Globální oznámení priorit',
	'GOOGLE_FORUM_GLOBAL_PRIORITY_EXPLAIN' => 'Globální priority musí mít hodnotu mezi - 0.0 & 1.0',
	'GOOGLE_FORUM_EXCLUDE' => 'Vyloučení fór',
	'GOOGLE_FORUM_EXCLUDE_EXPLAIN' => 'Zde můžete vyloučit jedno, nebo několik fór ze zobrazování v Sitemap<br /><u>Poznámka:</u> Pokud toto pole ponecháte prázdné, budou všechny sitemapy vypsány na všech veřejných fórech.',
	// Reset settings
	'GOOGLE_FORUM_RESET' => 'Moduly sitemap pro fóra',
	'GOOGLE_FORUM_RESET_EXPLAIN' => 'Reset všech modulů pro všechny fóra na výchozí hodnoty.',
	'GOOGLE_FORUM_MAIN_RESET' => 'Hlavní sitemapy fóra',
	'GOOGLE_FORUM_MAIN_RESET_EXPLAIN' => 'Obnovit výchozí nastavení všech možností ve "Fórum sitemap" Na kartě fór, všechny moduly budou nastavené na hlavní tudíž výchozí hodnoty.',
	'GOOGLE_FORUM_CACHE_RESET' => 'Vyrovnávací paměť sitemap',
	'GOOGLE_FORUM_CACHE_RESET_EXPLAIN' => 'Obnoví výchozí nastavení vyrovnávací paměti všech sitemap modulů.',
	'GOOGLE_FORUM_MODREWRITE_RESET' => 'Přepisování URL u všech sitemap',
	'GOOGLE_FORUM_MODREWRITE_RESET_EXPLAIN' => 'Obnoví výchozí nastavení u všech přepisovatelnych URL v sitemap modulech.',
	'GOOGLE_FORUM_GZIP_RESET' => 'Sitemap Gunzip',
	'GOOGLE_FORUM_GZIP_RESET_EXPLAIN' => 'Obnoví výchozí nastavení u všech gunzip modulů.',
	'GOOGLE_FORUM_LIMIT_RESET' => 'Sitemap limity',
	'GOOGLE_FORUM_LIMIT_RESET_EXPLAIN' => 'Obnoví všechny výchozí hodnoty u všech mezních možností sitemap modulů.',
	'GOOGLE_FORUM_SORT_RESET' => 'Řazení sitemap ve fórech.',
	'GOOGLE_FORUM_SORT_RESET_EXPLAIN' => 'Obnoví všechny výchozí hodnoty řazených sitemap modulů.',
	'GOOGLE_FORUM_PAGINATION_RESET' => 'Stránkování sitemap u fór.',
	'GOOGLE_FORUM_PAGINATION_RESET_EXPLAIN' => 'Obnoví výchozí hodnoty u všech stránkovaných sitemap modulů.',
));
?>