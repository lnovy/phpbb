<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $id: gym_rss.php - 11039 11-20-2008 14:38:27 - 2.0.RC1 dcz $
* @copyright (c) 2006 - 2008 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* gym_rss [English]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'RSS_MAIN' => 'RSS Feeds',
	'RSS_MAIN_EXPLAIN' => 'Toto jsou hlavní nastavení pro modul RSS feeds.<br/>Mohou být použity na všechny RSS moduly v závislosti na vašem vynuceném nastavení RSS.',
	// Reset settings
	'RSS_ALL_RESET' => 'Všechny RSS moduly',
	// Limits
	'RSS_LIMIT_GEN' => 'Hlavní limity',
	'RSS_LIMIT_SPEC' => 'Limity RSS',
	'RSS_URL_LIMIT_LONG' => 'Max. limit Feeds ',
	'RSS_URL_LIMIT_LONG_EXPLAIN' => 'Number of items displayed in a Long feed without content, requires Allow Long Feeds option activated.',
	'RSS_SQL_LIMIT_LONG' => 'Long SQL cycle',
	'RSS_SQL_LIMIT_LONG_EXPLAIN' => 'Number of items queried at a time for a long feed without content.',
	'RSS_URL_LIMIT_SHORT' => 'Short Feeds limit',
	'RSS_URL_LIMIT_SHORT_EXPLAIN' => 'Number of items displayed on a Short feed without content, requires Allow Short Feeds option activated.',
	'RSS_SQL_LIMIT_SHORT' => 'Short SQL cycle',
	'RSS_SQL_LIMIT_SHORT_EXPLAIN' => 'Number of items queried at a time for a Short feed without content.',
	'RSS_URL_LIMIT_MSG' => 'Default limit with content',
	'RSS_URL_LIMIT_MSG_EXPLAIN' => 'Number of items displayed by default in feeds with content, requires Allow Item Content option activated.',
	'RSS_SQL_LIMIT_MSG' => 'SQL cycle with content',
	'RSS_SQL_LIMIT_MSG_EXPLAIN' => 'Number of items queried at a time for a feed with content.',
	// Basic settings
	'RSS_SETTINGS' => 'Základní nastavení',
	'RSS_C_INFO' => 'Copyright',
	'RSS_C_INFO_EXPLAIN' => 'Copyright informace o zobrazených datech v RSS feeds. Výchozí je název fóra.',
	'RSS_SITENAME' => 'Název site',
	'RSS_SITENAME_EXPLAIN' => 'Jméno site, který bude zobrazen v RSS feeds. Výchozí je název fóra.',
	'RSS_SITE_DESC' => 'Popis site',
	'RSS_SITE_DESC_EXPLAIN' => 'Popis site, který který bude zobrazen v RSS feeds. Výchozí je popis fóra.',
	'RSS_LOGO_URL' => 'Logo site',
	'RSS_LOGO_URL_EXPLAIN' => 'Obrázek, který bude použit jako logo v RSS feeds, umístěné v adresáři gym_sitemaps/images/.',
	'RSS_IMAGE_URL' => 'Logo RSS',
	'RSS_IMAGE_URL_EXPLAIN' => 'Obrázek, který bude použit jako logo pro RSS v RSS feeds, umístěné v adresáři gym_sitemaps/images/.',
	'RSS_LANG' => 'Jazyk RSS',
	'RSS_LANG_EXPLAIN' => 'Jazyk, který bude definován jako hlavní pro RSS feeds. Výchozím je výchozí jazyk fóra.',
	'RSS_URL' => 'URL RSS feed',
	'RSS_URL_EXPLAIN' => 'Vložte celou URL k souboru gymrss.php, příklad http://www.example.com/eventual_dir/ pokud je gymrss.php nainstalován v http://www.example.com/eventual_dir/.<br/>Tato možnost je užitečná v případě pokud soubor gymrss.php si přejete mít v jiném adresáři než je nainstalováno phpBB.',
	// Auth settings
	'RSS_AUTH_SETTINGS' => 'Nastavení oprávnění',
	'RSS_ALLOW_AUTH' => 'Oprávnění',
	'RSS_ALLOW_AUTH_EXPLAIN' => 'Povolit oprávnění pro RSS feeds. Pokud bude aktivováno, přihlášení uživatelé budou moci vidět položky, které nejsou veřejnosti normálně dostupné.',
	'RSS_CACHE_AUTH' => 'Cache private feeds',
	'RSS_CACHE_AUTH_EXPLAIN' => 'You can disable cache for non public feeds when allowed.<br/> Caching private feeds will increase the number of file cached;  it should not be a problem, but you can decide to only cache public feeds here.',
	'RSS_NEWS_UPDATE' => 'Aktualizace novinek Feeds',
	'RSS_NEWS_UPDATE_EXPLAIN' => 'Aktualizace položek bude provedena po x hodinách. Nechte 0 pro použití nastavení obnovy cache, při které dojde k aktualizaci.',
	'RSS_ALLOW_NEWS' => 'Povolit novinky Feeds',
	'RSS_ALLOW_NEWS_EXPLAIN' => 'Jedná se o další MOD, kde jsou zobrazeny novinky. Jedná se o doplněk, který nebude kolidovat s ostatními moduly. Je to užitečné, pokud chcete použít Google News.',
	'RSS_ALLOW_SHORT' => 'Povolit krátké Feeds',
	'RSS_ALLOW_SHORT_EXPLAIN' => 'Povolit použití krátkých RSS feeds.',
	'RSS_ALLOW_LONG' => 'Povolit dlouhé Feeds',
	'RSS_ALLOW_LONG_EXPLAIN' => 'Povolit použití dlouhých RSS feeds.',
	// Notifications
	'RSS_NOTIFY' => 'Upozornění',
	'RSS_YAHOO_NOTIFY' => 'Upozornění Yahoo',
	'RSS_YAHOO_NOTIFY_EXPLAIN' => 'Povolení upozornění Yahoo! pro RSS feeds.<br/>Netýká se obecného feeds (RSS.xml).<br/>Pokaždé pokud je aktualizována cache je odeslána informace o aktualizaci na Yahoo!<br/><u>Poznámka:</u>MUSÍTE vložit Yahoo! AppID níže aby bylo upozornění odesláno.',
	'RSS_YAHOO_APPID' => 'Yahoo! AppID',
	'RSS_YAHOO_APPID_EXPLAIN' => 'Vložte vaše Yahoo! AppID. Pokud nemáte, navštivte <a href="http://api.search.yahoo.com/webservices/register_application">tuto stránku</a>.<br/><u>Poznámka:</u>Měli byste mít vytvořený účet u Yahoo! předtím než si vytvoříte Yahoo! AppID.',
	// Styling
	'RSS_STYLE' => 'Vzhled RSS',
	'RSS_XSLT' => 'Použití XSLT',
	'RSS_XSLT_EXPLAIN' => 'RSS feeds mohou být stylovány použitím <a href="http://www.w3schools.com/xsl/xsl_transformation.asp">XSL-Transform</a> Style Sheet.',
	'RSS_FORCE_XSLT' => 'Použít styly',
	'RSS_FORCE_XSLT_EXPLAIN' => 'Není to trochu hloupé, je zapotřebí trik aby bylo povoleno XLST v prohlížečích. Je potřeba vložit pár mezer na začátek výstupního XML souboru.<br/>FF 2 a IE7 rozhodují pouze podle prvních 500 znaků jestli je použit styl, jinak použijí vlastní',
	'RSS_LOAD_PHPBB_CSS' => 'Načíst phpBB CSS',
	'RSS_LOAD_PHPBB_CSS_EXPLAIN' => 'GYM sitemap plně využívají systém šablon phpBB3. XSL stylesheet je použit na vygenerování kompatibilního výstupu s phpBB.<btr/>Touto možností rozhodnete použít phpBB CSS na XSL stylesheet místo základního. Tímto způsobem bude použito vlastní nastavení stylu pro RSS.<br/>Bude použito pouze v případě pokud bude promazána cache v "údržbě".<br/>Pokud není RSS CSS soubor v používaném stylu, základní styl (vždy dostupný, založen na prosilver) bude použit.<br/>Nesnažte se používat prosilver šablony s jiným stylem, CSS většinou nepasuje.',
	// Content
	'RSS_CONTENT' => 'Obsah',
	'RSS_CONTENT_EXPLAIN' => 'Zde můžete nastavit různé možnosti formátování a filtrování obsahu.<br />Mohou být použity na všechny RSS moduly v závislosti na vašem vynuceném nastavení RSS.',
	'RSS_ALLOW_CONTENT' => 'Povolit obsah položky',
	'RSS_ALLOW_CONTENT_EXPLAIN' => 'Obsha zprávy bude zobrazen plně nebo částečně v RSS feeds.<br/><u>Poznámka:</u>Trochu zatíží server. Limity pro výstup obsahu mohou tuto zátěž snížit.',
	'RSS_SUMARIZE' => 'Výběr položek',
	'RSS_SUMARIZE_EXPLAIN' => 'Sumarizace obsahu zprávy pro feeds.<br/>Maximální počet vět, slov nebo znaků odpovídající nastavení níže. 0 pro zobrazení všeho.',
	'RSS_SUMARIZE_METHOD' => 'Metoda výběru',
	'RSS_SUMARIZE_METHOD_EXPLAIN' => 'Výběr ze tří možností zobrazení výběru.<br/> Počet řádek, slov nebo znaků. Značky BBcode nebudou poškozeny.',
	'RSS_ALLOW_PROFILE' => 'Zobrazit profily',
	'RSS_ALLOW_PROFILE_EXPLAIN' => 'Autor tématu bude zobrazen v RSS feed.',
	'RSS_ALLOW_PROFILE_LINKS' => 'Link profilu',
	'RSS_ALLOW_PROFILE_LINKS_EXPLAIN' => 'Pokud je autor zobrazen v příspěvku, můžete se rozhodnout bude li uveden odkaz na profil autora.',
	'RSS_ALLOW_BBCODE' => 'Povolit BBcodes',
	'RSS_ALLOW_BBCODE_EXPLAIN' => 'Ve výstupu bude použito BBCode.',
	'RSS_STRIP_BBCODE' => 'Vyjmout BBcodes',
	'RSS_STRIP_BBCODE_EXPLAIN' => 'Můžete nastavit, které BBCode nebudou použity.<br/>Formát:<br/><ul><li><u>Čárkou oddělený seznam bbcodů:</u> Odstranění bbcode, zachová obsah.<br/><u>Příklad:</u> <b>img,b,quote</b><br/>V tomto příkladu nebudou bbcode img, bold a quote bbcode parsovány, značky bbcode budou odstraněny při zachování obsahu.</li><li><u>Čárkou oddělený seznam s možností výmazu:</u> Rozhodnout o použití bbcode.<br /><u>Příklad:</u> <b>img:1,b:0,quote,code:1</b> <br/> BBcode img bude celý smazán, b - místo tučného písma bude pouze normální, quote - bude zachován pouze obsah, code - bude odstraněno včetně obsahu.</li></ul>',
	'RSS_ALLOW_LINKS' => 'Povolit aktivní odkazy',
	'RSS_ALLOW_LINKS_EXPLAIN' => 'Vybrat pokud si přejete aktivní odkazy v obsahu.<br/>Při neaktivní volbě, budou v obsahu zobrazeny linky a emaily bez možnosti odkazu.',
	'RSS_ALLOW_EMAILS' => 'Povolit Emaily',
	'RSS_ALLOW_EMAILS_EXPLAIN' => 'Vybrat pokud chcete email ve tvaru "email AT domain DOT com" místo "email@domain.com" v obsahu položek.',
	'RSS_ALLOW_SMILIES' => 'Povolit smajlíky',
	'RSS_ALLOW_SMILIES_EXPLAIN' => 'Vyberte pokud chcete zobrazit smajlíky v obsahu.',
	// Old URL handling
	'RSS_1XREDIR' => 'Handle GYM 1x rewriten URL',
	'RSS_1XREDIR_EXPLAIN' => 'Aktivace the GYM 1x rewriten URLs detection. The module will display a custom feed providing with the new URL of the requested feed.<br/><u>Note :</u><br/>This option requires the compatibility rewriterules as explained in the install file.',
));
?>