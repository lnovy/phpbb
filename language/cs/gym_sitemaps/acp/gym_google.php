<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $id: gym_google.php - 3646 11-26-2008 11:16:36 - 2.0.RC2 dcz $
* @copyright (c) 2006 - 2008 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* gym_google [English]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'GOOGLE_MAIN' => 'Nastavení Google sitemap',
	'GOOGLE_MAIN_EXPLAIN' => 'Hlavní nastavení pro Google sitemap moduly <br/>Vše se bude vztahovat na všechny služby Google sitemap ve výchozím nastavení.',
	// Reset settings
	'GOOGLE_ALL_RESET' => '<b>All</b> Moduly Google sitemap',
	'GOOGLE_URL' => 'URL sitemap Googlu',
	'GOOGLE_URL_EXPLAIN' => 'Zadejte úplnou adresu vaší sitemapy např. http://www.example.com/eventual_dir/ sitemap.php pokud je instalována v http://www.example.com/eventual_dir/. <br/> Tato volba je užitečná, když phpBB není instalován ve hlavním adresáři Vaší domény a chtěli byste mít seznam Vašich URL v hlavní úrovni Google sitemap.',
	'GOOGLE_PING' => 'Google Volání',
	'GOOGLE_PING_EXPLAIN' => 'Zavolá na Google pokaždé když se aktualizují sitemapy.',
	'GOOGLE_THRESHOLD' => 'Hranice sitemap',
	'GOOGLE_THRESHOLD_EXPLAIN' => 'Minimální množství položek při které se zobrazí sitemaps u fór.Při zadání hodnoty „1“ to znamená že se mapa zobrazí při překročení tohoto čísla.Příklad: chci aby se mi zobrazovala sitemapa u fora „A“ při překročení pěti příspěvků, tak zde musím nastavit hodnotu na „6“',
	'GOOGLE_PRIORITIES' => 'Prioritní nastavení',
	'GOOGLE_DEFAULT_PRIORITY' => 'Výchozí Priority',
	'GOOGLE_DEFAULT_PRIORITY_EXPLAIN' => 'Výchozí priorita pro URL adresy uvedené ve všech Sitemapách,budou použity pouze ty možnosti kde jsou nastaveny hodnoty mezi 0,0 & 1,0., ve všech modulech',
	'GOOGLE_XSLT' => 'XSLT Styli',
	'GOOGLE_XSLT_EXPLAIN' => 'Aktivuje XSL style-sheet pro výstup uživatelsky přívětivé Google sitemap s klikatelnými odkazy aj. Toto nastavení se projeví až poté, co v menu "Údržba" pročistíte cache.',
	'GOOGLE_LOAD_PHPBB_CSS' => 'Založte phpBB CSS',
	'GOOGLE_LOAD_PHPBB_CSS_EXPLAIN' => 'Modul GYM sitemap využívá šablonovací systém phpBB3. XSL stylesheet používáné k vybudování html výstupu jsou s phpBB3 kompatibilní.<br/> Díky tomuto můžete na XSL stylesheet aplikovat kaskádové styly phpBB namísto defaultně předvolených. Všechny změny, jako třeba změna pozadí, barvy fontu, či obrázků, budou následně použity ve výstupu Google sitemap. Změny se však projeví až po pročištění cache RSS v menu "Údržba".<br/> Pokud nebudou v daném stylu k dispozici soubory stylu Google sitemap, bude použit defaultní styl (vždy přítomný, založený na prosilveru).<br/>Nezkoušejte použít prosilver šablony s jiným stylem, kaskádové styly pravděpodobně nebudou odpovídat.',
));
?>