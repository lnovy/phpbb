<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $id: rss_forum.php - 3886 11-20-2008 14:38:27 - 2.0.RC1 dcz $
* @copyright (c) 2006 - 2008 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
/**
*
* rss_forum [English]
*
*/
/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
$lang = array_merge($lang, array(
	'RSS_FORUM' => 'Forum RSS moduly',
	'RSS_FORUM_EXPLAIN' => 'Jedná se o nastavení vašeho fóra, RSS kanálů všech modulu.<br/> Některé z nich mohou být převážně v závislosti na RSS a proto se přepíšou nastavení.',
	'RSS_FORUM_EXCLUDE' => 'Vyloučeni z Vašeho fóra',
	'RSS_FORUM_EXCLUDE_EXPLAIN' => 'Zde můžete vyloučit jedno nebo několik fór z výpisu RSS.<br /><u>Poznámka :</u> Pokud je toto pole ponecháno prázdné, všechny snímatelné fóra budou vypsány.',
	// Content
	'RSS_FORUM_CONTENT' => 'Obsah fóra, Nastavení',
	'RSS_FORUM_FIRST' => 'První zpráva',
	'RSS_FORUM_FIRST_EXPLAIN' => 'Zde je vidět jestli je, či není téma uvedené v RSS kanále.<br/> Ve výchozím nastavení je pouze poslední post a každé vlákno je uvedeno zvlášť. Zobrazí se pouze první post, toto znamená větší zátěž pro Váš server.',
	'RSS_FORUM_LAST' => 'Poslední zpráva',
	'RSS_FORUM_LAST_EXPLAIN' => 'Zobrazení posledních, nebo předešlých zpráv v kanálech RSS.<br/>  Ve výchozím nastavení je zobrazen pouze poslední post, každé vlákno je uvedené zvlášť. Tato volba je užitečná, jestliže chcete zobrazit pouze první URL v tématu v RSS.<br/>Please note: Setting First message to YES and last message to NO is the same as building a news feed.',
	'RSS_FORUM_RULES' => 'Zobrazit pravidla fóra.',
	'RSS_FORUM_RULES_EXPLAIN' => 'Zobrazení pravidel fóra v RSS.',
	// Reset settings
	'RSS_FORUM_RESET' => 'RSS modul fóra.',
	'RSS_FORUM_RESET_EXPLAIN' => 'Resetuje všechny RSS moduly na výchozí hodnoty.',
	'RSS_FORUM_MAIN_RESET' => 'Hlavní RSS kanál fóra.',
	'RSS_FORUM_MAIN_RESET_EXPLAIN' => 'Obnovit výchozí nastavení všech možností v RSS na kartě modulu RSS.',
	'RSS_FORUM_CONTENT_RESET' => 'Obsah RSS kanálu fóra.',
	'RSS_FORUM_CONTENT_RESET_EXPLAIN' => 'Obnovit výchozí nastavení všech možností RSS kanálu na obsahu fóra.',
	'RSS_FORUM_CACHE_RESET' => 'Vyrovnávací paměť RSS.',
	'RSS_FORUM_CACHE_RESET_EXPLAIN' => 'Obnovit výchozí nastavení všech možností ukládání do vyrovnávací paměti RSS modulu.',
	'RSS_FORUM_MODREWRITE_RESET' => 'Přepis všech RSS URL fóra.',
	'RSS_FORUM_MODREWRITE_RESET_EXPLAIN' => 'Obnoví výchozí hodnoty všech URL v RSS modulech.',
	'RSS_FORUM_GZIP_RESET' => 'RSS fóra Gunzip.',
	'RSS_FORUM_GZIP_RESET_EXPLAIN' => 'Obnovit výchozí nastavení všech možností nastavení gunzip v RSS modulech.',
	'RSS_FORUM_LIMIT_RESET' => 'Limity RSS fóra.',
	'RSS_FORUM_LIMIT_RESET_EXPLAIN' => 'Obnoví všechny mezní hodnoty ve všech RSS modulech.',
	'RSS_FORUM_SORT_RESET' => 'Třídění RSS kanálu fóra.',
	'RSS_FORUM_SORT_RESET_EXPLAIN' => 'Obnoví všechny výchozí hodnoty všech možností třídění RSS kanálu ve všech modulech fóra.',
	'RSS_FORUM_PAGINATION_RESET' => 'Stránkování všech RSS kanálu fóra.',
	'RSS_FORUM_PAGINATION_RESET_EXPLAIN' => 'Obnoví všechny výchozí hodnoty všech možností stránkování ve všech modulech RSS.',
));
?>