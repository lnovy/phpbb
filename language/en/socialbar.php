<?php
/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
   exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'LOGIN_EXPLAIN_SOCIALBAR'       => 'Pro použití Socialbaru musíte být zaregistrováni a příhlášeni',

  'acl_u_socialbar'               => array('lang' => 'Smí použít Socialbar', 'cat' => 'misc'),
	));

?>
