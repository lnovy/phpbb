<?php
/*
*
* language/en/acp/th23_autolinks.php
*
* @package th23_autolinks
* @author Thorsten Hartmann (www.th23.net)
* @copyright (c) 2008 by Thorsten Hartmann (www.th23.net)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	
	'ACP_TH23_AUTOLINKS' => 'th23 Autolinks',
	'ACP_TH23_AUTOLINKS_SETTINGS' => 'Settings',
	'ACP_TH23_AUTOLINKS_SETTINGS_EXPLAIN' => 'th23 Autolinks allows you to specify words and related URLs, e.g. "th23 Autolinks" -> "http://www.th23.net/". When these words are used within postings they are automatically converted to links. <strong>On this page you can define the main setting of th23 Autolinks</strong>, e.g. activate it, specify title and CSS class to be used for the automatically added links.',
	'ACP_TH23_AUTOLINKS_URLS' => 'URLs',
	'ACP_TH23_AUTOLINKS_URLS_EXPLAIN' => 'th23 Autolinks allows you to specify words and related URLs, e.g. "th23 Autolinks" -> "http://www.th23.net/". When these words are used within postings they are automatically converted to links. <strong>On this page you can define the words and related URLs</strong>, that should be automatically added.',

	'ACP_TH23_AUTOLINKS_STATUS' => 'Status',
	'ACP_TH23_AUTOLINKS_ENABLE' => 'Status of th23 Autolinks',
	'ACP_TH23_AUTOLINKS_ENABLE_EXPLAIN' => 'Disabling will prevent this MOD from converting specified words to links',
	'ACP_TH23_AUTOLINKS_ACTIONS' => 'Actions',
	'ACP_TH23_AUTOLINKS_POSTS' => 'Add th23 Autolinks in posts',
	'ACP_TH23_AUTOLINKS_POSTS_EXPLAIN' => 'Enable to add links in topics',
	'ACP_TH23_AUTOLINKS_FORUM' => 'Add autolinks in forums',
	'ACP_TH23_AUTOLINKS_FORUM_EXPLAIN' => 'Specify in which forums autolinks should be applied to user posts<br />Select "enable" or "disbale" and choose the forums you want to enable / disable autolinking from the list<br />For multiple choices hold the control key ("Ctrl") pressed',
	'ACP_TH23_AUTOLINKS_FORUM_ALL' => 'All',
	'ACP_TH23_AUTOLINKS_FORUM_ALLOW' => 'Enable',
	'ACP_TH23_AUTOLINKS_FORUM_DENY' => 'Disable',
	'ACP_TH23_AUTOLINKS_PMS' => 'Add th23 Autolinks in private messages',
	'ACP_TH23_AUTOLINKS_PMS_EXPLAIN' => 'Enable to add links in private messages',
	'ACP_TH23_AUTOLINKS_PREVIEW' => 'Add th23 Autolinks in preview',
	'ACP_TH23_AUTOLINKS_PREVIEW_EXPLAIN' => 'Enable to add links already in previews (posts and private messages)',
	'ACP_TH23_AUTOLINKS_CODE' => 'Add th23 Autolinks within <em>code</em> BBcode',
	'ACP_TH23_AUTOLINKS_CODE_EXPLAIN' => 'Enable to add links within [code][/code] BBcode tags',
	'ACP_TH23_AUTOLINKS_USERNAMES' => 'User names',
	'ACP_TH23_AUTOLINKS_USER' => 'Automatically link user names',
	'ACP_TH23_AUTOLINKS_USER_EXPLAIN' => 'Each registered user name is automatically linked to the respective profile',
	'ACP_TH23_AUTOLINKS_USER_EXPIRE' => 'Refresh cache of user names to link after',
	'ACP_TH23_AUTOLINKS_USER_EXPIRE_MINUTE' => '1 minute',
	'ACP_TH23_AUTOLINKS_USER_EXPIRE_HOUR' => '1 hour',
	'ACP_TH23_AUTOLINKS_USER_EXPIRE_DAY' => '1 day',
	'ACP_TH23_AUTOLINKS_USER_EXPIRE_WEEK' => '1 week',
	'ACP_TH23_AUTOLINKS_USER_EXPIRE_EXPLAIN' => 'User names are cached to increase performance, this cache will be updated as specified here',
	'ACP_TH23_AUTOLINKS_APPEARANCE' => 'Appearance',
	'ACP_TH23_AUTOLINKS_TITLE' => 'Title for th23 Autolinks',
	'ACP_TH23_AUTOLINKS_TITLE_EXPLAIN' => 'Allows you to specify a title/caption for every automatically added link, e.g. to show users, that not the poster included this link',
	'ACP_TH23_AUTOLINKS_CSS' => 'CSS class for th23 Autolinks',
	'ACP_TH23_AUTOLINKS_CSS_EXPLAIN' => 'Allows you to style the automatically added links differently from "normal" ones<br/>Note: According CSS class has to be added to your sites CSS file manually!',

	'ACP_TH23_AUTOLINKS_NEW' => 'New',
	'ACP_TH23_AUTOLINKS_NEW_EXPLAIN' => 'Specifiy the word to be linked and the respective URL:',
	'ACP_TH23_AUTOLINKS_EDIT' => 'Edit',
	'ACP_TH23_AUTOLINKS_EDIT_EXPLAIN' => 'Modify the word to be linked and/or the respective URL:',
	'ACP_TH23_AUTOLINKS_DELETE' => 'Delete',
	'ACP_TH23_AUTOLINKS_DELETE_EXPLAIN' => 'To delete the word/link below, confirm by pressing "Delete"',
	'ACP_TH23_AUTOLINKS_SORT' => 'Position',
	'ACP_TH23_AUTOLINKS_WORD' => 'Word',
	'ACP_TH23_AUTOLINKS_WORD_EXPLAIN' => 'Word/phrase that should be autolinked, e.g. "th23 autolinks"<br />Comparison is case <strong>in</strong>sensitive, only <strong>whole word/phrase</strong> is linked (e.g. "to" within "today" will not be linked), single " " (white-space) and containing "th23autolink" is <strong>not</strong> allowed',
	'ACP_TH23_AUTOLINKS_URL' => 'URL',
	'ACP_TH23_AUTOLINKS_URL_EXPLAIN' => 'URL that should be linked to, e.g. "http://www.domain.net/"<br />URLs <strong>not</strong> starting with "http://" are considered local and will be prefixed with phpbb root path and appended by session id, phrase "th23autolink" is <strong>not</strong> allowed in URL',
	'ACP_TH23_AUTOLINKS_NO_ITEMS' => 'No items to display',
	'ACP_TH23_AUTOLINKS_NO_VALID_ITEM' => 'No valid item specified',
	'ACP_TH23_AUTOLINKS_NO_VALID_MOVE' => 'No valid move - either item is already in first or last place',
	'ACP_TH23_AUTOLINKS_NOT_ALLOWED_CHARACTER' => 'Word contains one or more not allowed character(s).<br />The characters "[", "]", "/" are not allowed within the word.',
	'ACP_TH23_AUTOLINKS_NOT_ALLOWED_TH23AUTOLINK' => 'Word or URL contains not allowed phrase "th23autolink".',
	'ACP_TH23_AUTOLINKS_NO_WORD' => 'No word specified',
	'ACP_TH23_AUTOLINKS_SAVE_SUCCESS' => 'The word/link has been saved successfully',
	'ACP_TH23_AUTOLINKS_SAVE_NO_SUCCESS' => 'The word/link could not be saved',
	'ACP_TH23_AUTOLINKS_DELETE_SUCCESS' => 'The word/link has been deleted successfully',
	'ACP_TH23_AUTOLINKS_DELETE_NO_SUCCESS' => 'The word/link could not be deleted',
	
));

?>