<?php
define('IN_PHPBB', true);
$phpEx = substr(strrchr(__FILE__, '.'), 1);
$phpbb_root_path = './';
include($phpbb_root_path . 'common.' . $phpEx);
// Start session management
$user->session_begin();
$auth->acl($user->data);

if (!$user->data['is_registered'])
{
    // TODO: Add better lang string here
    trigger_error('NOT_AUTHORISED');
}

$template->assign_var('S_SIMPLE_MESSAGE', true);
trigger_error('Vaše ID (variabilní symbol pro platbu členského příspěvku) je ' . $user->data['user_id']);

?>
