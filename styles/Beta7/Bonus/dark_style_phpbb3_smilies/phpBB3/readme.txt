Title: [Release] 'Dark Style' phpBB3 Smilies
Download Link: http://localbikers.org.uk/phpbb/dark_style_phpbb3_smilies/dark_style_phpbb3_smilies.zip
Release Topic: http://www.phpbb.com/community/viewtopic.php?f=74&t=898525

This ZIP includes the current phpBB3 smilies which have been altered slightly in order to display correctly on darker styles (removes the white pixels from around the edges).

Compatability: 3.0.1 or above. [*]

Notes: Simply upload to your 'phpBB3/images/smilies' directory, overwriting the existing smilies. You may need to refresh your browser cache for the changes to appear. [*] If uploading to a prior version, you'll need to manually resize some of the smilies in the ACP to suit the new dimensions.

Please use the release topic link above to obtain support.

Enjoy!
camm15h