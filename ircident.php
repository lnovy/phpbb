<?php
/**
*
* @package phpBB SEO GYM Sitemaps
* @version $Id: sitemap.php 112 2009-09-30 17:21:34Z dcz $
* @copyright (c) 2006 - 2009 www.phpbb-seo.com
* @license http://opensource.org/osi3.0/licenses/lgpl-license.php GNU Lesser General Public License
*
*/
define('IN_PHPBB', true);
$phpEx = substr(strrchr(__FILE__, '.'), 1);
$phpbb_root_path = './';
include($phpbb_root_path . 'common.' . $phpEx);
// Start session management
$user->session_begin();
$auth->acl($user->data);

$nick = $_GET['nick'];
$ticket = $_GET['ticket'];
$username = strtolower($user->data['username_clean']);
$username = str_replace(' ', '.', $username);

file_put_contents("store/ircident", $nick . "|" . $ticket . "|" . $user->data['username'] . "|" . $username . "|". PHP_EOL,  FILE_APPEND | LOCK_EX);
exit;
?>
