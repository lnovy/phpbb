User-agent: *
Disallow: /ucp.php
Disallow: /faq.php
Disallow: /search.php
Disallow: /?abrakadabra=magic
Disallow: /posting.php

Disallow: /harming/humans
Disallow: /ignoring/human/orders
Disallow: /harm/to/self

Allow: /
