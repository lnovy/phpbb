<?php
/*
*
* includes/th23_autolinks.php
*
* @package th23_autolinks
* @author Thorsten Hartmann (www.th23.net)
* @copyright (c) 2008 by Thorsten Hartmann (www.th23.net)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

if(!defined('IN_PHPBB'))
{
	exit;
}

// obtain th23_autolinks (if possible from cache)
if (!isset($th23_autolinks) || !is_array($th23_autolinks))
{
	$th23_autolinks = $cache->obtain_th23_autolinks();
}

// setup autolinks css/title
$th23_autolinks_css = ($config['th23_autolinks_css']) ? ' class="' . $config['th23_autolinks_css'] . '"' : '';
$th23_autolinks_title = ($config['th23_autolinks_title']) ? ' title="' . $config['th23_autolinks_title'] . '"' : '';

// prepare root path and session id for local links
$th23_autolinks_local = array(
	'search' => array(
		'th23autolink_root',
		'th23autolink_sid_and',
		'th23autolink_sid_que',
	),
	'replace' => array(
		$phpbb_root_path,
		(!empty($_SID)) ? '&amp;sid=' . $_SID : '',
		(!empty($_SID)) ? '?sid=' . $_SID : '',
	),
);

// ensure global availability of bbcode_uid for replacement callbacks
$th23_autolinks_uid = '';

// replace text with links in specified text
function th23_autolinks($text, $bbcode_uid)
{
	global $th23_autolinks, $th23_autolinks_uid, $user, $config;

	$th23_autolinks_uid = $bbcode_uid;

	// convert standard bbcodes tags to secure them from being linked
	$search = array(
		'[i:' . $th23_autolinks_uid . ']',
		'[/i:' . $th23_autolinks_uid . ']',
		'[u:' . $th23_autolinks_uid . ']',
		'[/u:' . $th23_autolinks_uid . ']',
		'[b:' . $th23_autolinks_uid . ']',
		'[/b:' . $th23_autolinks_uid . ']',
		'[*:' . $th23_autolinks_uid . ']',
		'[/*:m:' . $th23_autolinks_uid . ']',
		'[/quote:' . $th23_autolinks_uid . ']',
		'[/list:o:' . $th23_autolinks_uid . ']',
		'[/list:u:' . $th23_autolinks_uid . ']',
		'[/color' . $th23_autolinks_uid . ']',
		'[/size' . $th23_autolinks_uid . ']',
	);
	$replace = array(
		' th23autolink_bbcode_start_i_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_i_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_start_u_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_u_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_start_b_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_b_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_start_star_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_star_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_quote_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_list_o_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_list_u_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_color_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_size_' . $th23_autolinks_uid . ' ',
	);
	if ($config['th23_autolinks_code'])
	{
		$search[] = '[code:' . $th23_autolinks_uid . ']';
		$search[] = '[/code:' . $th23_autolinks_uid . ']';
		$replace[] = ' th23autolink_bbcode_start_code_' . $th23_autolinks_uid . ' ';
		$replace[] = ' th23autolink_bbcode_end_code_' . $th23_autolinks_uid . ' ';
	}
	$text = str_replace($search, $replace, $text);

	// encode img, url, flash, attachment, smilies (incl. start/end tag)
	// and encode start tag of quote, list, color, size
	$search = array(
		'/\[img:' . $th23_autolinks_uid . '\].*?\[\/img:' . $th23_autolinks_uid . '\]/i',
		'/\[url.*?:' . $th23_autolinks_uid . '\].*?\[\/url:' . $th23_autolinks_uid . '\]/i',
		'/\[flash.*?:' . $th23_autolinks_uid . '\].*?\[\/flash:' . $th23_autolinks_uid . '\]/i',
		'/\[attachment.*?:' . $th23_autolinks_uid . '\].*?\[\/attachment:' . $th23_autolinks_uid . '\]/i',
		'/(<!-- s:.*?: -->)<img.*?\/>\1/i',
		'/(<!-- m -->).*?\1/i',
		'/(<!-- l -->).*?\1/i',
		'/(<!-- w -->).*?\1/i',
		'/(<!-- e -->).*?\1/i',
		'/\[quote.*?:' . $th23_autolinks_uid . '\]/i',
		'/\[list.*?:' . $th23_autolinks_uid . '\]/i',
		'/\[color.*?:' . $th23_autolinks_uid . '\]/i',
		'/\[size.*?:' . $th23_autolinks_uid . '\]/i',
	);
	// optionally encode code (incl. start/end tag)
	if (!$config['th23_autolinks_code'])
	{
		$search[] = '/\[code:' . $th23_autolinks_uid . '\].*?\[\/code:' . $th23_autolinks_uid . '\]/i';
	}
	$text = preg_replace_callback($search, 'th23_autolinks_encode', $text);

	// find specified words in posts and replace it by an encoded link
	$text = preg_replace_callback($th23_autolinks['pattern'], 'th23_autolinks_encode_links', $text);

	// decode the encoded part
	do {
		$oldtext = $text;
		$text = preg_replace_callback('/\sth23autolink_encode_start_' . $th23_autolinks_uid . '(.*?)th23autolink_encode_end_' . $th23_autolinks_uid . '\s/i', 'th23_autolinks_decode', $text);
	} while ($oldtext != $text);

	// re-convert standard bbcodes tags
	$search = array(
		' th23autolink_bbcode_start_i_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_i_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_start_u_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_u_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_start_b_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_b_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_start_star_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_star_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_quote_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_list_o_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_list_u_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_color_' . $th23_autolinks_uid . ' ',
		' th23autolink_bbcode_end_size_' . $th23_autolinks_uid . ' ',
	);
	$replace = array(
		'[i:' . $th23_autolinks_uid . ']',
		'[/i:' . $th23_autolinks_uid . ']',
		'[u:' . $th23_autolinks_uid . ']',
		'[/u:' . $th23_autolinks_uid . ']',
		'[b:' . $th23_autolinks_uid . ']',
		'[/b:' . $th23_autolinks_uid . ']',
		'[*:' . $th23_autolinks_uid . ']',
		'[/*:m:' . $th23_autolinks_uid . ']',
		'[/quote:' . $th23_autolinks_uid . ']',
		'[/list:o:' . $th23_autolinks_uid . ']',
		'[/list:u:' . $th23_autolinks_uid . ']',
		'[/color' . $th23_autolinks_uid . ']',
		'[/size' . $th23_autolinks_uid . ']',
	);
	if ($config['th23_autolinks_code'])
	{
		$search[] = ' th23autolink_bbcode_start_code_' . $th23_autolinks_uid . ' ';
		$search[] = ' th23autolink_bbcode_end_code_' . $th23_autolinks_uid . ' ';
		$replace[] = '[code:' . $th23_autolinks_uid . ']';
		$replace[] = '[/code:' . $th23_autolinks_uid . ']';
	}
	$text = str_replace($search, $replace, $text);

	return $text;
}

// encode smilies to avoid matching them against the search words
function th23_autolinks_encode($match)
{
	global $th23_autolinks_uid;
	return ' th23autolink_encode_start_' . $th23_autolinks_uid . base64_encode($match[0]) . 'th23autolink_encode_end_' . $th23_autolinks_uid . ' ';
}

// encode link to avoid matching this part against the next specified word
function th23_autolinks_encode_links($match)
{
	global $th23_autolinks, $th23_autolinks_css, $th23_autolinks_title, $th23_autolinks_local, $th23_autolinks_uid;
	return ' th23autolink_encode_start_' . $th23_autolinks_uid . base64_encode('<a href="' . ereg_replace('\|', '" title="', ereg_replace('^\./http', 'http', str_replace($th23_autolinks_local['search'], $th23_autolinks_local['replace'], $th23_autolinks['replacement'][strtolower($match[1])]))) . '"' . $th23_autolinks_css . '>' . $match[1] . '</a>') . 'th23autolink_encode_end_' . $th23_autolinks_uid . ' ';
}

// decode the encoded links and smilies to display them properly
function th23_autolinks_decode($match)
{
	return base64_decode($match[1]);
}

?>
