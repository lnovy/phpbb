<?php
/*
*
* includes/acp/info/acp_th23_autolinks.php
*
* @package th23_autolinks
* @author Thorsten Hartmann (www.th23.net)
* @copyright (c) 2008 by Thorsten Hartmann (www.th23.net)
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

class acp_th23_autolinks_info
{
	function module()
	{
		global $user;

		$user->add_lang('acp/th23_autolinks');

		return array(
			'filename' => 'acp_th23_autolinks',
			'title' => 'ACP_TH23_AUTOLINKS',
			'version' => '1.0.0',
			'modes' => array(
				'default' => array('title' => 'ACP_TH23_AUTOLINKS', 'auth' => 'acl_a_board', 'cat' => array('ACP_DOT_MODS')),
				'settings' => array('title' => 'ACP_TH23_AUTOLINKS_SETTINGS', 'auth' => 'acl_a_board', 'cat' => array('ACP_DOT_MODS')),
				'urls' => array('title' => 'ACP_TH23_AUTOLINKS_URLS', 'auth' => 'acl_a_board', 'cat' => array('ACP_DOT_MODS')),
			),
		);
	}

	function install()
	{
	}

	function uninstall()
	{
	}
}

?>